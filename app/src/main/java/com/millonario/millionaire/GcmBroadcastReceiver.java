package com.millonario.millionaire;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

// Referenced classes of package com.millonario.millionaire:
//            GcmIntentService

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    public GcmBroadcastReceiver() {
    }

    public void onReceive(Context context, Intent intent) {
        startWakefulService(context, intent.setComponent(new ComponentName(context.getPackageName(), com.millonario.millionaire.GcmIntentService.class.getName())));
        setResultCode(-1);
    }
}
