package com.millonario.millionaire.objetos;

import java.util.ArrayList;
import java.util.Random;

public class Pergunta {

    public int Dificuldade;
    public int ID;
    public String Pergunta;
    public String RespostaCerta;
    public String RespostaErrada1;
    public String RespostaErrada2;
    public String RespostaErrada3;
    public boolean isRespostaErrada1Visivel;
    public boolean isRespostaErrada2Visivel;
    public boolean isRespostaErrada3Visivel;
    public boolean saiu;
    private int respostaCertaIndex;
    private ArrayList respostas;

    public Pergunta() {
        ID = -1;
        Dificuldade = 0;
        Pergunta = "";
        respostaCertaIndex = -1;
        respostas = null;
        isRespostaErrada1Visivel = true;
        isRespostaErrada2Visivel = true;
        isRespostaErrada3Visivel = true;
        saiu = false;
    }

    private void ocultaResposta(int i) {
        if (respostas.size() > i) {
            if (((String) respostas.get(i)).equals(RespostaErrada1)) {
                isRespostaErrada1Visivel = false;
                respostas.remove(i);
                return;
            }
            if (((String) respostas.get(i)).equals(RespostaErrada2)) {
                isRespostaErrada2Visivel = false;
                respostas.remove(i);
                return;
            }
            if (((String) respostas.get(i)).equals(RespostaErrada3)) {
                isRespostaErrada3Visivel = false;
                respostas.remove(i);
                return;
            }
        }
    }

    public int getIndexRespostaCerta() {
        int i = 0;
        while (i < respostas.size()) {
            if (((String) respostas.get(i)).equals(RespostaCerta)) {
                respostaCertaIndex = i;
                break;
            }
            i++;
        }
        return respostaCertaIndex;
    }

    public ArrayList getRespostas() {
        if (respostas != null)
            return respostas;
        ArrayList arraylist;
        int i;
        Random random;
        respostas = new ArrayList();
        arraylist = new ArrayList();
        arraylist.add(RespostaCerta);
        arraylist.add(RespostaErrada1);
        arraylist.add(RespostaErrada2);
        arraylist.add(RespostaErrada3);
        i = 0;
        random = new Random();
        random.setSeed(System.currentTimeMillis());
        while (arraylist.size() > 0) {
            int j = random.nextInt(arraylist.size());
            if (((String) arraylist.get(j)).equals(RespostaCerta)) {
                respostaCertaIndex = i;
            }
            respostas.add((String) arraylist.get(j));
            arraylist.remove(j);
            i++;
        }
        return respostas;
    }

    public boolean isRespostaCorreta(String s) {
        return s.equals(RespostaCerta);
    }

    public void ocultaResposta(String s) {
        int i = 0;
        do {
            if (i >= respostas.size()) {
                return;
            }
            if (((String) respostas.get(i)).equals(s)) {
                ocultaResposta(i);
                return;
            }
            i++;
        } while (true);
    }
}
