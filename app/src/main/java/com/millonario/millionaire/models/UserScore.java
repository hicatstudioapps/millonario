package com.millonario.millionaire.models;


public class UserScore
    implements Comparable
{

    private String imageURL;
    private boolean me;
    private String nivel;
    private String nombre;
    private String ranking;

    public UserScore(String s, String s1, String s2, String s3)
    {
        me = false;
        ranking = s;
        nombre = s1;
        nivel = s2;
        imageURL = s3;
    }

    public UserScore(String s, String s1, String s2, String s3, boolean flag)
    {
        me = false;
        ranking = s;
        nombre = s1;
        nivel = s2;
        me = flag;
        imageURL = s3;
    }

    public int compareTo(UserScore userscore)
    {
        return Integer.parseInt(nivel) - Integer.parseInt(userscore.nivel);
    }

    public int compareTo(Object obj)
    {
        return compareTo((UserScore)obj);
    }

    public String getImageURL()
    {
        return imageURL;
    }

    public String getNivel()
    {
        return nivel;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getRanking()
    {
        return ranking;
    }

    public boolean isMe()
    {
        return me;
    }

    public void setImageURL(String s)
    {
        imageURL = s;
    }

    public void setMe(boolean flag)
    {
        me = flag;
    }

    public void setNivel(String s)
    {
        nivel = s;
    }

    public void setNombre(String s)
    {
        nombre = s;
    }

    public void setRanking(String s)
    {
        ranking = s;
    }
}
