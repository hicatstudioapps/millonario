package com.millonario.millionaire.utils.youtube2;


public class Format {

    protected int mId;

    public Format(int i) {
        mId = i;
    }

    public Format(String s) {
        mId = Integer.parseInt(s.split("/")[0]);
    }

    public boolean equals(Object obj) {
        while (!(obj instanceof Format) || ((Format) obj).mId != mId) {
            return false;
        }
        return true;
    }

    public int getId() {
        return mId;
    }
}
