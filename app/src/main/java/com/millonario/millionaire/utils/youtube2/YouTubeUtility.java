package com.millonario.millionaire.utils.youtube2;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.FactoryConfigurationError;

// Referenced classes of package com.millonario.millionaire.utils.youtube2:
//            Format, VideoStream, PlaylistId

public class YouTubeUtility {

    public YouTubeUtility() {
    }

    public static String calculateYouTubeUrl(String s, boolean flag, String s1)
            throws IOException, ClientProtocolException, UnsupportedEncodingException {
        String as[];
        HashMap hashmap;
        int i;
        String s3;
        String s4;
        String s5;
        ArrayList arraylist;
        String as2[];
        ArrayList arraylist1;
        int j;
        int k;
        String as3[];
        int l1;
        int i2;
        HttpResponse httpresponse = (new DefaultHttpClient()).execute(new HttpGet((new StringBuilder("http://www.youtube.com/get_video_info?&video_id=")).append(s1).toString()));
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        httpresponse.getEntity().writeTo(bytearrayoutputstream);
        String s2 = new String(bytearrayoutputstream.toString("UTF-8"));
        as = s2.split("&");
        hashmap = new HashMap();
        i = 0;
        while (i < as.length) {
            String as1[] = as[i].split("=");
            if (as1 != null && as1.length >= 2) {
                hashmap.put(as1[0], URLDecoder.decode(as1[1]));
            }
            i++;
        }
        s3 = URLDecoder.decode((String) hashmap.get("fmt_list"));
        arraylist = new ArrayList();
        if (s3 != null) {
            as3 = s3.split(",");
            l1 = as3.length;
            i2 = 0;
            while (i2 < l1) {
                arraylist.add(new Format(as3[i2]));
                i2++;
            }
        }
        s4 = (String) hashmap.get("url_encoded_fmt_stream_map");
        s5 = null;
        if (s4 == null)
            return s5;
        as2 = s4.split(",");
        arraylist1 = new ArrayList();
        j = as2.length;
        k = 0;
        while (k < j) {
            String s6 = as2[k];
            VideoStream videostream = new VideoStream(s6);
            arraylist1.add(videostream);
            k++;
        }
        Format format;
        int l = Integer.parseInt(s);
        format = new Format(l);
        while (!arraylist.contains(format) && flag) {
            int j1;
            int k1;
            j1 = format.getId();
            k1 = getSupportedFallbackId(j1);
            if (j1 == k1)
                break;
            format = new Format(k1);
        }
        int i1 = arraylist.indexOf(format);
        s5 = null;
        if (i1 >= 0) {
            s5 = ((VideoStream) arraylist1.get(i1)).getUrl();
        }
        return s5;
    }

    public static int getSupportedFallbackId(int i) {
        int ai[] = {
                13, 17, 18, 22, 37
        };
        int j = i;
        int k = -1 + ai.length;
        do {
            if (k < 0) {
                return j;
            }
            if (i == ai[k] && k > 0) {
                j = ai[k - 1];
            }
            k--;
        } while (true);
    }

    public static boolean hasVideoBeenViewed(Context context, String s) {
        String as[];
        String s1 = PreferenceManager.getDefaultSharedPreferences(context).getString("com.keyes.screebl.lastViewedVideoIds", null);
        if (s1 != null) {
            if ((as = s1.split(";")) != null && as.length != 0) {
                int i = 0;
                while (i < as.length) {
                    if (as[i] != null && as[i].equals(s)) {
                        return true;
                    }
                    i++;
                }
            }
        }
        return false;
    }

    public static void markVideoAsViewed(Context context, String s) {
        SharedPreferences sharedpreferences;
        String as[];
        HashMap hashmap;
        int i;
        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (s == null) {
            return;
        }
        String s1 = sharedpreferences.getString("com.keyes.screebl.lastViewedVideoIds", null);
        if (s1 == null) {
            s1 = "";
        }
        as = s1.split(";");
        if (as == null) {
            as = new String[0];
        }
        hashmap = new HashMap();
        i = 0;
        while (i < as.length) {
            hashmap.put(as[i], as[i]);
            i++;
        }
        String s2;
        Iterator iterator;
        s2 = "";
        iterator = hashmap.keySet().iterator();
        while (true) {
            if (!iterator.hasNext()) {
                String s4 = (new StringBuilder(String.valueOf(s2))).append(s).append(";").toString();
                android.content.SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("com.keyes.screebl.lastViewedVideoIds", s4);
                editor.commit();
                return;
            }
            String s3 = (String) iterator.next();
            if (!s3.trim().equals("")) {
                s2 = (new StringBuilder(String.valueOf(s2))).append(s3).append(";").toString();
            }
        }
    }

    public static String queryLatestPlaylistVideo(PlaylistId playlistid)
            throws IOException, FactoryConfigurationError {
        HttpResponse httpresponse;
        ByteArrayOutputStream bytearrayoutputstream;
        httpresponse = (new DefaultHttpClient()).execute(new HttpGet((new StringBuilder("http://gdata.youtube.com/feeds/api/playlists/")).append(playlistid.getId()).append("?v=2&max-results=50&alt=json").toString()));
        bytearrayoutputstream = new ByteArrayOutputStream();
        JSONObject jsonobject;
        httpresponse.getEntity().writeTo(bytearrayoutputstream);
        String s = bytearrayoutputstream.toString("UTF-8");
        try {
            jsonobject = new JSONObject(s);
            JSONArray jsonarray1;
            JSONArray jsonarray = jsonobject.getJSONObject("feed").getJSONArray("entry");
            jsonarray1 = jsonarray.getJSONObject(-1 + jsonarray.length()).getJSONArray("link");
            int i = 0;
            while (i < jsonarray1.length()) {
                JSONObject jsonobject1;
                String s1;
                jsonobject1 = jsonarray1.getJSONObject(i);
                s1 = jsonobject1.optString("rel", null);
                if (s1 == null) {
                    i++;
                    continue;
                }
                String s2;
                if (!s1.equals("alternate")) {
                    i++;
                    continue;
                }
                s2 = Uri.parse(jsonobject1.optString("href", null)).getQueryParameter("v");
                return s2;
            }
        } catch (Exception e) {
        }
        return null;
    }
}
