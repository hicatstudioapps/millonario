package com.millonario.millionaire.utils;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;

import java.util.Timer;
import java.util.TimerTask;

// Referenced classes of package com.millonario.millionaire.utils:
//            Utils, DBAdapter

public class MusicaFundo
        implements android.media.MediaPlayer.OnErrorListener {

    public static final int SOM_CORRETO = 2;
    public static final int SOM_ERRADO = 1;
    public static final int SOM_TICK = 4;
    public static final int SOM_VENCEDOR = 3;
    private static final float FLOAT_VOLUME_MAX = 1F;
    private static final float FLOAT_VOLUME_MIN = 0F;
    private static final int INT_VOLUME_MAX = 100;
    private static final int INT_VOLUME_MIN = 0;
    private static MusicaFundo instance = new MusicaFundo();
    private final int soundID;
    public boolean isPlaying;
    public boolean isPlayingCR;
    boolean isFading;
    MediaPlayer temp;
    Timer timer;
    boolean wasPlaying;
    private int iVolume;
    private MediaPlayer mp;
    private MediaPlayer mp_contra;
    private SoundPool soundPool;

    private MusicaFundo() {
        isPlaying = false;
        isPlayingCR = false;
        wasPlaying = false;
        isFading = false;
        timer = null;
        try {
            if (mp == null || !mp.isPlaying()) {
                configuraMP(R.raw.som_fundo);
            }
        } catch (Exception exception) {
            configuraMP(R.raw.som_fundo);
        }
        instance = this;
        soundPool = new SoundPool(10, 3, 0);
        soundID = soundPool.load(App.getContext(), R.raw.som_tick, 1);
    }

    public static MusicaFundo getInstance() {
        return instance;
    }

    private void configuraMP(int i) {
        if (i != R.raw.som_fundo) {
            mp_contra = MediaPlayer.create(App.getContext(), i);
            if (mp_contra == null) {
                mp_contra = MediaPlayer.create(App.getContext(), i);
                if (mp_contra == null) {
                    return;
                }
            }
            mp_contra.setOnErrorListener(this);
            mp_contra.setLooping(true);
            return;
        }
        mp = MediaPlayer.create(App.getContext(), i);
        if (mp == null) {
            mp = MediaPlayer.create(App.getContext(), i);
            if (mp == null) {
                return;
            }
        }
        try {
            mp.setOnErrorListener(this);
            mp.setLooping(true);
            return;
        } catch (Exception exception) {
            return;
        }
    }

    private void fadeOut() {
        int i = 50;
        isFading = true;
        iVolume = 100;
        TimerTask timertask;
        int j;
        if (Utils.isVoiceOverActive()) {
            iVolume = i;
        } else {
            iVolume = 100;
        }
        timer = new Timer();
        timertask = new TimerTask() {
            public void run() {
                try {
                    updateVolume(-1);
                    if (iVolume <= 0) {
                        timer.cancel();
                        timer.purge();
                        timer = null;
                        mp.pause();
                        isFading = false;
                    }
                    return;
                } catch (Exception exception) {
                    mp = null;
                }
            }
        };
        if (!Utils.isVoiceOverActive()) {
            i = 100;
        }
        j = 2000 / i;
        if (j == 0) {
            j = 1;
        }
        timer.schedule(timertask, j, j);
    }

    private void updateVolume(int i) {
        int j = 50;
        iVolume = i + iVolume;
        int j1;
        float f;
        float f1;
        if (iVolume < 0) {
            iVolume = 0;
        } else {
            int k = iVolume;
            int l;
            if (Utils.isVoiceOverActive()) {
                l = j;
            } else {
                l = 100;
            }
            if (k > l) {
                int i1;
                if (Utils.isVoiceOverActive()) {
                    i1 = j;
                } else {
                    i1 = 100;
                }
                iVolume = i1;
            }
        }
        if (Utils.isVoiceOverActive()) {
            j1 = j;
        } else {
            j1 = 100;
        }
        f = (float) Math.log(j1 - iVolume);
        if (!Utils.isVoiceOverActive()) {
            j = 100;
        }
        f1 = 1.0F - f / (float) Math.log(j);
        if (f1 < 0.0F) {
            f1 = 0.0F;
        } else {
            float f2;
            if (Utils.isVoiceOverActive()) {
                f2 = 0.5F;
            } else {
                f2 = 1.0F;
            }
            if (f1 > f2) {
                if (Utils.isVoiceOverActive()) {
                    f1 = 0.5F;
                } else {
                    f1 = 1.0F;
                }
            }
        }
        mp.setVolume(f1, f1);
    }

    public boolean onError(MediaPlayer mediaplayer, int i, int j) {
        MediaPlayer mediaplayer1;
        try {
            mediaplayer.stop();
        } catch (Exception exception) {
        }
        try {
            mediaplayer.release();
        } catch (Exception exception1) {
        }
        mediaplayer1 = MediaPlayer.create(App.getContext(), R.raw.som_fundo);
        if (mediaplayer1 == null) {
            mediaplayer1 = MediaPlayer.create(App.getContext(), R.raw.som_fundo);
            if (mediaplayer1 == null)
                return true;
        }
        mediaplayer1.setLooping(true);
        mediaplayer1.setOnErrorListener(this);
        if (isPlaying) {
            mediaplayer1.start();
            return true;
        }
        return true;
    }

    public void paraContraRelogio() {
        try {
            synchronized (this) {
                boolean flag = App.DB().getValue("isSonsJogoOn").equals("1");
                if (flag) {
                    mp_contra.stop();
                    //mp_contra.release();
                    isPlayingCR = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        realPause();
    }

    public void playSoundEffect(int i) {
        boolean flag;
        try {
            if (!App.DB().getValue("isSonsJogoOn").equals("1"))
                return;
            if (flag = App.isApplicationSentToBackground())
                return;
        } catch (Exception e) {
        }
        temp = MediaPlayer.create(App.getContext(), i);
        if (temp == null) {
            temp = MediaPlayer.create(App.getContext(), i);
            if (temp == null)
                return;
        }
        temp.setLooping(false);
        temp.setOnCompletionListener(new android.media.MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaplayer) {
                try {
                    temp.release();
                    return;
                } catch (Exception exception1) {
                    return;
                }
            }
        });
        temp.start();
    }

    public void playTick() {
        AudioManager audiomanager = (AudioManager) App.getContext().getSystemService("audio");
        float f = (float) audiomanager.getStreamVolume(3) / (float) audiomanager.getStreamMaxVolume(3);
        soundPool.play(soundID, f, f, 1, 0, 1.0F);
    }

    public void realPause() {
        if (isPlaying) {
            try {
                mp.pause();
            } catch (Exception exception) {
                mp = null;
            }
            isPlaying = false;
        }
    }

    public void start() {
        boolean flag2;
        boolean flag;
        boolean flag1;

        synchronized (this) {
            flag2 = isFading;
            if (timer != null && flag2) {
                try {
                    mp.pause();
                    timer.cancel();
                    timer.purge();
                    timer = null;
                    isFading = false;
                } catch (Exception exception3) {
                }
            }
            if (isPlaying)
                return;
            try {
                flag = App.DB().getValue("isMusicaOn").equals("1");
                if (!flag)
                    return;
                if (mp_contra != null) {
                    flag1 = mp_contra.isPlaying();
                    if (flag1)
                        return;
                }
                if (mp == null) {
                    configuraMP(R.raw.som_fundo);
                }
                if (!Utils.isVoiceOverActive())
                    mp.setVolume(1.0F, 1.0F);
                else
                    mp.setVolume(0.5F, 0.5F);
                mp.start();
                isPlaying = true;
            } catch (Exception e) {
            }
        }
    }

    public void stop() {
        isPlaying = false;
        try {
            mp.stop();
            mp.release();
            return;
        } catch (Exception exception) {
            return;
        }
    }

    public void tocaContraRelogio() {
        try {
            synchronized (this) {
                boolean flag = App.DB().getValue("isSonsJogoOn").equals("1");
                if (!flag)
                    return;
                realPause();
                try {
                    mp_contra.stop();
                } catch (Exception exception1) {
                }
                configuraMP(R.raw.som_contrarrelogio);
                if (mp_contra == null)
                    return;
                if (!Utils.isVoiceOverActive())
                    mp_contra.setVolume(1.0F, 1.0F);
                else
                    mp_contra.setVolume(0.5F, 0.5F);
                mp_contra.start();
                isPlayingCR = true;
            }
        } catch (Exception e) {
        }
    }

    public void tocaSom(int i) {
        tocaSom(i, false);
    }

    public void tocaSom(int i, boolean flag) {
        boolean flag1;
        if (!App.DB().getValue("isSonsJogoOn").equals("1"))
            return;
        if (flag1 = App.isApplicationSentToBackground())
            return;
        int j = -1;
        switch (i) {
            default:
                break;
            case 1:
                j = R.raw.som_resposta_errada;
                if (App.isContraRelogio) {
                    paraContraRelogio();
                }
                break;
            case 2:
                j = R.raw.som_resposta_correta;
                if (App.isContraRelogio) {
                    flag = false;
                }
                break;
            case 3:
                j = R.raw.som_vencedor;
                if (App.isContraRelogio) {
                    paraContraRelogio();
                }
                break;
            case 4:
                j = R.raw.som_tick;
                if (App.isContraRelogio) {
                    flag = false;
                }
                break;
        }
        final boolean tempBool = flag;
        if (j == -1)
            return;
        wasPlaying = isPlaying;
        try {
            mp.pause();
        } catch (Exception exception1) {
        }
        temp = MediaPlayer.create(App.getContext(), j);
        if (temp == null) {
            temp = MediaPlayer.create(App.getContext(), j);
            if (temp == null)
                return;
        }
        temp.setLooping(false);
        temp.setOnCompletionListener(new android.media.MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaplayer) {
                try {
                    temp.release();
                } catch (Exception exception2) {
                }
                if (App.DB().getValue("isMusicaOn").equals("1") && (wasPlaying || tempBool)) {
                    start();
                }
            }
        });
        temp.start();
    }
}
