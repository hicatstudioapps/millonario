package com.millonario.millionaire.utils.youtube2;

import java.util.HashMap;

public class VideoStream {

    protected String mUrl;

    public VideoStream(String s) {
        String as[] = s.split("&");
        HashMap hashmap = new HashMap();
        int i = 0;
        do {
            if (i >= as.length) {
                mUrl = (String) hashmap.get("url");
                return;
            }
            String as1[] = as[i].split("=");
            if (as1 != null && as1.length >= 2) {
                hashmap.put(as1[0], as1[1]);
            }
            i++;
        } while (true);
    }

    public String getUrl() {
        return mUrl;
    }
}
