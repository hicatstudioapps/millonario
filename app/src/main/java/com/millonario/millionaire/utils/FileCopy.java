package com.millonario.millionaire.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;

import com.millonario.millionaire.classes.App;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

// Referenced classes of package com.millonario.millionaire.utils:
//            Utils

public class FileCopy {

    private static final FileCopy instance = new FileCopy();

    private FileCopy() {
    }

    public static FileCopy Instance() {
        return instance;
    }

    public static File copyImg(Context context, int i) {
        File file;
        File file1;
        file = new File((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/Android/data/").append(App.getContext().getPackageName()).append("/icone/logo.png").toString());
        file1 = new File((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/Android/data/").append(App.getContext().getPackageName()).append("/icone/").toString());
        if (file.exists())
            return file;
        FileOutputStream fileoutputstream;
        InputStream inputstream;
        if (!file1.exists()) {
            file1.mkdirs();
        }
        fileoutputstream = null;
        inputstream = null;
        try {
            FileOutputStream fileoutputstream1 = new FileOutputStream(file);
            byte abyte0[];
            inputstream = context.getResources().openRawResource(i);
            abyte0 = new byte[1024];
            while (true) {
                int j = inputstream.read(abyte0);
                if (j == -1)
                    break;
                fileoutputstream1.write(abyte0, 0, j);
            }
            if (inputstream != null) {
                try {
                    inputstream.close();
                } catch (IOException ioexception8) {
                }
            }
            if (fileoutputstream1 != null) {
                try {
                    fileoutputstream1.close();
                } catch (IOException ioexception7) {
                    return file;
                }
            }
            return file;
        } catch (Exception e) {
            return null;
        }
    }

    public static Uri downloadFile(Uri uri, String s) {
        try {
            File file = new File((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/Android/data/").append(App.getContext().getPackageName()).append("/video/temp.").append(s).toString());
            File file1 = new File((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/Android/data/").append(App.getContext().getPackageName()).append("/video/").toString());
            if (!file1.exists()) {
                file1.mkdirs();
            }
            HttpURLConnection httpurlconnection;
            StatFs statfs;
            InputStream inputstream;
            FileOutputStream fileoutputstream;
            byte abyte0[];
            int i;
            if (file.exists()) {
                try {
                    file.delete();
                } catch (Exception exception1) {
                }
            }
            httpurlconnection = (HttpURLConnection) (new URL(uri.toString())).openConnection();
            httpurlconnection.setDoInput(true);
            httpurlconnection.setConnectTimeout(10000);
            httpurlconnection.connect();
            statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            if ((double) statfs.getAvailableBlocks() * (double) statfs.getBlockSize() < (double) httpurlconnection.getContentLength()) {
                return null;
            }
            inputstream = httpurlconnection.getInputStream();
            fileoutputstream = new FileOutputStream(file.getAbsolutePath());
            abyte0 = new byte[4096];
            while (true) {
                i = inputstream.read(abyte0);
                if (i == -1) {
                    try {
                        fileoutputstream.flush();
                        fileoutputstream.close();
                    }
                    // Misplaced declaration of an exception variable
                    catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    return Uri.fromFile(file);
                }
                fileoutputstream.write(abyte0, 0, i);
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static String downloadImg(String s) {
        return downloadImg(s, "cross_app");
    }

    public static String downloadImg(String s, String s1) {
        File file;
        if (Utils.isEmpty(s)) {
            return "";
        }
        String as[] = s.split("/");
        String s2;
        File file1;
        if (s1.equals("me")) {
            s2 = "me.png";
        } else {
            s2 = as[-1 + as.length];
        }
        file = new File((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/Android/data/").append(App.getContext().getPackageName()).append("/").append(s1).append("/").append(s2).toString());
        file1 = new File((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/Android/data/").append(App.getContext().getPackageName()).append("/").append(s1).append("/").toString());
        if (!file1.exists()) {
            file1.mkdirs();
        }
        if (file.exists()) {
            return file.getAbsolutePath();
        }
        HttpURLConnection httpurlconnection;
        StatFs statfs;
        try {
            URL url = new URL(s);
            httpurlconnection = (HttpURLConnection) url.openConnection();
            httpurlconnection.setDoInput(true);
            httpurlconnection.setConnectTimeout(10000);
            httpurlconnection.connect();
            statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            if ((double) statfs.getAvailableBlocks() * (double) statfs.getBlockSize() < (double) httpurlconnection.getContentLength()) {
                return null;
            }
            InputStream inputstream;
            FileOutputStream fileoutputstream;
            byte abyte0[];
            inputstream = httpurlconnection.getInputStream();
            fileoutputstream = new FileOutputStream(file.getAbsolutePath());
            abyte0 = new byte[4096];
            while (true) {
                int i = inputstream.read(abyte0);
                if (i == -1) {
                    try {
                        fileoutputstream.flush();
                        fileoutputstream.close();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    return file.getAbsolutePath();
                }
                fileoutputstream.write(abyte0, 0, i);
            }
        } catch (Exception e) {
            return "";
        }
    }
}
