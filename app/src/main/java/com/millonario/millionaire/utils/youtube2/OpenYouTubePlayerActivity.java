package com.millonario.millionaire.utils.youtube2;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.popup.PopUp;
import com.nineoldandroids.view.ViewHelper;

// Referenced classes of package com.millonario.millionaire.utils.youtube2:
//            PlaylistId, YouTubeId, VideoId, YouTubeUtility

@SuppressWarnings("ResourceType")
public class OpenYouTubePlayerActivity extends Activity {
    public static final String MSG_DETECT = "com.keyes.video.msg.detect";
    public static final String MSG_ERROR_MSG = "com.keyes.video.msg.error.msg";
    public static final String MSG_ERROR_TITLE = "com.keyes.video.msg.error.title";
    public static final String MSG_HI_BAND = "com.keyes.video.msg.hiband";
    public static final String MSG_INIT = "com.keyes.video.msg.init";
    public static final String MSG_LO_BAND = "com.keyes.video.msg.loband";
    public static final String MSG_PLAYLIST = "com.keyes.video.msg.playlist";
    public static final String MSG_TOKEN = "com.keyes.video.msg.token";
    public static final String SCHEME_YOUTUBE_PLAYLIST = "ytpl";
    public static final String SCHEME_YOUTUBE_VIDEO = "ytv";
    static final String YOUTUBE_PLAYLIST_ATOM_FEED_URL = "http://gdata.youtube.com/feeds/api/playlists/";
    static final String YOUTUBE_VIDEO_INFORMATION_URL = "http://www.youtube.com/get_video_info?&video_id=";
    protected View mClickView;
    protected String mMsgDetect;
    protected String mMsgError;
    protected String mMsgErrorTitle;
    protected String mMsgHiBand;
    protected String mMsgInit;
    protected String mMsgLowBand;
    protected String mMsgPlaylist;
    protected String mMsgToken;
    protected ProgressBar mProgressBar;
    protected TextView mProgressMessage;
    protected QueryYouTubeTask mQueryYouTubeTask;
    protected String mVideoId;
    protected VideoView mVideoView;
    private String videoLink;
    private String videoName;
    public OpenYouTubePlayerActivity() {
        mMsgInit = "Initializing";
        mMsgDetect = "Detecting Bandwidth";
        mMsgPlaylist = "Determining Latest Video in YouTube Playlist";
        mMsgToken = "Retrieving YouTube Video Token";
        mMsgLowBand = "Buffering Low-bandwidth Video";
        mMsgHiBand = "Buffering High-bandwidth Video";
        mMsgErrorTitle = "Communications Error";
        mMsgError = "An error occurred during the retrieval of the video.  This could be due to netwo" +
                "rk issues or YouTube protocols.  Please try again later."
        ;
        mVideoId = null;
        videoName = null;
        videoLink = null;
    }

    private void extractMessages() {
        Intent intent = getIntent();
        String s = intent.getStringExtra("com.keyes.video.msg.init");
        if (s != null) {
            mMsgInit = s;
        }
        String s1 = intent.getStringExtra("com.keyes.video.msg.detect");
        if (s1 != null) {
            mMsgDetect = s1;
        }
        String s2 = intent.getStringExtra("com.keyes.video.msg.playlist");
        if (s2 != null) {
            mMsgPlaylist = s2;
        }
        String s3 = intent.getStringExtra("com.keyes.video.msg.token");
        if (s3 != null) {
            mMsgToken = s3;
        }
        String s4 = intent.getStringExtra("com.keyes.video.msg.loband");
        if (s4 != null) {
            mMsgLowBand = s4;
        }
        String s5 = intent.getStringExtra("com.keyes.video.msg.hiband");
        if (s5 != null) {
            mMsgHiBand = s5;
        }
        String s6 = intent.getStringExtra("com.keyes.video.msg.error.title");
        if (s6 != null) {
            mMsgErrorTitle = s6;
        }
        String s7 = intent.getStringExtra("com.keyes.video.msg.error.msg");
        if (s7 != null) {
            mMsgError = s7;
        }
    }

    private void setupView() {
        LinearLayout linearlayout = new LinearLayout(this);
        linearlayout.setId(1);
        linearlayout.setOrientation(1);
        linearlayout.setGravity(17);
        linearlayout.setBackgroundColor(0xff000000);
        linearlayout.setLayoutParams(new android.widget.LinearLayout.LayoutParams(-1, -1));
        setContentView(linearlayout);
        RelativeLayout relativelayout = new RelativeLayout(this);
        relativelayout.setId(2);
        relativelayout.setGravity(17);
        relativelayout.setBackgroundColor(0xff000000);
        relativelayout.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(-1, -1));
        linearlayout.addView(relativelayout);
        mVideoView = new VideoView(this);
        mVideoView.setId(3);
        android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, -1);
        layoutparams.addRule(13);
        mVideoView.setLayoutParams(layoutparams);
        relativelayout.addView(mVideoView);
        View view = new View(this);
        view.setId(10);
        android.widget.RelativeLayout.LayoutParams layoutparams1 = new android.widget.RelativeLayout.LayoutParams(-1, -1);
        layoutparams1.addRule(13);
        view.setLayoutParams(layoutparams1);
        view.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view1) {
                App.DB().insertValue("sim", "ViuVideoAteFim");
                //Analytics.send("V\355deo", videoName, null);
                App.Musica.start();
                String s = (new StringBuilder("market://details?id=")).append(videoLink).toString();
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(s));
                startActivity(intent);
                finish();
            }
        });
        relativelayout.addView(view);
        ViewHelper.setAlpha(view, 0.0F);
        view.setVisibility(4);
        mClickView = view;
        mProgressBar = new ProgressBar(this);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setVisibility(0);
        mProgressBar.setEnabled(true);
        mProgressBar.setId(4);
        android.widget.RelativeLayout.LayoutParams layoutparams2 = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams2.addRule(13);
        mProgressBar.setLayoutParams(layoutparams2);
        relativelayout.addView(mProgressBar);
        mProgressMessage = new TextView(this);
        mProgressMessage.setId(5);
        android.widget.RelativeLayout.LayoutParams layoutparams3 = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams3.addRule(14);
        layoutparams3.addRule(3, 4);
        mProgressMessage.setLayoutParams(layoutparams3);
        mProgressMessage.setTextColor(0xffcccccc);
        mProgressMessage.setTextSize(2, 12F);
        mProgressMessage.setText("...");
        Button button = new Button(this);
        button.setId(6);
        button.setText("X");
        button.setBackgroundColor(0);
        button.setTextColor(-1);
        button.setTextSize(20F);
        button.setTypeface(null, 1);
        ViewHelper.setAlpha(button, 0.7F);
        android.widget.RelativeLayout.LayoutParams layoutparams4 = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams3.addRule(10);
        layoutparams3.addRule(11);
        button.setLayoutParams(layoutparams4);
        int i = (int) (0.5F + 10F * getResources().getDisplayMetrics().density);
        button.setPadding(i, i, i, i);
        button.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view1) {
                App.Musica.start();
                if (mVideoView != null) {
                    try {
                        if (mVideoView.getCurrentPosition() >= 10000) {
                            App.DB().insertValue("sim", "ViuVideoAteFim");
                        }
                    } catch (Exception exception) {
                    }
                }
                finish();
            }
        });
        relativelayout.addView(button);
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        videoName = getIntent().getStringExtra("KEY_VIDEO_NAME");
        videoLink = getIntent().getStringExtra("KEY_VIDEO_LINK");
        setupView();
        extractMessages();
        getWindow().setFlags(128, 128);
        setRequestedOrientation(0);
        mProgressBar.bringToFront();
        mProgressBar.setVisibility(0);
        mProgressMessage.setText(mMsgInit);
        Uri uri = getIntent().getData();
        if (uri == null) {
            Log.i(getClass().getSimpleName(), "No video ID was specified in the intent.  Closing video activity.");
            finish();
        }
        String s = uri.getScheme();
        String s1 = uri.getEncodedSchemeSpecificPart();
        if (s1 == null) {
            Log.i(getClass().getSimpleName(), "No video ID was specified in the intent.  Closing video activity.");
            finish();
        }
        Object obj;
        if (s1.startsWith("//")) {
            if (s1.length() > 2) {
                s1 = s1.substring(2);
            } else {
                Log.i(getClass().getSimpleName(), "No video ID was specified in the intent.  Closing video activity.");
                finish();
            }
        }
        if (s != null && s.equalsIgnoreCase("ytpl")) {
            obj = new PlaylistId(s1);
        } else {
            obj = null;
            if (s != null) {
                boolean flag = s.equalsIgnoreCase("ytv");
                obj = null;
                if (flag) {
                    obj = new VideoId(s1);
                }
            }
        }
        if (obj == null) {
            Log.i(getClass().getSimpleName(), "Unable to extract video ID from the intent.  Closing video activity.");
            finish();
        }
        if (android.os.Build.VERSION.SDK_INT == 9 || android.os.Build.VERSION.SDK_INT == 10) {
            mQueryYouTubeTask = (QueryYouTubeTask) (new QueryYouTubeTask(null)).execute(new YouTubeId[]{
                    (YouTubeId) obj
            });
            return;
        } else {
            mQueryYouTubeTask = (QueryYouTubeTask) (new QueryYouTubeTask(null)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new YouTubeId[]{
                    (YouTubeId) obj
            });
            return;
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        YouTubeUtility.markVideoAsViewed(this, mVideoId);
        if (mQueryYouTubeTask != null) {
            mQueryYouTubeTask.cancel(true);
        }
        if (mVideoView != null) {
            mVideoView.stopPlayback();
        }
        getWindow().clearFlags(128);
        mQueryYouTubeTask = null;
        mVideoView = null;
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onStop() {
        super.onStop();
    }

    public void updateProgress(String s) {
        try {
            mProgressMessage.setText(s);
            return;
        } catch (Exception exception) {
            Log.e(getClass().getSimpleName(), "Error updating video status!", exception);
        }
    }

    private class ProgressUpdateInfo {
        public String mMsg;

        public ProgressUpdateInfo(String s) {
            super();
            mMsg = s;
        }
    }

    private class QueryYouTubeTask extends AsyncTask {

        private boolean mShowedError;

        private QueryYouTubeTask() {
            super();
            mShowedError = false;
        }

        QueryYouTubeTask(QueryYouTubeTask queryyoutubetask) {
            this();
        }

        private void showErrorAlert() {
            try {
                PopUp.show(OpenYouTubePlayerActivity.this, 0, 1);
                PopUp.setMensagem(App.getMyString(R.string.video_nao_disponivel));
                PopUp.setTextoEListenerBotao(App.getMyString(R.string.ok), new android.view.View.OnClickListener() {
                    public void onClick(View view) {
                        PopUp.hide();
                        finish();
                    }
                }, 1);
                return;
            } catch (Exception exception) {
                Log.e(getClass().getSimpleName(), "Problem showing error dialog.", exception);
            }
        }

        protected Uri doInBackground(YouTubeId ayoutubeid[]) {
            String s;
            s = null;
            if (isCancelled()) {
                return null;
            }
            try {
                ProgressUpdateInfo aprogressupdateinfo[] = new ProgressUpdateInfo[1];
                aprogressupdateinfo[0] = new ProgressUpdateInfo(mMsgDetect);
                publishProgress(aprogressupdateinfo);
                String s1;
                if (!(ayoutubeid[0] instanceof PlaylistId)) {
                    boolean flag = ayoutubeid[0] instanceof VideoId;
                    s1 = null;
                    if (flag)
                        s1 = ayoutubeid[0].getId();
                } else {
                    ProgressUpdateInfo aprogressupdateinfo4[] = new ProgressUpdateInfo[1];
                    aprogressupdateinfo4[0] = new ProgressUpdateInfo(mMsgPlaylist);
                    publishProgress(aprogressupdateinfo4);
                    s1 = YouTubeUtility.queryLatestPlaylistVideo((PlaylistId) ayoutubeid[0]);
                }
                mVideoId = s1;
                ProgressUpdateInfo aprogressupdateinfo1[] = new ProgressUpdateInfo[1];
                aprogressupdateinfo1[0] = new ProgressUpdateInfo(mMsgToken);
                publishProgress(aprogressupdateinfo1);
                if (isCancelled()) {
                    return null;
                }
                s = YouTubeUtility.calculateYouTubeUrl("18", true, s1);
                if (isCancelled()) {
                    return null;
                }
                if (!"18".equals("17")) {
                    try {
                        ProgressUpdateInfo aprogressupdateinfo2[] = new ProgressUpdateInfo[1];
                        aprogressupdateinfo2[0] = new ProgressUpdateInfo(mMsgHiBand);
                        publishProgress(aprogressupdateinfo2);
                    } catch (Exception exception) {
                        Log.e(getClass().getSimpleName(), "Error occurred while retrieving information from YouTube.", exception);
                    }
                } else {
                    ProgressUpdateInfo aprogressupdateinfo3[] = new ProgressUpdateInfo[1];
                    aprogressupdateinfo3[0] = new ProgressUpdateInfo(mMsgLowBand);
                    publishProgress(aprogressupdateinfo3);
                }
                if (s != null)
                    return Uri.parse(s);
            } catch (Exception e) {
            }
            return null;
        }

        protected Object doInBackground(Object aobj[]) {
            return doInBackground((YouTubeId[]) aobj);
        }

        protected void onPostExecute(Uri uri) {
            super.onPostExecute(uri);
            if (isCancelled()) {
                return;
            }
            if (uri == null) {
                try {
                    throw new RuntimeException("Invalid NULL Url.");
                } catch (Exception exception) {
                    Log.e(getClass().getSimpleName(), "Error playing video!", exception);
                }
                if (!mShowedError) {
                    showErrorAlert();
                    return;
                }
                //break MISSING_BLOCK_LABEL_143;
                return;
            }
            mVideoView.setVideoURI(uri);
            if (!isCancelled()) {
                mVideoView.setOnCompletionListener(new android.media.MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mediaplayer) {
                        if (isCancelled()) {
                            return;
                        } else {
                            App.Musica.start();
                            App.DB().insertValue("sim", "ViuVideoAteFim");
                            return;
                        }
                    }
                });
                if (!isCancelled()) {
                    mVideoView.setOnPreparedListener(new android.media.MediaPlayer.OnPreparedListener() {
                        public void onPrepared(MediaPlayer mediaplayer) {
                            if (isCancelled()) {
                                return;
                            } else {
                                App.Musica.pause();
                                mProgressBar.setVisibility(8);
                                mProgressMessage.setVisibility(8);
                                mClickView.setVisibility(0);
                                return;
                            }
                        }
                    });
                    if (!isCancelled()) {
                        mVideoView.requestFocus();
                        mVideoView.start();
                    }
                }
            }
        }

        protected void onPostExecute(Object obj) {
            onPostExecute((Uri) obj);
        }

        protected void onProgressUpdate(ProgressUpdateInfo aprogressupdateinfo[]) {
            super.onProgressUpdate(aprogressupdateinfo);
            updateProgress(aprogressupdateinfo[0].mMsg);
        }

        protected void onProgressUpdate(Object aobj[]) {
            onProgressUpdate((ProgressUpdateInfo[]) aobj);
        }
    }


}
