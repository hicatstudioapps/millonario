package com.millonario.millionaire.utils;

public class Analytics {
    /*public enum TrackerName
    {
    	APP_TRACKER("APP_TRACKER", 0),
        GLOBAL_TRACKER("GLOBAL_TRACKER", 1);

        static 
        {
            TrackerName atrackername[] = new TrackerName[2];
            atrackername[0] = APP_TRACKER;
            atrackername[1] = GLOBAL_TRACKER;
        }

        private TrackerName(String s, int i)
        {
        }
    }


    public static int GENERAL_TRACKER = 0;
    static HashMap mTrackers = new HashMap();

    public Analytics()
    {
    }

    public static Tracker getTracker(TrackerName trackername)
    {
    	try{
	        synchronized(com.millonario.millionaire.utils.Analytics.class){
	        	Tracker tracker;
	            if(!mTrackers.containsKey(trackername))
	            {
	                Tracker tracker1 = GoogleAnalytics.getInstance(App.getContext()).newTracker(R.xml.app_tracker);
	                mTrackers.put(trackername, tracker1);
	            }
	            tracker = (Tracker)mTrackers.get(trackername);
	            return tracker;
	        }
    	}catch(Exception e){
    		return null;
    	}
    }

    public static void onStart(Activity activity)
    {
        GoogleAnalytics.getInstance(activity).reportActivityStart(activity);
        FlurryAgent.onStartSession(activity, App.getMyString(R.string.flurry_api_key));
    }

    public static void onStop(Activity activity)
    {
        GoogleAnalytics.getInstance(activity).reportActivityStop(activity);
        FlurryAgent.onEndSession(activity);
    }

    public static void send(String s, String s1, String s2)
    {
        getTracker(TrackerName.APP_TRACKER).send((new com.google.android.gms.Analytics.HitBuilders.EventBuilder()).setCategory(s).setAction(s1).setLabel(s2).build());
        HashMap hashmap = new HashMap();
        if(s1 != null)
        {
            hashmap.put("action", s1);
        }
        if(s2 != null)
        {
            hashmap.put("label", s2);
        }
        if(hashmap.size() > 0)
        {
            FlurryAgent.logEvent(s, hashmap);
            return;
        } else
        {
            FlurryAgent.logEvent(s);
            return;
        }
    }

    public static void sendPageView(String s)
    {
        getTracker(TrackerName.APP_TRACKER).setScreenName(s);
        getTracker(TrackerName.APP_TRACKER).send((new com.google.android.gms.Analytics.HitBuilders.AppViewBuilder()).build());
        FlurryAgent.onPageView();
    }*/

}
