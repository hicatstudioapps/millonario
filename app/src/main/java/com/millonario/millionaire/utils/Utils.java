package com.millonario.millionaire.utils;

import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.util.Base64;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.accessibility.AccessibilityManager;

import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.objetos.Pergunta;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

// Referenced classes of package com.millonario.millionaire.utils:
//            DBAdapter, MusicaFundo

public class Utils {

    private static final String SCREENREADER_INTENT_ACTION = "android.accessibilityservice.AccessibilityService";
    private static final String SCREENREADER_INTENT_CATEGORY = "android.accessibilityservice.category.FEEDBACK_SPOKEN";
    static String whitespace_charclass;
    static String whitespace_chars;

    private Utils() {
    }

    public static float DpToPx(int i) {
        Resources resources = App.getMyResources();
        return TypedValue.applyDimension(1, i, resources.getDisplayMetrics());
    }

    public static String XOR(String s, String s1) {
        return XOR(s, s1, true);
    }

    public static String XOR(String s, String s1, boolean flag) {
        if (s == null || s1 == null) {
            return null;
        }
        char ac[];
        char ac1[];
        int i;
        int j;
        char ac2[];
        int k;
        try {
            ac = s1.toCharArray();
            ac1 = s.toCharArray();
            i = ac1.length;
            j = ac.length;
            ac2 = new char[i];
        } catch (Exception exception) {
            return null;
        }
        k = 0;
        while (k < i) {
            ac2[k] = (char) (ac1[k] ^ ac[k % j]);
            k++;
        }
        return new String(ac2);
    }

    public static void adicionaAjudaExtra(int i) {
        String s = App.DB().getValue("NumAjudasExtra");
        if (isEmptyDB(s)) {
            s = "0";
            App.DB().insertValue(s, "NumAjudasExtra");
        }
        int j = i + Integer.parseInt(s);
        App.DB().insertValue((new StringBuilder()).append(j).toString(), "NumAjudasExtra");
    }

    public static boolean canGivePresent() {
        boolean flag = true;
        Calendar calendar = Calendar.getInstance();
        String s = (new StringBuilder("key_christmas_present_date_")).append(calendar.get(1)).append(calendar.get(2)).append(calendar.get(5)).toString();
        if (App.DB().getValue(s).equals("1")) {
            flag = false;
        }
        return flag;
    }

    public static String constroiStats() {
        int j;
        String s2;
        String s = App.DB().getValue("ids_perguntas");
        if (s.equals(""))
            return "";
        String s1 = "";
        String as[];
        SparseIntArray sparseintarray;
        int i;
        as = s.split(",");
        sparseintarray = new SparseIntArray();
        i = 0;
        while (i < as.length) {
            j = Integer.parseInt(as[i]);
            sparseintarray.put(j, 1 + sparseintarray.get(j));
            i++;
        }
        s1 = "";
        int k = 0;
        while (k < sparseintarray.size()) {
            int l = sparseintarray.keyAt(k);
            int i1 = sparseintarray.get(l);
            Pergunta pergunta = App.DB().getPerguntaStats(l);
            StringBuilder stringbuilder = new StringBuilder(String.valueOf(s1));
            if (s1.equals("")) {
                s2 = "";
            } else {
                s2 = "\n";
            }
            s1 = stringbuilder.append(s2).append(l).append(": ").append(i1).append(" - D:").append(pergunta.ID).append(" - ").append(pergunta.RespostaCerta).append("(").append(pergunta.RespostaErrada1).append(")/").append(pergunta.Pergunta).append("(").append(pergunta.RespostaErrada2).append(")").toString();
            k++;
        }
        return s1;
    }

    public static String constroiStringTempo(long l) {
        long l1 = (l / 1000L) % 60L;
        long l2 = (l / 60000L) % 60L;
        long l3 = (l / 0x36ee80L) % 24L;
        long l4 = l % 1000L;
        String s = "";
        if (l3 > 0L) {
            s = (new StringBuilder(String.valueOf(l3))).append("h").toString();
        }
        String s2;
        String s1;
        StringBuilder stringbuilder1;
        if (l2 > 0L) {
            StringBuilder stringbuilder = new StringBuilder(String.valueOf(s));
            if (s.equals("")) {
                s1 = "";
            } else {
                s1 = " ";
            }
            s = stringbuilder.append(s1).append(l2).append("m").toString();
        }
        stringbuilder1 = new StringBuilder(String.valueOf(s));
        if (s.equals("")) {
            s2 = "";
        } else {
            s2 = " ";
        }
        return stringbuilder1.append(s2).append(l1).append("s ").append(l4).append("ms").toString();
    }

    public static String converteDinheiro(String s) {
        String s2;
        int i;
        try {
            s2 = (new BigDecimal(s)).toPlainString();
            if (!s2.contains(".") && !s2.contains(",")) {
                //break MISSING_BLOCK_LABEL_60;
                return "0";
            }
            i = s2.indexOf(".");
            if (i != -1) {
                //break MISSING_BLOCK_LABEL_52;
                //return "0";
            }
            //i = s2.indexOf(",");
            s2 = s2.substring(0, i);
            String s3;
            long l = 100L * (long) Math.ceil(Long.parseLong(s2) / 100L);
            String s1;
            try {
                s3 = (new StringBuilder()).append(l).toString();
                s1 = s3;
            } catch (Exception e) {
                s1 = "0";
            }
            if (s1 == null || s1.equals("") || s1.equals("<null>")) {
                s1 = "0";
            }
            return s1;
        } catch (Exception e) {
            return "0";
        }
    }

    public static String desencriptaValor(String s) {
        String s1 = XOR(XOR(new String(Base64.decode(s, 0), Charset.forName("UTF-8")), "1475289631"), "alguma coisa para mudar isto").replace("Isto \351", "").replace(" meio ", "").replace("chato", "");
        String s2;
        try {
            s2 = URLDecoder.decode(s1, "UTF-8");
        } catch (Exception exception) {
            return s1;
        }
        return s2;
    }

    public static String encriptaValor(String s, String s1, String s2, String s3, String s4, String s5) {
        try {
            String s6 = URLEncoder.encode(s, "UTF-8");
            s = s6;
        } catch (Exception e) {
        }
        int i = s.length() / 2;
        return Base64.encodeToString(XOR(XOR((new StringBuilder(String.valueOf(s2))).append(s.substring(0, i)).append(s3).append(s.substring(i)).append(s4).toString(), s5), s1).getBytes(), 0);
    }

    public static int getDificuldadeAtual() {
        return App.PerguntaAtual / 3;
    }

    public static String getDinheiroTemp() {
        return converteDinheiro(App.DB().getValue("DinheiroTemp"));
    }

    public static int getNumeroAjudasExtra() {
        String s = App.DB().getValue("NumAjudasExtra");
        if (isEmptyDB(s)) {
            s = "0";
            App.DB().insertValue(s, "NumAjudasExtra");
        }
        return Integer.parseInt(s);
    }

    public static long getRecordLong() {
        return getRecordLong(1);
    }

    public static long getRecordLong(int i) {
        long l;
        try {
            l = Long.parseLong(App.DB().getValue("recordTempo"));
        } catch (Exception exception) {
            return -1L;
        }
        return l;
    }

    public static String getRecordTempo() {
        if (App.DB().getValue("recordTempo").equals("") || Long.parseLong(App.DB().getValue("recordTempo")) == -1L) {
            return "-";
        } else {
            return constroiStringTempo(Long.parseLong(App.DB().getValue("recordTempo")));
        }
    }

    public static String getTempoAtual() {
        long l = Long.parseLong(App.DB().getValue("tempoJogo"));
        return constroiStringTempo(System.currentTimeMillis() - l);
    }

    public static void iniciaTimer() {
        App.DB().insertValue((new StringBuilder()).append(System.currentTimeMillis()).toString(), "tempoJogo");
    }

    public static boolean isChristmas() {
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.set(calendar.get(1), 11, 1, 0, 0);
        calendar2.set(1 + calendar.get(1), 0, 1, 0, 0);
        return calendar.getTimeInMillis() >= calendar1.getTimeInMillis() && calendar.getTimeInMillis() < calendar2.getTimeInMillis();
    }

    public static boolean isEmpty(String s) {
        return removeWhiteSpaces(s).equals("");
    }

    public static boolean isEmptyDB(String s) {
        return removeWhiteSpaces(s).equals("") || s.equals("(null)") || s.equals("<null>");
    }

    private static boolean isScreenReaderActive_pre_and_postAPI14(Context context) {
        label0:
        {
            Intent intent = new Intent("android.accessibilityservice.AccessibilityService");
            intent.addCategory("android.accessibilityservice.category.FEEDBACK_SPOKEN");
            List list = context.getPackageManager().queryIntentServices(intent, 0);
            ContentResolver contentresolver = context.getContentResolver();
            ArrayList arraylist = new ArrayList();
            Iterator iterator = ((ActivityManager) context.getSystemService("activity")).getRunningServices(0x7fffffff).iterator();
            ResolveInfo resolveinfo;
            do {
                if (!iterator.hasNext()) {
                    Iterator iterator1 = list.iterator();
                    if (!iterator1.hasNext()) {
                        break label0;
                    }
                    resolveinfo = (ResolveInfo) iterator1.next();
                    Cursor cursor = contentresolver.query(Uri.parse((new StringBuilder("content://")).append(resolveinfo.serviceInfo.packageName).append(".providers.StatusProvider").toString()), null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        int i = cursor.getInt(0);
                        cursor.close();
                        return i == 1;
                    }
                    break;
                }
                arraylist.add(((android.app.ActivityManager.RunningServiceInfo) iterator.next()).service.getPackageName());
            } while (true);
            return arraylist.contains(resolveinfo.serviceInfo.packageName);
        }
        return false;
    }

    public static boolean isValidPassword(String s) {
        boolean flag = true;
        if (s == null || isEmpty(s)) {
            flag = false;
        }
        return flag;
    }

    public static boolean isVoiceOverActive() {
        boolean flag = ((AccessibilityManager) App.getContext().getSystemService("accessibility")).isEnabled();
        boolean flag1 = isScreenReaderActive_pre_and_postAPI14(App.getContext());
        return flag && flag1;
    }

    public static boolean openApp(String s, Context context) {
        if (s.equals(context.getPackageName())) {
            return false;
        }
        PackageManager packagemanager = context.getPackageManager();
        Intent intent;
        try {
            intent = packagemanager.getLaunchIntentForPackage(s);
        } catch (Exception namenotfoundexception) {
            return false;
        }
        if (intent != null) {
            intent.addCategory("android.intent.category.LAUNCHER");
            context.startActivity(intent);
            return true;
        }
        try {
            throw new android.content.pm.PackageManager.NameNotFoundException();
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }

    public static String paraTimer() {
        String s;
        long l;
        s = App.DB().getValue("tempoJogo");
        l = -1L;
        try {
            long l8 = Long.parseLong(s);
            l = l8;
        } catch (Exception e) {
        }
        long l1;
        String s1;
        if (l < 0L) {
            return "";
        }
        l1 = System.currentTimeMillis() - l;
        s1 = App.DB().getValue("recordTempo");
        long l2;
        try {
            long l7 = Long.parseLong(s1);
            l2 = l7;
        } catch (Exception e1) {
            l2 = -1L;
        }
        if (l1 < l2 || l2 == -1L) {
            App.DB().insertValue((new StringBuilder()).append(l1).toString(), "recordTempo");
        }
        long l3;
        try {
            long l6 = Long.parseLong(App.DB().getValue((new StringBuilder("TempoTemp-Mes")).append(1 + Calendar.getInstance().get(2)).toString()));
            l3 = l6;
        } catch (Exception e2) {
            l3 = -1L;
        }
        if (l1 < l3 || l3 == -1L) {
            App.DB().insertValue((new StringBuilder()).append(l1).toString(), (new StringBuilder("TempoTemp-Mes")).append(1 + Calendar.getInstance().get(2)).toString());
        }
        long l4;
        try {
            long l5 = Long.parseLong(App.DB().getValue((new StringBuilder("TempoTemp-Semana")).append(Calendar.getInstance().get(3)).toString()));
            l4 = l5;
        } catch (Exception e3) {
            l4 = -1L;
        }
        if (l1 < l4 || l4 == -1L) {
            App.DB().insertValue((new StringBuilder()).append(l1).toString(), (new StringBuilder("TempoTemp-Semana")).append(Calendar.getInstance().get(3)).toString());
        }
        return constroiStringTempo(l1);
    }

    public static void removeAjudaExtra() {
        adicionaAjudaExtra(-1);
    }

    public static String removeWhiteSpaces(String s) {
        if (s == null) {
            return "";
        } else {
            return s.toString().replaceAll(whitespace_charclass, "");
        }
    }

    public static String sha1Hash(String s) {
        try {
            int i = 0;
            byte abyte1[];
            StringBuilder stringbuilder;
            int j;
            MessageDigest messagedigest = MessageDigest.getInstance("SHA-1");
            byte abyte0[] = s.getBytes("UTF-8");
            messagedigest.update(abyte0, 0, abyte0.length);
            abyte1 = messagedigest.digest();
            stringbuilder = new StringBuilder();
            j = abyte1.length;
            while (i < j) {
                byte byte0 = abyte1[i];
                Object aobj[] = new Object[1];
                aobj[0] = Byte.valueOf(byte0);
                stringbuilder.append(String.format("%02X", aobj));
                i++;
            }
            String s2 = stringbuilder.toString();
            String s1 = s2;
            return s1.toLowerCase(Locale.ENGLISH);
        } catch (Exception e) {
            return null;
        }
    }

    static {
        whitespace_chars = "\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000";
        whitespace_charclass = (new StringBuilder("[")).append(whitespace_chars).append("]").toString();
    }
}
