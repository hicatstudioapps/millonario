package com.millonario.millionaire.utils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.millonario.millionaire.classes.App;

public class TestConnection {

    private static final int maxTries = 10;
    private static boolean stateOk = false;

    private TestConnection() {
    }

    private static boolean check() {
        if (connectionFound())
            return true;
        WifiManager wifimanager;
        int i;
        wifimanager = (WifiManager) App.getContext().getSystemService("wifi");
        stateOk = false;
        i = 0;
        while (true) {
            if (stateOk || i >= 10) {
                return i < 10 || stateOk ? false : false;
            }
            switch (wifimanager.getWifiState()) {
                default:
                    i++;
                    stateOk = false;
                    break;
                case 0:
                    i++;
                    stateOk = false;
                    break;
                case 1:
                    stateOk = true;
                    break;
                case 2:
                    i++;
                    stateOk = true;
                    break;
                case 3:
                    stateOk = true;
                    if (wifimanager.getConnectionInfo().getNetworkId() != -1)
                        return true;
                    i++;
                    break;
                case 4:
                    i++;
                    stateOk = false;
                    break;
            }
        }
    }

    public static boolean connectionFound() {
        NetworkInfo networkinfo = ((ConnectivityManager) App.getContext().getSystemService("connectivity")).getActiveNetworkInfo();
        return networkinfo != null && networkinfo.isConnectedOrConnecting();
    }

    public static boolean test() {
        return check();
    }

}
