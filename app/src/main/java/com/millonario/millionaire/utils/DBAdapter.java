package com.millonario.millionaire.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.SparseIntArray;


import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.objetos.CrossApp;
import com.millonario.millionaire.objetos.Pergunta;
import com.millonario.millionaire.utils.youtube2.Brazil;
import com.millonario.millionaire.utils.youtube2.English;
import com.millonario.millionaire.utils.youtube2.FR;
import com.millonario.millionaire.utils.youtube2.German;
import com.millonario.millionaire.utils.youtube2.IT;
import com.millonario.millionaire.utils.youtube2.Spanish;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import nl.matshofman.saxrssreader.RssFeed;
import nl.matshofman.saxrssreader.RssItem;
import nl.matshofman.saxrssreader.RssReader;

// Referenced classes of package com.millonario.millionaire.utils:
//            Utils, FileCopy, TestConnection, AcessoExterno, 
//            DB, DBExtend

@SuppressWarnings("ResourceType")
public class DBAdapter {
    private static final int DATABASE_VERSION = 76;
    private static final String TAG = "DBAdapter";
    private static String DATABASE_NAME = "es";
    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;
    private ArrayList savedValues;
    public DBAdapter(Context context1) {
        context = context1;
        switch (context.getResources().getConfiguration().locale.getLanguage()) {
            case "fr": {
                DB.INSERT_DEFAULT = FR.DEFAULT;
                DBAdapter.DATABASE_NAME = "fr";
                break;
            }
            case "it": {
                DB.INSERT_DEFAULT = IT.DEFAULT_ES;
                DBAdapter.DATABASE_NAME = "it";
                break;
            }
            case "de": {
                DB.INSERT_DEFAULT = German.DEFAULT_DT;
                DBAdapter.DATABASE_NAME = "de";
                break;
            }
            case "pt": {
                DB.INSERT_DEFAULT = Brazil.DEFAULT;
                DBAdapter.DATABASE_NAME = "br";
                break;
            }
            case "es": {
                DB.INSERT_DEFAULT = Spanish.DEFAULT_ES;
                DBAdapter.DATABASE_NAME = "es";
                break;
            }
            default: {
                DB.INSERT_DEFAULT = English.DEFAULT;
                DBAdapter.DATABASE_NAME = "en";
                break;
            }
        }
        DBHelper = new DatabaseHelper(context);
    }

    private ArrayList get5PerguntasPorDificuldade(int i) {
        ArrayList arraylist;
        SparseIntArray sparseintarray;
        Cursor cursor;
        arraylist = new ArrayList();
        sparseintarray = new SparseIntArray();
        sparseintarray.append(1, 0);
        sparseintarray.append(2, 0);
        sparseintarray.append(3, 0);
        sparseintarray.append(4, 0);
        sparseintarray.append(5, 0);
        sparseintarray.append(6, 0);
        cursor = db.rawQuery((new StringBuilder("SELECT p.idPergunta, p.idCategoria, IFNULL(e.numeroAcertou, 0) as nc, IFNULL(e.numeroApareceu, 0) as na FROM pergunta as p LEFT OUTER JOIN estatisticas_temp as e ON p.idPergunta = e.idPergunta WHERE (idDificuldadeNovo = '")).append(i).append("') ORDER BY na ASC, RANDOM()").toString(), null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                int j = cursor.getInt(cursor.getColumnIndex("idCategoria"));
                if (sparseintarray.get(j) < 1) {
                    sparseintarray.put(j, 1 + sparseintarray.get(j));
                    arraylist.add(Integer.valueOf(cursor.getInt(cursor.getColumnIndex("idPergunta"))));
                }
                if (arraylist.size() == 5)
                    break;
                cursor.moveToNext();
            }
        }
        cursor.close();
        return arraylist;
    }

    public void apagaEstatisticas() {
        db.execSQL("DELETE FROM estatisticas WHERE 1 = 1");
    }

    public void close() {
        DBHelper.close();
    }

    public void deletePerguntasNotIn(ArrayList arraylist) {
        String s = "";
        Iterator iterator = arraylist.iterator();
        do {
            if (!iterator.hasNext()) {
                String s1 = (new StringBuilder("DELETE FROM pergunta WHERE idPergunta NOT IN (")).append(s).append(")").toString();
                db.execSQL(s1);
                return;
            }
            Integer integer = (Integer) iterator.next();
            if (s.equals("")) {
                s = (new StringBuilder("'")).append(integer).append("'").toString();
            } else {
                s = (new StringBuilder(String.valueOf(s))).append(",'").append(integer).append("'").toString();
            }
        } while (true);
    }

    public void finalizaTransacao(boolean flag) {
        if (flag) {
            db.setTransactionSuccessful();
        }
        db.endTransaction();
    }

    public ArrayList getApps() {
        ArrayList arraylist = new ArrayList();
        Cursor cursor = db.rawQuery("SELECT * From app ORDER BY RANDOM();", null);
        do {
            if (!cursor.moveToNext()) {
                return arraylist;
            }
            CrossApp crossapp = new CrossApp();
            crossapp.id = cursor.getInt(cursor.getColumnIndex("id"));
            crossapp.img = cursor.getString(cursor.getColumnIndex("img"));
            crossapp.url = cursor.getString(cursor.getColumnIndex("url"));
            crossapp.name = cursor.getString(cursor.getColumnIndex("name"));
            arraylist.add(crossapp);
        } while (true);
    }

    public String getEstatisticas() {
        String s = "";
        Cursor cursor = db.rawQuery("SELECT * FROM estatisticas", null);
        do {
            if (!cursor.moveToNext()) {
                cursor.close();
                return s;
            }
            if (s.equals("")) {
                s = (new StringBuilder(String.valueOf(cursor.getInt(cursor.getColumnIndex("idPergunta"))))).append(",").append(cursor.getInt(cursor.getColumnIndex("numeroApareceu"))).append(",").append(cursor.getInt(cursor.getColumnIndex("numeroAcertou"))).toString();
            } else {
                s = (new StringBuilder(String.valueOf(s))).append(" ").append(cursor.getInt(cursor.getColumnIndex("idPergunta"))).append(",").append(cursor.getInt(cursor.getColumnIndex("numeroApareceu"))).append(",").append(cursor.getInt(cursor.getColumnIndex("numeroAcertou"))).toString();
            }
        } while (true);
    }

    public Pergunta getPergunta(int i) {
        Cursor cursor = db.rawQuery((new StringBuilder("SELECT * FROM pergunta WHERE idPergunta = '")).append(i).append("';").toString(), null);
        boolean flag = cursor.moveToFirst();
        Pergunta pergunta = null;
        if (flag) {
            pergunta = new Pergunta();
            pergunta.ID = cursor.getInt(cursor.getColumnIndex("idPergunta"));
            pergunta.Pergunta = cursor.getString(cursor.getColumnIndex("Pergunta"));
            pergunta.RespostaCerta = cursor.getString(cursor.getColumnIndex("Resposta_correta"));
            pergunta.RespostaErrada1 = cursor.getString(cursor.getColumnIndex("Resposta_errada_1"));
            pergunta.RespostaErrada2 = cursor.getString(cursor.getColumnIndex("Resposta_errada_2"));
            pergunta.RespostaErrada3 = cursor.getString(cursor.getColumnIndex("Resposta_errada_3"));
            pergunta.Dificuldade = cursor.getInt(cursor.getColumnIndex("idDificuldadeNovo"));
            cursor.close();
        }
        return pergunta;
    }

    public Pergunta getPerguntaStats(int i) {
        Cursor cursor = db.rawQuery((new StringBuilder("SELECT p.*, (case e.numeroAcertou when null then '-1' else e.numeroAcertou end) " +
                "as nc, (case e.numeroApareceu when null then '-1' else e.numeroApareceu end) as " +
                "na FROM pergunta as p LEFT OUTER JOIN estatisticas_temp as e ON p.idPergunta = e" +
                ".idPergunta WHERE p.idPergunta = '"
        )).append(i).append("';").toString(), null);
        boolean flag = cursor.moveToFirst();
        Pergunta pergunta = null;
        if (flag) {
            pergunta = new Pergunta();
            pergunta.ID = cursor.getInt(cursor.getColumnIndex("idDificuldade"));
            pergunta.Pergunta = (new StringBuilder()).append(cursor.getInt(cursor.getColumnIndex("numeroApareceu"))).toString();
            pergunta.RespostaCerta = (new StringBuilder()).append(cursor.getInt(cursor.getColumnIndex("numeroCerto"))).toString();
            pergunta.RespostaErrada1 = (new StringBuilder()).append(cursor.getInt(cursor.getColumnIndex("nc"))).toString();
            pergunta.RespostaErrada2 = (new StringBuilder()).append(cursor.getInt(cursor.getColumnIndex("na"))).toString();
            cursor.close();
        }
        return pergunta;
    }

    public ArrayList getPerguntas() {
        ArrayList arraylist = new ArrayList();
        arraylist.addAll(get5PerguntasPorDificuldade(1));
        arraylist.addAll(get5PerguntasPorDificuldade(2));
        arraylist.addAll(get5PerguntasPorDificuldade(3));
        arraylist.addAll(get5PerguntasPorDificuldade(4));
        arraylist.addAll(get5PerguntasPorDificuldade(5));
        return arraylist;
    }

    public String getValue(String s) {
        Cursor cursor = db.rawQuery((new StringBuilder("SELECT * FROM SavedValues WHERE Name = '")).append(s).append("';").toString(), null);
        if (cursor.moveToFirst()) {
            String s1 = cursor.getString(cursor.getColumnIndex("Value"));
            cursor.close();
            return s1;
        } else {
            return "";
        }
    }

    public int getVersao() {
        String s = getValue("Versao");
        if (s.equals("")) {
            return -1;
        } else {
            return Integer.parseInt(s);
        }
    }

    public int getVersaoPergunta(int i) {
        String s = (new StringBuilder("SELECT Versao FROM pergunta WHERE idPergunta = '")).append(i).append("';").toString();
        Cursor cursor = db.rawQuery(s, null);
        int j;
        if (cursor.moveToFirst()) {
            j = cursor.getInt(cursor.getColumnIndex("Versao"));
        } else {
            j = -1;
        }
        cursor.close();
        return j;
    }

    public void iniciaTransacao() {
        db.beginTransaction();
    }

    public void insertValue(String s, String s1) {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("Name", s1);
        contentvalues.put("Value", s);
        db.insertWithOnConflict("SavedValues", null, contentvalues, 5);
    }

    public void logout() {
        insertValue("", "Username");
        insertValue("", "CodigoSync");
        insertValue("", "idUtilizador");
        insertValue("", "idFacebook");
        insertValue("", "DinheiroTemp");
        insertValue("", "recordTempo");
        insertValue("", "AutorizacaoEnviarPergunta");
        insertValue("0", (new StringBuilder("DinheiroTemp-Semana")).append(Calendar.getInstance().get(3)).toString());
        insertValue("0", (new StringBuilder("DinheiroTemp-Mes")).append(1 + Calendar.getInstance().get(2)).toString());
        insertValue("", (new StringBuilder("TempoTemp-Semana")).append(Calendar.getInstance().get(3)).toString());
        insertValue("", (new StringBuilder("TempoTemp-Mes")).append(1 + Calendar.getInstance().get(2)).toString());
        insertValue("0", "alterouPassword");
        insertValue("", "FotoUtilizador");

    }

    public DBAdapter open()
            throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    public boolean updateCrossApps() {
        String s4;
        String s6;
        String s8;
        int j;
        String s9;
        int k;
        boolean flag;
        try {
            if (getValue("key_cross_promotion_is_loading").equals("1")) {
                return false;
            }
            Iterator iterator;
            insertValue("1", "key_cross_promotion_is_loading");
            RssFeed rssfeed = RssReader.read(new URL(App.getMyString(R.string.url_feed_more_apps)));
            insertValue(rssfeed.getDescription(), "key_cross_promotion_num_downloads");
            iterator = rssfeed.getRssItems().iterator();
            while (true) {
                boolean flag1 = iterator.hasNext();
                if (flag1) {
                    RssItem rssitem = (RssItem) iterator.next();
                    String s = "";
                    String s1;
                    int i;
                    s1 = App.getMyString(R.string.lingua);
                    i = rssitem.getGuid().indexOf("?p=");
                    if (i <= 0) {
                        //break MISSING_BLOCK_LABEL_154;
                        break;
                    }
                    s = rssitem.getGuid().substring(i + "?p=".length());
                    String s2 = rssitem.getContent();
                    String s3 = s2.substring(s2.indexOf("<a href=\"") + "<a href=\"".length());
                    s4 = s3.substring(0, s3.indexOf("\""));
                    String s5 = s3.substring(s3.indexOf("<img") + "<img".length());
                    s6 = s5.substring(0, s5.indexOf(">"));
                    String s7 = s6.substring(s6.indexOf("src=\"") + "src=\"".length());
                    s8 = s7.substring(0, s7.indexOf("\""));
                    j = s6.indexOf("alt=\"");
                    if (j > 0) {
                        s9 = s6.substring(j + "alt=\"".length());
                        k = s9.indexOf("\"");
                        if (k <= 0) {
                            String s10 = App.getMyString(R.string.lingua);
                            s1 = s10;
                        } else
                            s1 = s9.substring(0, k);
                        if (s1.length() > 2) {
                            s1 = App.getMyString(R.string.lingua);
                        }
                    }
                    if (!Utils.isEmpty(s) && s1.equals(App.getMyString(R.string.lingua))) {
                        ContentValues contentvalues = new ContentValues();
                        contentvalues.put("id", s);
                        contentvalues.put("img", FileCopy.downloadImg(s8));
                        contentvalues.put("url", s4);
                        contentvalues.put("name", rssitem.getTitle());
                        db.insertWithOnConflict("app", null, contentvalues, 5);
                    }
                }
            }
            flag = true;
        } catch (Exception e) {
            flag = false;
        }
        insertValue("0", "key_cross_promotion_is_loading");
        return flag;
    }

    public void updateDinheiro(float f) {
        String s1;
        String s2;
        String s3;
        float f2;
        float f3;
        float f4;
        if (f >= 1.0F) {
            float f1 = f;
            String s = getValue("Dinheiro");
            if (s != null && !s.equals("") && !s.equals("<null>")) {
                float f5 = Float.parseFloat(s);
                if (f5 > 0.0F) {
                    f1 = f + f5;
                } else {
                    f1 = f;
                }
            }
            insertValue((new StringBuilder()).append(f1).toString(), "Dinheiro");
            s1 = getValue("DinheiroTemp");
            if (s1 != null && !s1.equals("") && !s1.equals("<null>")) {
                f4 = Float.parseFloat(s1);
                if (f4 > 0.0F) {
                    f1 = f + f4;
                } else {
                    f1 = f;
                }
            }
            insertValue((new StringBuilder()).append(f1).toString(), "DinheiroTemp");
            s2 = getValue((new StringBuilder("DinheiroTemp-Semana")).append(Calendar.getInstance().get(3)).toString());
            if (s2 != null && !s2.equals("") && !s2.equals("<null>")) {
                f3 = Float.parseFloat(s2);
                if (f3 > 0.0F) {
                    f1 = f + f3;
                } else {
                    f1 = f;
                }
            }
            insertValue((new StringBuilder()).append(f1).toString(), (new StringBuilder("DinheiroTemp-Semana")).append(Calendar.getInstance().get(3)).toString());
            s3 = getValue((new StringBuilder("DinheiroTemp-Mes")).append(1 + Calendar.getInstance().get(2)).toString());
            if (s3 != null && !s3.equals("") && !s3.equals("<null>")) {
                f2 = Float.parseFloat(s3);
                if (f2 > 0.0F) {
                    f1 = f + f2;
                } else {
                    f1 = f;
                }
            }
            insertValue((new StringBuilder()).append(f1).toString(), (new StringBuilder("DinheiroTemp-Mes")).append(1 + Calendar.getInstance().get(2)).toString());

        }
    }

    public void updatePergunta(int i, boolean flag) {
        String s;
        Cursor cursor;
        Cursor cursor1;
        String s1;
        String s2;
        if (flag) {
            db.execSQL((new StringBuilder("UPDATE pergunta SET numeroApareceu = numeroApareceu + 1, numeroCerto = numeroCerto + 1 WHERE idPergunta = '")).append(i).append("'").toString());
        } else {
            db.execSQL((new StringBuilder("UPDATE pergunta SET numeroApareceu = numeroApareceu + 1 WHERE idPergunta = '")).append(i).append("'").toString());
        }
        s = (new StringBuilder("SELECT COUNT(idPergunta) as count FROM estatisticas WHERE idPergunta = '")).append(i).append("'").toString();
        cursor = db.rawQuery(s, null);
        if (cursor.moveToFirst() && cursor.getInt(cursor.getColumnIndex("count")) > 0) {
            cursor.close();
            if (flag) {
                db.execSQL((new StringBuilder("UPDATE estatisticas SET numeroApareceu = numeroApareceu + 1, numeroAcertou = numeroAcertou + 1 WHERE idPergunta = '")).append(i).append("'").toString());
            } else {
                db.execSQL((new StringBuilder("UPDATE estatisticas SET numeroApareceu = numeroApareceu + 1 WHERE idPergunta = '")).append(i).append("'").toString());
            }
        } else {
            cursor.close();
            SQLiteDatabase sqlitedatabase = db;
            StringBuilder stringbuilder = (new StringBuilder("INSERT INTO estatisticas(idPergunta, numeroApareceu, numeroAcertou) VALUES('")).append(i).append("', '1', ");
            if (flag) {
                s1 = "'1'";
            } else {
                s1 = "'0'";
            }
            sqlitedatabase.execSQL(stringbuilder.append(s1).append(")").toString());
        }
        s2 = (new StringBuilder("SELECT COUNT(idPergunta) as count FROM estatisticas_temp WHERE idPergunta = '")).append(i).append("'").toString();
        cursor1 = db.rawQuery(s2, null);
        if (cursor1.moveToFirst() && cursor1.getInt(cursor1.getColumnIndex("count")) > 0) {
            cursor1.close();
            if (flag) {
                db.execSQL((new StringBuilder("UPDATE estatisticas_temp SET numeroApareceu = numeroApareceu + 1, numeroAcertou = numeroAcertou + 1 WHERE idPergunta = '")).append(i).append("'").toString());
                return;
            } else {
                db.execSQL((new StringBuilder("UPDATE estatisticas_temp SET numeroApareceu = numeroApareceu + 1 WHERE idPergunta = '")).append(i).append("'").toString());
                return;
            }
        }
        cursor1.close();
        SQLiteDatabase sqlitedatabase1 = db;
        StringBuilder stringbuilder1 = (new StringBuilder("INSERT INTO estatisticas_temp(idPergunta, numeroApareceu, numeroAcertou) VALUES('")).append(i).append("', '1', ");
        String s3;
        if (flag) {
            s3 = "'1'";
        } else {
            s3 = "'0'";
        }
        sqlitedatabase1.execSQL(stringbuilder1.append(s3).append(")").toString());
    }


    private class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context1) {
            super(context1, DBAdapter.DATABASE_NAME, null, 76);
        }

        public void onCreate(SQLiteDatabase sqlitedatabase) {
            int i = 0;
            while (i < DB.CREATE_DB.length) {
                sqlitedatabase.execSQL(DB.CREATE_DB[i]);
                i++;
            }
            boolean flag;
            try {
                Class.forName((new StringBuilder(String.valueOf(App.getContext().getPackageName()))).append(".DBExtend").toString());
                flag = true;
            } catch (ClassNotFoundException classnotfoundexception) {
                flag = false;
            }
            try {
                if (!flag) {
                    int j = 0;
                    while (j < DB.INSERT_DEFAULT.length) {
                        sqlitedatabase.execSQL(DB.INSERT_DEFAULT[j]);
                        j++;
                    }
                } else {
                    DBExtend dbextend = (DBExtend) Class.forName((new StringBuilder(String.valueOf(App.getContext().getPackageName()))).append(".DBExtend").toString()).newInstance();
                    int l = 0;
                    int i1 = dbextend.INSERT_DEFAULT.length;
                    while (l < i1) {
                        sqlitedatabase.execSQL(dbextend.INSERT_DEFAULT[l]);
                        l++;
                    }
                }
                int k = 0;
                while (k < DB.INSERT_DEFAULT_CROSS_PROMOTION.length) {
                    sqlitedatabase.execSQL(DB.INSERT_DEFAULT_CROSS_PROMOTION[k]);
                    k++;
                }
                if (savedValues == null)
                    return;
                Iterator iterator = savedValues.iterator();
                while (iterator.hasNext()) {
                    ContentValues contentvalues;
                    Cursor cursor;
                    contentvalues = (ContentValues) iterator.next();
                    if (!contentvalues.get("Name").equals("VersaoOnline")) {
                        return;
                    }
                    cursor = sqlitedatabase.rawQuery("SELECT * FROM SavedValues WHERE Name = 'VersaoOnline'", null);
                    if (!cursor.moveToFirst() || cursor.getInt(cursor.getColumnIndex("Value")) <= Integer.parseInt((new StringBuilder()).append(contentvalues.get("Value")).toString())) {
                        cursor.close();
                        sqlitedatabase.insertWithOnConflict("SavedValues", null, contentvalues, 5);
                    } else
                        cursor.close();
                }
                savedValues = null;
            } catch (Exception e) {
            }
        }

        public void onUpgrade(SQLiteDatabase sqlitedatabase, int i, int j) {
           /* Cursor cursor = sqlitedatabase.rawQuery("SELECT * FROM SavedValues", null);
            if(cursor.moveToFirst()){
	            savedValues = new ArrayList();
	            while(true){
		            boolean flag = cursor.isAfterLast();
		            if(!flag){
		            	ContentValues contentvalues = new ContentValues();
		                contentvalues.put("Name", cursor.getString(cursor.getColumnIndex("Name")));
		                contentvalues.put("Value", cursor.getString(cursor.getColumnIndex("Value")));
		                savedValues.add(contentvalues);
		                cursor.moveToNext();
		                continue;
		            }
		            break;
	            }
            }
            int k = 0;
            while(k < DB.DB_TABLES.length)
            {
            	sqlitedatabase.execSQL((new StringBuilder("DROP TABLE IF EXISTS ")).append(DB.DB_TABLES[k]).toString());
                k++;

            }
            onCreate(sqlitedatabase);*/
        }
    }


}
