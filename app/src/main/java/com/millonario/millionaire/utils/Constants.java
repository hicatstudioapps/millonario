package com.millonario.millionaire.utils;


public class Constants {

    public static final String CHRISTMAS_PRESENT_DATE = "key_christmas_present_date_";
    public static final String CROSS_PROMOTION_IS_LOADING = "key_cross_promotion_is_loading";
    public static final String CROSS_PROMOTION_NUM_DOWNLOADS = "key_cross_promotion_num_downloads";
    public static final String CROSS_PROMOTION_UPDATE_DATE = "key_cross_promotion_last_update";

    public Constants() {
    }
}
