package com.millonario.millionaire.utils;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.chartboost.sdk.Chartboost.CBAgeGateConfirmation;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Model.CBError.CBClickError;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyAd;
import com.jirbo.adcolony.AdColonyAdListener;
import com.jirbo.adcolony.AdColonyV4VCAd;
import com.jirbo.adcolony.AdColonyV4VCListener;
import com.jirbo.adcolony.AdColonyV4VCReward;
import com.millonario.millionaire.AjudasExtraActivity;
import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.interfaces.VideoRewardInterface;
import com.millonario.millionaire.popup.PopUp;

public class Ads
        implements ChartboostDelegate, AdColonyV4VCListener, AdColonyAdListener {
    public static final String TAG_ADCOLONY = "AdColonyAds_tag";
    public static final String TAG_CHARTBOOST = "ChartboostAds_tag";
    private static final Ads _instance = new Ads();
    private static int $SWITCH_TABLE$com$creactive$quemquersermilionario$lib$utils$Ads$AdSourceType[];
    private static VideoRewardInterface _delegate;
    private static AdColonyV4VCAd video;
    private static boolean isInited = false;
    public Ads() {
    }

    static int[] $SWITCH_TABLE$com$creactive$quemquersermilionario$lib$utils$Ads$AdSourceType() {
        int ai[] = $SWITCH_TABLE$com$creactive$quemquersermilionario$lib$utils$Ads$AdSourceType;
        if (ai != null) {
            return ai;
        }
        int ai1[] = new int[AdSourceType.values().length];
        try {
            ai1[AdSourceType.ADCOLONY.ordinal()] = 2;
        } catch (NoSuchFieldError nosuchfielderror) {
        }
        try {
            ai1[AdSourceType.CHARTBOOST.ordinal()] = 1;
        } catch (NoSuchFieldError nosuchfielderror1) {
        }
        try {
            ai1[AdSourceType.INTERNAL_AD.ordinal()] = 3;
        } catch (NoSuchFieldError nosuchfielderror2) {
        }
        $SWITCH_TABLE$com$creactive$quemquersermilionario$lib$utils$Ads$AdSourceType = ai1;
        return ai1;
    }

    public static void init(Activity activity) {
        String s = App.getMyString(R.string.adcolony_app_id);
        String as[] = new String[1];
        as[0] = App.getMyString(R.string.adcolony_zone_id);
        AdColony.configure(activity, "version:2.1,store:google", s, as);
        AdColony.addV4VCListener(_instance);
        video =new AdColonyV4VCAd(App.getMyString(R.string.adcolony_zone_id));
        video.withConfirmationDialog().withResultsDialog();
        video.withListener(_instance);
        isInited = true;
    }

    public static boolean onBackPressed() {
        if (isInited) ;
        return false;
    }

    public static void onCreate(Activity activity) {
        if (isInited) ;
    }

    public static void onDestroy(Activity activity) {
        if (isInited) ;
    }

    public static void onPause(Activity activity) {
        if (!isInited) {
            return;
        } else {
            AdColony.pause();
            return;
        }
    }

    public static void onResume(Activity activity) {
        if (!isInited) {
            return;
        } else {
            AdColony.resume(activity);
            return;
        }
    }

    public static void onStart(Activity activity) {
        if (isInited) ;
    }

    public static void onStop(Activity activity) {
        if (isInited) ;
    }

    public static void showVideoReward(VideoRewardInterface videorewardinterface) {
        if(!video.isReady()){
        PopUp.show((AjudasExtraActivity)videorewardinterface, 0, 1);
        PopUp.setMensagem(App.getContext().getString(R.string.video));
        PopUp.setTextoEListenerBotao(App.getContext().getString(R.string.ok), new android.view.View.OnClickListener() {
            public void onClick(View view)
            {
                PopUp.hide();
            }
        }, 1);}
        else
        if (isInited) {
            _delegate = videorewardinterface;
            if (_delegate != null) {
                showVideoRewardFrom(AdSourceType.ADCOLONY);
                return;
            }
        }
    }

    private static void showVideoRewardFrom(AdSourceType adsourcetype) {
        switch ($SWITCH_TABLE$com$creactive$quemquersermilionario$lib$utils$Ads$AdSourceType()[adsourcetype.ordinal()]) {
            case 1: // '\001'
            default:
                return;

            case 2: // '\002'
                video.show();
                return;
            case 3: // '\003'
                break;
        }
        if (_delegate != null) {
            _delegate.onInternalVideoPlay();
        }
        _delegate = null;
    }

    public void didCompleteRewardedVideo(String s, int i) {
        Object aobj[] = new Object[2];
        if (s == null) {
            s = "null";
        }
        aobj[0] = s;
        aobj[1] = Integer.valueOf(i);
        Log.i("ChartboostAds_tag", String.format("DID COMPLETE REWARDED VIDEO '%s' FOR REWARD %d", aobj));
        if (_delegate != null) {
            _delegate.onRewardVideoFinished(true, "Chartboost");
        }
        _delegate = null;
    }

    public void didDismissRewardedVideo(String s) {
        Object aobj[] = new Object[1];
        if (s == null) {
            s = "null";
        }
        aobj[0] = s;
        Log.i("ChartboostAds_tag", String.format("DID DISMISS REWARDED VIDEO '%s'", aobj));
        if (_delegate != null) {
            _delegate.onRewardVideoFinished(false, "Chartboost");
        }
        _delegate = null;
    }

    public void didFailToLoadRewardedVideo(String s, com.chartboost.sdk.Model.CBError.CBImpressionError cbimpressionerror) {
        Object aobj[] = new Object[2];
        if (s == null) {
            s = "null";
        }
        aobj[0] = s;
        aobj[1] = cbimpressionerror.name();
        Log.i("ChartboostAds_tag", String.format("DID FAIL TO LOAD REWARDED VIDEO: '%s', Error:  %s", aobj));
        showVideoRewardFrom(AdSourceType.ADCOLONY);
    }

    public void onAdColonyAdAttemptFinished(AdColonyAd adcolonyad) {
        if (!adcolonyad.shown()) {
            //showVideoRewardFrom(AdSourceType.INTERNAL_AD);
        }
    }

    public void onAdColonyAdStarted(AdColonyAd adcolonyad) {
        if (_delegate != null) {
            _delegate.onWillDisplayRewardVideo();
        }
    }

    public void onAdColonyV4VCReward(AdColonyV4VCReward adcolonyv4vcreward) {
        if (_delegate != null && adcolonyv4vcreward.success()) {
            _delegate.onRewardVideoFinished(true, "AdColony");
        }
        _delegate = null;
    }

    public boolean shouldDisplayRewardedVideo(String s) {
        return true;
    }

    public void willDisplayVideo(String s) {
        Log.i("ChartboostAds_tag", String.format("WILL DISPLAY VIDEO '%s", new Object[]{
                s
        }));
        if (_delegate != null) {
            _delegate.onWillDisplayRewardVideo();
        }
    }

    @Override
    public void didCacheInterstitial(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void didCacheMoreApps() {
        // TODO Auto-generated method stub

    }

    @Override
    public void didClickInterstitial(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void didClickMoreApps() {
        // TODO Auto-generated method stub

    }

    @Override
    public void didCloseInterstitial(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void didCloseMoreApps() {
        // TODO Auto-generated method stub

    }

    @Override
    public void didDismissInterstitial(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void didDismissMoreApps() {
        // TODO Auto-generated method stub

    }

    @Override
    public void didFailToLoadInterstitial(String arg0, CBImpressionError arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void didFailToLoadMoreApps(CBImpressionError arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void didFailToRecordClick(String arg0, CBClickError arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void didShowInterstitial(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void didShowMoreApps() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean shouldDisplayInterstitial(String arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean shouldDisplayLoadingViewForMoreApps() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean shouldDisplayMoreApps() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean shouldPauseClickForConfirmation(CBAgeGateConfirmation arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean shouldRequestInterstitial(String arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean shouldRequestInterstitialsInFirstSession() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean shouldRequestMoreApps() {
        // TODO Auto-generated method stub
        return false;
    }

    private enum AdSourceType {
        CHARTBOOST("CHARTBOOST", 0),
        ADCOLONY("ADCOLONY", 1),
        INTERNAL_AD("INTERNAL_AD", 2);

        static {
            AdSourceType aadsourcetype[] = new AdSourceType[3];
            aadsourcetype[0] = CHARTBOOST;
            aadsourcetype[1] = ADCOLONY;
            aadsourcetype[2] = INTERNAL_AD;
        }

        private AdSourceType(String s, int i) {
        }
    }

}
