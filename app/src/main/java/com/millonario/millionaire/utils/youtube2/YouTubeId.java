package com.millonario.millionaire.utils.youtube2;


public abstract class YouTubeId {

    protected String mId;

    public YouTubeId(String s) {
        mId = s;
    }

    public String getId() {
        return mId;
    }
}
