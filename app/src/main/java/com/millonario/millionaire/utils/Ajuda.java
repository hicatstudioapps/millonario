package com.millonario.millionaire.utils;

import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.objetos.Pergunta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Ajuda {

    static int pCerta[] = {
            90, 80, 60, 40, 30
    };
    static int pErrada[];

    public Ajuda() {
    }

    public static int[] AjudaPublico(Pergunta pergunta, int i) {
        ArrayList arraylist = getRandomArray(pergunta, i);
        ArrayList arraylist1 = pergunta.getRespostas();
        int j = 0;
        int k = 0;
        int l = 0;
        int i1 = 0;
        int j1 = arraylist.size();
        Random random = new Random(System.currentTimeMillis());
        do {
            if (i1 + (l + (j + k)) >= 100) {
                return (new int[]{
                        j, k, l, i1
                });
            }
            int k1 = random.nextInt(j1);
            if (((String) arraylist.get(k1)).equals(arraylist1.get(0))) {
                j++;
            } else if (((String) arraylist.get(k1)).equals(arraylist1.get(1))) {
                k++;
            } else if (((String) arraylist.get(k1)).equals(arraylist1.get(2))) {
                l++;
            } else if (((String) arraylist.get(k1)).equals(arraylist1.get(3))) {
                i1++;
            }
        } while (true);
    }

    public static String AjudaTelefone(Pergunta pergunta, int i, String as[]) {
        String as1[];
        int ai[];
        ArrayList arraylist2;
        int j2;
        String s;
        String s1;
        int k2;
        ArrayList arraylist;
        ArrayList arraylist1;
        int j;
        int k;
        int l;
        int i1;
        int j1;
        int k1;
        Random random = new Random(System.currentTimeMillis());
        if (as.length != 1) {
            arraylist = getRandomArray(pergunta, i);
            arraylist1 = pergunta.getRespostas();
            j = 0;
            k = 0;
            l = 0;
            i1 = 0;
            j1 = arraylist.size();
            while (i1 + (l + (j + k)) < 100) {
                k1 = random.nextInt(j1);
                if (((String) arraylist.get(k1)).equals(arraylist1.get(0))) {
                    j++;
                } else if (((String) arraylist.get(k1)).equals(arraylist1.get(1))) {
                    k++;
                } else if (((String) arraylist.get(k1)).equals(arraylist1.get(2))) {
                    l++;
                } else if (((String) arraylist.get(k1)).equals(arraylist1.get(3))) {
                    i1++;
                }
            }
            if (j > k) {
                if (j > l) {
                    if (j > i1) {
                        s = as[0];
                    } else {
                        try {
                            s = as[3];
                        } catch (Exception exception18) {
                            try {
                                s = as[2];
                            } catch (Exception exception19) {
                                try {
                                    s = as[1];
                                } catch (Exception exception20) {
                                    try {
                                        s = as[0];
                                    } catch (Exception exception21) {
                                        s = "A";
                                    }
                                }
                            }
                        }
                    }
                } else if (l > i1) {
                    try {
                        s = as[2];
                    } catch (Exception exception15) {
                        try {
                            s = as[1];
                        } catch (Exception exception16) {
                            try {
                                s = as[0];
                            } catch (Exception exception17) {
                                s = "A";
                            }
                        }
                    }
                } else {
                    try {
                        s = as[3];
                    } catch (Exception exception11) {
                        try {
                            s = as[2];
                        } catch (Exception exception12) {
                            try {
                                s = as[1];
                            } catch (Exception exception13) {
                                try {
                                    s = as[0];
                                } catch (Exception exception14) {
                                    s = "A";
                                }
                            }
                        }
                    }
                }
            } else if (k > l) {
                if (k > i1) {
                    try {
                        s = as[1];
                    } catch (Exception exception9) {
                        try {
                            s = as[0];
                        } catch (Exception exception10) {
                            s = "A";
                        }
                    }
                } else {
                    try {
                        s = as[2];
                    } catch (Exception exception6) {
                        try {
                            s = as[1];
                        } catch (Exception exception7) {
                            try {
                                s = as[0];
                            } catch (Exception exception8) {
                                s = "A";
                            }
                        }
                    }
                }
            } else if (l > i1) {
                try {
                    s = as[2];
                } catch (Exception exception3) {
                    try {
                        s = as[1];
                    } catch (Exception exception4) {
                        try {
                            s = as[0];
                        } catch (Exception exception5) {
                            s = "A";
                        }
                    }
                }
            } else {
                try {
                    s = as[2];
                } catch (Exception exception) {
                    try {
                        s = as[1];
                    } catch (Exception exception1) {
                        try {
                            s = as[0];
                        } catch (Exception exception2) {
                            s = "A";
                        }
                    }
                }
            }
        } else
            s = as[0];
        as1 = new String[9];
        as1[0] = App.getMyString(R.string.ajuda_telefone_1, s);
        as1[1] = App.getMyString(R.string.ajuda_telefone_2, s);
        as1[2] = App.getMyString(R.string.ajuda_telefone_3, s);
        as1[3] = App.getMyString(R.string.ajuda_telefone_4, s);
        as1[4] = App.getMyString(R.string.ajuda_telefone_5, s);
        as1[5] = App.getMyString(R.string.ajuda_telefone_6, s);
        as1[6] = App.getMyString(R.string.ajuda_telefone_7, s);
        as1[7] = App.getMyString(R.string.ajuda_telefone_8, s);
        as1[8] = App.getMyString(R.string.ajuda_telefone_9);
        int l1 = 80 / (i + 1);
        byte byte0 = 50;
        int i2 = 10 * (i + 1);
        if (as.length == 1) {
            byte0 = 0;
            i2 = 0;
        }
        ai = (new int[]{
                i2, byte0, l1, l1, l1, byte0, byte0, l1, i2
        });
        arraylist2 = new ArrayList();
        j2 = 0;
        while (true) {
            if (j2 >= as1.length) {
                long l2 = System.currentTimeMillis();
                Random random1 = new Random(l2);
                Collections.shuffle(arraylist2, random1);
                Random random2 = new Random(l2);
                Collections.shuffle(arraylist2, random2);
                Random random3 = new Random(System.currentTimeMillis());
                return (String) arraylist2.get(random3.nextInt(arraylist2.size()));
            }
            s1 = as1[j2];
            k2 = 0;
            while (k2 < ai[j2]) {
                arraylist2.add(s1);
                k2++;
            }
            j2++;
        }
    }

    private static ArrayList getRandomArray(Pergunta pergunta, int i) {
        ArrayList arraylist;
        ArrayList arraylist1;
        int k;
        float f2;
        int l;
        int i1;
        int j1;
        arraylist = new ArrayList();
        if (pergunta.getRespostas().size() == 1) {
            arraylist.add(pergunta.RespostaCerta);
            return arraylist;
        }
        float f = pCerta[i];
        float f1 = pErrada[i];
        arraylist1 = pergunta.getRespostas();
        int j;
        Random random;
        long l2;
        Random random1;
        Random random2;
        if (arraylist1.size() == 2 && (i == 0 || i == 1)) {
            f1 = 0.0F;
        } else if (arraylist1.size() == 2 && (i == 2 || i == 3)) {
            f1 = 20F;
        } else if (arraylist1.size() == 2 && i == 4) {
            f1 = 30F;
        }
        j = (int) (1000F * (f / 100F));
        k = (int) (1000F * (f1 / 100F));
        f2 = (1000F * (1.0F - (f + f1) / 100F)) / 2.0F;
        l = 0;
        while (l < j) {
            arraylist.add((String) arraylist1.get(pergunta.getIndexRespostaCerta()));
            l++;
        }
        random = new Random();
        random.setSeed(System.currentTimeMillis());
        do {
            i1 = random.nextInt(arraylist1.size());
        } while (i1 == pergunta.getIndexRespostaCerta() || i1 == -1);
        j1 = 0;
        while (true) {
            if (j1 >= arraylist1.size()) {
                l2 = System.currentTimeMillis();
                random1 = new Random(l2);
                Collections.shuffle(arraylist, random1);
                random2 = new Random(l2);
                Collections.shuffle(arraylist, random2);
                return arraylist;
            }
            if (j1 != pergunta.getIndexRespostaCerta()) {
                if (j1 == i1) {
                    if (k != 0) {
                        int l1 = 0;
                        while ((double) l1 < Math.ceil(k)) {
                            arraylist.add((String) arraylist1.get(i1));
                            l1++;
                        }
                    }
                } else {
                    int k1 = 0;
                    while ((double) k1 < Math.ceil(f2)) {
                        arraylist.add((String) arraylist1.get(j1));
                        k1++;
                    }
                }
            }
            j1++;
        }
    }

    static {
        int ai[] = new int[5];
        ai[1] = 10;
        ai[2] = 20;
        ai[3] = 30;
        ai[4] = 30;
        pErrada = ai;
    }
}
