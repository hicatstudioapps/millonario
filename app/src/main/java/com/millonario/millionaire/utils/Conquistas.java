package com.millonario.millionaire.utils;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

// Referenced classes of package com.millonario.millionaire.utils:
//            DBAdapter, Utils, Analytics

public class Conquistas {
    public Conquistas() {
    }

    private static ValueHolder checkAndAdd(ArrayList arraylist, int i, int j) {
        if (i >= getAchivementValue(j) && !arraylist.contains((new StringBuilder()).append(j).toString())) {
            ValueHolder valueholder = new ValueHolder();
            valueholder.ID = j;
            valueholder.Name = getAchivementName(j);
            valueholder.Image = App.getMyDrawable((new StringBuilder("medalha_conquista_")).append(j).toString());
            return valueholder;
        } else {
            return null;
        }
    }

    public static String getAchivementName(int i) {
        String s = App.getMyString(R.string.novato);
        switch (i) {
            default:
                return s;
            case 1: // '\001'
                return App.getMyString(R.string.novato);
            case 2: // '\002'
                return App.getMyString(R.string.leigo);
            case 3: // '\003'
                return App.getMyString(R.string.aprendiz);
            case 4: // '\004'
                return App.getMyString(R.string.amador);
            case 5: // '\005'
                return App.getMyString(R.string.bom);
            case 6: // '\006'
                return App.getMyString(R.string.perito);
            case 7: // '\007'
                return App.getMyString(R.string.mestre);
            case 8: // '\b'
                return App.getMyString(R.string.genio);
            case 9: // '\t'
                return App.getMyString(R.string.sabio);
        }
    }

    public static int getAchivementValue(int i) {
        String s = App.getMyString(R.string.valor_conquista1);
        switch (i) {
            default:
                break;
            case 1:
                s = App.getMyString(R.string.valor_conquista1);
                break;
            case 2:
                s = App.getMyString(R.string.valor_conquista2);
                break;
            case 3:
                s = App.getMyString(R.string.valor_conquista3);
                break;
            case 4:
                s = App.getMyString(R.string.valor_conquista4);
                break;
            case 5:
                s = App.getMyString(R.string.valor_conquista5);
                break;
            case 6:
                s = App.getMyString(R.string.valor_conquista6);
                break;
            case 7:
                s = App.getMyString(R.string.valor_conquista7);
                break;
            case 8:
                s = App.getMyString(R.string.valor_conquista8);
                break;
            case 9:
                s = App.getMyString(R.string.valor_conquista9);
                break;
        }
        return Integer.parseInt(s.replace(" ", ""));
    }

    public static void verificaConquista(float f, Context context) {
        String s;
        ArrayList arraylist;
        ArrayList arraylist1;
        int i;
        int j;
        s = App.DB().getValue("Conquistas");
        arraylist = new ArrayList();
        if (!s.equals("") && !s.equals("<null>")) {
            arraylist = new ArrayList(Arrays.asList(s.split(",")));
        }
        arraylist1 = new ArrayList();
        i = (int) f;
        j = 1;
        while (j < 10) {
            arraylist1.add(checkAndAdd(arraylist, i, j));
            j++;
        }
        if (arraylist1.size() <= 0)
            return;
        Iterator iterator = arraylist1.iterator();
        while (iterator.hasNext()) {
            ValueHolder valueholder = (ValueHolder) iterator.next();
            if (valueholder != null) {
                StringBuilder stringbuilder = new StringBuilder(String.valueOf(s));
                String s1;
                View view;
                Toast toast;
                if (Utils.isEmptyDB(s)) {
                    s1 = "";
                } else {
                    s1 = ",";
                }
                s = stringbuilder.append(s1).append(valueholder.ID).toString();
                view = View.inflate(context, R.layout.toast_conquista, null);
                ((TextView) view.findViewById(R.id.txt_nome)).setText((new StringBuilder(String.valueOf(App.getMyString(R.string.nova_conquista)))).append(": ").append(valueholder.Name).toString());
                ((ImageView) view.findViewById(R.id.img_conquista)).setImageResource(valueholder.Image);
                toast = new Toast(context);
                toast.setView(view);
                toast.setDuration(1);
                toast.show();
                //Analytics.send("Conquista", valueholder.Name, null);
            }
        }
        App.DB().insertValue(s, "Conquistas");
    }

    static class ValueHolder {

        public int ID;
        public int Image;
        public String Name;

        ValueHolder() {
        }
    }
}
