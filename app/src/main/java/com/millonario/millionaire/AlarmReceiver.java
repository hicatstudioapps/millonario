package com.millonario.millionaire;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context arg0, Intent arg1) {
        // For our recurring task, we'll just display a message
        boolean c = arg1.getBooleanExtra("M", false);
        if (c) {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(arg0)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(arg0.getString(R.string.notification_short_cut_title))
                            .setContentText(arg0.getString(R.string.notification_short_cut_message)).setOngoing(false);
// Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(arg0, SplashActivity.class);
            PendingIntent notifyIntent =
                    PendingIntent.getActivity(
                            arg0,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            mBuilder.setContentIntent(notifyIntent);
            NotificationManager mNotificationManager =
                    (NotificationManager) arg0.getSystemService(Context.NOTIFICATION_SERVICE);
// Builds an anonymous Notification object from the builder, and
// passes it to the NotificationManager
            mNotificationManager.notify(99, mBuilder.build());
        }
    }

}
