package com.millonario.millionaire.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.nineoldandroids.animation.ValueAnimator;

import java.util.ArrayList;
// Referenced classes of package com.millonario.millionaire.views:
//            Flake

public class SnowFlakeView extends View {

    ValueAnimator animator;
    Bitmap droid;
    ArrayList flakes;
    float fps;
    String fpsString;
    int frames;
    Matrix m;
    Camera mCamera;
    int numFlakes;
    String numFlakesString;
    long prevTime;
    long startTime;
    Paint textPaint;

    public SnowFlakeView(Context context) {
        super(context);
        numFlakes = 0;
        flakes = new ArrayList();
        animator = ValueAnimator.ofFloat(new float[]{
                0.0F, 1.0F
        });
        frames = 0;
        fps = 0.0F;
        m = new Matrix();
        mCamera = new Camera();
        fpsString = "";
        numFlakesString = "";
        initMe();
    }

    public SnowFlakeView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
        numFlakes = 0;
        flakes = new ArrayList();
        animator = ValueAnimator.ofFloat(new float[]{
                0.0F, 1.0F
        });
        frames = 0;
        fps = 0.0F;
        m = new Matrix();
        mCamera = new Camera();
        fpsString = "";
        numFlakesString = "";
        initMe();
    }

    public SnowFlakeView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        numFlakes = 0;
        flakes = new ArrayList();
        animator = ValueAnimator.ofFloat(new float[]{
                0.0F, 1.0F
        });
        frames = 0;
        fps = 0.0F;
        m = new Matrix();
        mCamera = new Camera();
        fpsString = "";
        numFlakesString = "";
        initMe();
    }

    private void initMe() {
        droid = BitmapFactory.decodeResource(getResources(), R.drawable.ven_snowflake);
        textPaint = new Paint(1);
        textPaint.setColor(-1);
        textPaint.setTextSize(24F);
        animator.addUpdateListener(new com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueanimator) {
                float f;
                int i;
                long l = System.currentTimeMillis();
                f = (float) (l - prevTime) / 1000F;
                prevTime = l;
                i = 0;
                while (true) {
                    Flake flake;
                    if (i >= numFlakes) {
                        invalidate();
                        return;
                    }
                    flake = (Flake) flakes.get(i);
                    flake.y = flake.y + f * flake.speed;
                    if (flake.y > (float) getHeight()) {
                        flake.y = 0 - flake.height;
                        flake.x = flake.originalX;
                    }
                    if (flake.x >= 0.0F) {
                        if (flake.x > (float) getWidth())
                            flake.x = getWidth() + flake.width;
                    } else
                        flake.x = 0 - flake.width;
                    flake.previousRotation = flake.rotation;
                    flake.rotation = flake.rotation + f * flake.rotationSpeed;
                    i++;
                }
            }
        });
        animator.setRepeatCount(-1);
        animator.setDuration(1800L);
    }

    void addFlakes(int i) {
        int j = 0;
        do {
            if (j >= i) {
                setNumFlakes(i + numFlakes);
                return;
            }
            flakes.add(Flake.createSnowFlake(getWidth(), droid));
            j++;
        } while (true);
    }

    int getNumFlakes() {
        return numFlakes;
    }

    private void setNumFlakes(int i) {
        numFlakes = i;
        numFlakesString = (new StringBuilder("numFlakes: ")).append(numFlakes).toString();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int i = 0;
        do {
            if (i >= numFlakes) {
                frames = 1 + frames;
                long l = System.currentTimeMillis();
                long l1 = l - startTime;
                if (l1 > 1000L) {
                    float f = (float) l1 / 1000F;
                    fps = (float) frames / f;
                    fpsString = (new StringBuilder("fps: ")).append(fps).toString();
                    startTime = l;
                    frames = 0;
                }
                return;
            }
            Flake flake = (Flake) flakes.get(i);
            m.setTranslate(-flake.width / 2, -flake.height / 2);
            m.postTranslate((float) (flake.width / 2) + flake.x, (float) (flake.height / 2) + flake.y);
            canvas.drawBitmap(flake.bitmap, m, null);
            i++;
        } while (true);
    }

    protected void onSizeChanged(int i, int j, int k, int l) {
        super.onSizeChanged(i, j, k, l);
        flakes.clear();
        numFlakes = 0;
        byte byte0;
        if (App.getMyResources().getBoolean(R.bool.isTablet)) {
            byte0 = 100;
        } else if (App.getMyResources().getBoolean(R.bool.isLdpi)) {
            byte0 = 30;
        } else {
            byte0 = 60;
        }
        addFlakes(byte0);
        animator.cancel();
        startTime = System.currentTimeMillis();
        prevTime = startTime;
        frames = 0;
        animator.start();
    }

    public void pause() {
        animator.cancel();
    }

    public void resume() {
        animator.start();
    }

    void subtractFlakes(int i) {
        int j = 0;
        do {
            if (j >= i) {
                setNumFlakes(numFlakes - i);
                return;
            }
            int k = -1 + (numFlakes - j);
            flakes.remove(k);
            j++;
        } while (true);
    }
}
