package com.millonario.millionaire.views;

import android.graphics.Bitmap;

import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;

import java.util.HashMap;
import java.util.Random;

public class Flake {

    static int DIRECAO_GLOBAL = 1;
    static int DIRECAO_GLOBAL_X = 1;
    static HashMap bitmapMap = new HashMap();
    static HashMap bitmapSnowMap = new HashMap();
    int alpha;
    Bitmap bitmap;
    float direcao;
    float direcaoX;
    int height;
    float originalX;
    float originalY;
    float previousRotation;
    float rotation;
    float rotationSpeed;
    float speed;
    int width;
    float x;
    float y;

    public Flake() {
        previousRotation = 0.0F;
        direcao = 1.0F;
        direcaoX = 1.0F;
        alpha = 100;
    }

    static Flake createFlake(float f, float f1, Bitmap bitmap1) {
        Flake flake = new Flake();
        boolean flag = App.getMyResources().getBoolean(R.bool.isLdpi);
        boolean flag1 = App.getMyResources().getBoolean(R.bool.isTablet);
        float f2 = (float) Math.random();
        int i;
        float f3;
        if (flag) {
            i = 20;
        } else if (flag1) {
            i = 100;
        } else {
            i = 80;
        }
        flake.width = (int) (5F + f2 * (float) i);
        flake.height = (int) ((float) (bitmap1.getHeight() / bitmap1.getWidth()) * (float) flake.width);
        f3 = (float) Math.random() * (f - (float) flake.width);
        flake.x = f3;
        flake.originalX = f3;
        flake.y = (float) Math.random() * (f1 - (float) flake.height);
        flake.speed = (float) ((double) 0.2F + Math.random() * (double) (0.5F - 0.2F));
        DIRECAO_GLOBAL = -1 * DIRECAO_GLOBAL;
        flake.direcao = DIRECAO_GLOBAL;
        flake.rotation = 180F * (float) Math.random() - 90F;
        flake.rotationSpeed = 90F * (float) Math.random() - 45F;
        flake.bitmap = (Bitmap) bitmapMap.get(Integer.valueOf(flake.width));
        if (flake.bitmap == null) {
            flake.bitmap = Bitmap.createScaledBitmap(bitmap1, flake.width, flake.height, true);
            bitmapMap.put(Integer.valueOf(flake.width), flake.bitmap);
        }
        flake.width = flake.bitmap.getWidth();
        flake.height = flake.bitmap.getHeight();
        return flake;
    }

    static Flake createFlake(float f, Bitmap bitmap1) {
        Flake flake = new Flake();
        boolean flag = App.getMyResources().getBoolean(R.bool.isTablet);
        float f1 = (float) Math.random();
        int i;
        float f2;
        float f3;
        float f4;
        int j;
        if (false) {
            i = 20;
        } else if (flag) {
            i = 100;
        } else {
            i = 80;
        }
        flake.width = (int) (5F + f1 * (float) i);
        flake.height = (int) ((float) (bitmap1.getHeight() / bitmap1.getWidth()) * (float) flake.width);
        f2 = (float) Math.random() * (f - (float) flake.width);
        flake.x = f2;
        flake.originalX = f2;
        f3 = 0.0F - ((float) flake.height + (float) Math.random() * (float) flake.height);
        flake.y = f3;
        flake.originalY = f3;
        f4 = (float) Math.random();
        if (false) {
            j = 50;
        } else {
            j = 150;
        }
        flake.speed = 50F + f4 * (float) j;
        flake.rotation = 180F * (float) Math.random() - 90F;
        flake.rotationSpeed = 90F * (float) Math.random() - 45F;
        flake.bitmap = (Bitmap) bitmapMap.get(Integer.valueOf(flake.width));
        if (flake.bitmap == null) {
            flake.bitmap = Bitmap.createScaledBitmap(bitmap1, flake.width, flake.height, true);
            bitmapMap.put(Integer.valueOf(flake.width), flake.bitmap);
        }
        flake.width = flake.bitmap.getWidth();
        flake.height = flake.bitmap.getHeight();
        return flake;
    }

    static Flake createFlakeXY(int i, int j, int k, int l, float f, float f1, Bitmap bitmap1) {
        Flake flake = new Flake();
        boolean flag = App.getMyResources().getBoolean(R.bool.isTablet);
        float f2 = (float) Math.random();
        int i1;
        float f3;
        Random random;
        int j1;
        int k1;
        int l1;
        byte byte0;
        if (false) {
            i1 = 20;
        } else if (flag) {
            i1 = 100;
        } else {
            i1 = 80;
        }
        flake.width = (int) (5F + f2 * (float) i1);
        flake.height = (int) ((float) (bitmap1.getHeight() / bitmap1.getWidth()) * (float) flake.width);
        f3 = (float) i + (float) Math.random() * (float) (j - i);
        flake.x = f3;
        flake.originalX = f3;
        flake.y = (float) k + (float) Math.random() * (float) (l - k);
        flake.speed = (float) ((double) 0.1F + Math.random() * (double) (0.2F - 0.1F));
        random = new Random();
        j1 = DIRECAO_GLOBAL;
        if (random.nextInt(100) % 2 == 0) {
            k1 = 1;
        } else {
            k1 = -1;
        }
        DIRECAO_GLOBAL = k1 * j1;
        l1 = DIRECAO_GLOBAL_X;
        if (random.nextInt(100) % 2 == 0) {
            byte0 = -1;
        } else {
            byte0 = 1;
        }
        DIRECAO_GLOBAL_X = byte0 * l1;
        flake.direcao = DIRECAO_GLOBAL;
        flake.direcaoX = DIRECAO_GLOBAL_X;
        flake.rotation = 180F * (float) Math.random() - 90F;
        flake.rotationSpeed = 90F * (float) Math.random() - 45F;
        flake.bitmap = (Bitmap) bitmapMap.get(Integer.valueOf(flake.width));
        if (flake.bitmap == null) {
            flake.bitmap = Bitmap.createScaledBitmap(bitmap1, flake.width, flake.height, true);
            bitmapMap.put(Integer.valueOf(flake.width), flake.bitmap);
        }
        flake.width = flake.bitmap.getWidth();
        flake.height = flake.bitmap.getHeight();
        return flake;
    }

    static Flake createSnowFlake(float f, Bitmap bitmap1) {
        Flake flake = new Flake();
        boolean flag = App.getMyResources().getBoolean(R.bool.isLdpi);
        boolean flag1 = App.getMyResources().getBoolean(R.bool.isTablet);
        float f1 = (float) Math.random();
        int i;
        float f2;
        float f3;
        float f4;
        int j;
        if (flag) {
            i = 10;
        } else if (flag1) {
            i = 60;
        } else {
            i = 40;
        }
        flake.width = (int) (5F + f1 * (float) i);
        flake.height = (int) ((float) (bitmap1.getHeight() / bitmap1.getWidth()) * (float) flake.width);
        f2 = (float) Math.random() * (f - (float) flake.width);
        flake.x = f2;
        flake.originalX = f2;
        f3 = 0.0F - ((float) flake.height + 500F * (float) Math.random());
        flake.y = f3;
        flake.originalY = f3;
        f4 = (float) Math.random();
        if (flag) {
            j = 50;
        } else {
            j = 150;
        }
        flake.speed = 50F + f4 * (float) j;
        flake.rotation = 180F * (float) Math.random() - 90F;
        flake.rotationSpeed = 90F * (float) Math.random() - 45F;
        flake.bitmap = (Bitmap) bitmapSnowMap.get(Integer.valueOf(flake.width));
        if (flake.bitmap == null) {
            flake.bitmap = Bitmap.createScaledBitmap(bitmap1, flake.width, flake.height, true);
            bitmapSnowMap.put(Integer.valueOf(flake.width), flake.bitmap);
        }
        flake.width = flake.bitmap.getWidth();
        flake.height = flake.bitmap.getHeight();
        return flake;
    }

}
