package com.millonario.millionaire.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.util.AttributeSet;
import android.view.View;

import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.nineoldandroids.animation.ValueAnimator;

import java.util.ArrayList;

// Referenced classes of package com.millonario.millionaire.views:
//            Flake

//public class FlakeViewFlutuar extends View {
//
//    ValueAnimator animator;
//    Bitmap droid;
//    ArrayList flakes;
//    float fps;
//    String fpsString;
//    int frames;
//    int initialFlakes;
//    Matrix m;
//    int numFlakes;
//    String numFlakesString;
//    long prevTime;
//    long startTime;
//    Paint textPaint;
//
//    public FlakeViewFlutuar(Context context) {
//        super(context);
//        numFlakes = 0;
//        flakes = new ArrayList();
//        animator = ValueAnimator.ofFloat(new float[]{
//                0.0F, 1.0F
//        });
//        frames = 0;
//        fps = 0.0F;
//        m = new Matrix();
//        fpsString = "";
//        numFlakesString = "";
//        initialFlakes = -1;
//        initMe();
//    }
//
//    public FlakeViewFlutuar(Context context, AttributeSet attributeset) {
//        super(context, attributeset);
//        numFlakes = 0;
//        flakes = new ArrayList();
//        animator = ValueAnimator.ofFloat(new float[]{
//                0.0F, 1.0F
//        });
//        frames = 0;
//        fps = 0.0F;
//        m = new Matrix();
//        fpsString = "";
//        numFlakesString = "";
//        initialFlakes = -1;
//        initMe();
//    }
//
//    public FlakeViewFlutuar(Context context, AttributeSet attributeset, int i) {
//        super(context, attributeset, i);
//        numFlakes = 0;
//        flakes = new ArrayList();
//        animator = ValueAnimator.ofFloat(new float[]{
//                0.0F, 1.0F
//        });
//        frames = 0;
//        fps = 0.0F;
//        m = new Matrix();
//        fpsString = "";
//        numFlakesString = "";
//        initialFlakes = -1;
//        initMe();
//    }
//
//    private void initMe() {
//        boolean flag = App.getMyResources().getBoolean(R.bool.isLdpi);
//        boolean flag1 = App.getMyResources().getBoolean(R.bool.isTablet);
//        byte byte0;
//        int i;
//        Bitmap bitmap;
//        Canvas canvas;
//        if (flag) {
//            byte0 = 20;
//        } else if (flag1) {
//            byte0 = 100;
//        } else {
//            byte0 = 80;
//        }
//        i = byte0 + 5;
//        bitmap = Bitmap.createBitmap(i, i, android.graphics.Bitmap.Config.ARGB_8888);
//        canvas = new Canvas(bitmap);
//        try {
//            GradientDrawable gradientdrawable = (GradientDrawable) ShapeDrawable.createFromXml(App.getMyResources(), App.getMyResources().getXml(R.xml.square_animation));
//            gradientdrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
//            gradientdrawable.draw(canvas);
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }
//        droid = bitmap;
//        textPaint = new Paint(1);
//        textPaint.setColor(-1);
//        textPaint.setTextSize(24F);
//        animator.addUpdateListener(new com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener() {
//            public void onAnimationUpdate(ValueAnimator valueanimator) {
//                float f;
//                int j;
//                long l = System.currentTimeMillis();
//                f = (float) (l - prevTime) / 1000F;
//                prevTime = l;
//                j = 0;
//                while (true) {
//                    Flake flake;
//                    if (j >= numFlakes) {
//                        invalidate();
//                        return;
//                    }
//                    flake = (Flake) flakes.get(j);
//                    if (flake.y <= (float) (getHeight() + flake.height)) {
//                        if (flake.y < (float) (0 - flake.height)) {
//                            flake.direcao = 1.0F;
//                            flake.y = 1 + (0 - flake.height);
//                        }
//                    } else {
//                        flake.direcao = -1F;
//                        flake.y = -1 + getHeight() + flake.height;
//                    }
//                    if (flake.x > (float) (getWidth() + flake.width)) {
//                        flake.x = 0 - flake.width;
//                    }
//                    flake.y = flake.y + flake.speed * flake.direcao;
//                    flake.x = (float) ((double) flake.x + 0.5D * (double) flake.speed);
//                    flake.rotation = flake.rotation + f * flake.rotationSpeed;
//                    j++;
//                }
//            }
//        });
//        animator.setRepeatCount(-1);
//        animator.setDuration(3000L);
//    }
//
//    public void addFlakes(int i) {
//        int j = 0;
//        do {
//            if (j >= i) {
//                setNumFlakes(i + numFlakes);
//                return;
//            }
//            flakes.add(Flake.createFlake(getWidth(), getHeight(), droid));
//            j++;
//        } while (true);
//    }
//
//    int getNumFlakes() {
//        return numFlakes;
//    }
//
//    private void setNumFlakes(int i) {
//        numFlakes = i;
//        numFlakesString = (new StringBuilder("numFlakes: ")).append(numFlakes).toString();
//    }
//
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//        int i = 0;
//        do {
//            if (i >= numFlakes) {
//                frames = 1 + frames;
//                long l = System.currentTimeMillis();
//                long l1 = l - startTime;
//                if (l1 > 1000L) {
//                    float f = (float) l1 / 1000F;
//                    fps = (float) frames / f;
//                    fpsString = (new StringBuilder("fps: ")).append(fps).toString();
//                    startTime = l;
//                    frames = 0;
//                }
//                return;
//            }
//            Flake flake = (Flake) flakes.get(i);
//            m.setTranslate(-flake.width / 2, -flake.height / 2);
//            m.postRotate(flake.rotation);
//            m.postTranslate(flake.x, flake.y);
//            canvas.drawBitmap(flake.bitmap, m, null);
//            i++;
//        } while (true);
//    }
//
//    public void onSizeChanged(int i, int j, int k, int l) {
//        super.onSizeChanged(i, j, k, l);
//        flakes.clear();
//        numFlakes = 0;
//        if (initialFlakes < 0) {
//            int i1;
//            if (App.getMyResources().getBoolean(R.bool.isTablet)) {
//                i1 = 30;
//            } else {
//                i1 = 10;
//            }
//            initialFlakes = i1;
//        }
//        addFlakes(initialFlakes);
//        animator.cancel();
//        startTime = System.currentTimeMillis();
//        prevTime = startTime;
//        frames = 0;
//        animator.start();
//    }
//
//    public void pause() {
//        animator.cancel();
//    }
//
//    public void resume() {
//        animator.start();
//    }
//
//    public void setInitialFlakes(int i) {
//        initialFlakes = i;
//    }
//
//    void subtractFlakes(int i) {
//        int j = 0;
//        do {
//            if (j >= i) {
//                setNumFlakes(numFlakes - i);
//                return;
//            }
//            int k = -1 + (numFlakes - j);
//            flakes.remove(k);
//            j++;
//        } while (true);
//    }
//}
