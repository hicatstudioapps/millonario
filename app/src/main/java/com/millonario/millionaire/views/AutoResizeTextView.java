package com.millonario.millionaire.views;

import android.content.Context;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

public class AutoResizeTextView extends TextView {
    public static final float MIN_TEXT_SIZE = 20F;
    private static final String mEllipsis = "...";
    private boolean mAddEllipsis;
    private float mMaxTextSize;
    private float mMinTextSize;
    private boolean mNeedsResize;
    private float mSpacingAdd;
    private float mSpacingMult;
    private OnTextResizeListener mTextResizeListener;
    private float mTextSize;
    public AutoResizeTextView(Context context) {
        this(context, null);
    }

    public AutoResizeTextView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public AutoResizeTextView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mNeedsResize = false;
        mMaxTextSize = 0.0F;
        mMinTextSize = 20F;
        mSpacingMult = 1.0F;
        mSpacingAdd = 0.0F;
        mAddEllipsis = true;
        mTextSize = getTextSize();
    }

    private int getTextHeight(CharSequence charsequence, TextPaint textpaint, int i, float f) {
        TextPaint textpaint1 = new TextPaint(textpaint);
        textpaint1.setTextSize(f);
        return (new StaticLayout(charsequence, textpaint1, i, android.text.Layout.Alignment.ALIGN_NORMAL, mSpacingMult, mSpacingAdd, true)).getHeight();
    }

    public boolean getAddEllipsis() {
        return mAddEllipsis;
    }

    public void setAddEllipsis(boolean flag) {
        mAddEllipsis = flag;
    }

    public float getMaxTextSize() {
        return mMaxTextSize;
    }

    public void setMaxTextSize(float f) {
        mMaxTextSize = f;
        requestLayout();
        invalidate();
    }

    public float getMinTextSize() {
        return mMinTextSize;
    }

    public void setMinTextSize(float f) {
        mMinTextSize = f;
        requestLayout();
        invalidate();
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l) {
        if (flag || mNeedsResize) {
            resizeText(k - i - getCompoundPaddingLeft() - getCompoundPaddingRight(), l - j - getCompoundPaddingBottom() - getCompoundPaddingTop());
        }
        super.onLayout(flag, i, j, k, l);
    }

    protected void onSizeChanged(int i, int j, int k, int l) {
        if (i != k || j != l) {
            mNeedsResize = true;
        }
    }

    protected void onTextChanged(CharSequence charsequence, int i, int j, int k) {
        mNeedsResize = true;
        resetTextSize();
    }

    public void resetTextSize() {
        if (mTextSize > 0.0F) {
            super.setTextSize(0, mTextSize);
            mMaxTextSize = mTextSize;
        }
    }

    public void resizeText() {
        int i = getHeight() - getPaddingBottom() - getPaddingTop();
        resizeText(getWidth() - getPaddingLeft() - getPaddingRight(), i);
    }

    public void resizeText(int i, int j) {
        CharSequence charsequence;
        TextPaint textpaint;
        float f1;
        int k;
        int i1;
        int j1;
        float f2;
        float f3;
        StaticLayout staticlayout;
        int l;
        charsequence = getText();
        if (charsequence == null || charsequence.length() == 0 || j <= 0 || i <= 0 || mTextSize == 0.0F) {
            return;
        }
        if (getTransformationMethod() != null) {
            charsequence = getTransformationMethod().getTransformation(charsequence, this);
        }
        textpaint = getPaint();
        float f = textpaint.getTextSize();
        if (mMaxTextSize > 0.0F) {
            f1 = Math.min(mTextSize, mMaxTextSize);
        } else {
            f1 = mTextSize;
        }
        k = getTextHeight(charsequence, textpaint, i, f1);
        while (k > j && f1 > mMinTextSize) {
            f1 = Math.max(f1 - 2.0F, mMinTextSize);
            k = getTextHeight(charsequence, textpaint, i, f1);
        }
        if (!(!mAddEllipsis || f1 != mMinTextSize || k <= j)) {
            staticlayout = new StaticLayout(charsequence, new TextPaint(textpaint), i, android.text.Layout.Alignment.ALIGN_NORMAL, mSpacingMult, mSpacingAdd, false);
            if (staticlayout.getLineCount() > 0) {
                l = -1 + staticlayout.getLineForVertical(j);
                if (l >= 0) {
                    i1 = staticlayout.getLineStart(l);
                    j1 = staticlayout.getLineEnd(l);
                    f2 = staticlayout.getLineWidth(l);
                    f3 = textpaint.measureText("...");
                    while ((float) i < f2 + f3) {
                        j1--;
                        f2 = textpaint.measureText(charsequence.subSequence(i1, j1 + 1).toString());
                    }
                    setText((new StringBuilder()).append(charsequence.subSequence(0, j1)).append("...").toString());
                } else
                    setText("");
            }
        }
        setTextSize(0, f1);
        setLineSpacing(mSpacingAdd, mSpacingMult);
        if (mTextResizeListener != null) {
            mTextResizeListener.onTextResize(this, f, f1);
        }
        mNeedsResize = false;
    }

    public void setLineSpacing(float f, float f1) {
        super.setLineSpacing(f, f1);
        mSpacingMult = f1;
        mSpacingAdd = f;
    }

    public void setOnResizeListener(OnTextResizeListener ontextresizelistener) {
        mTextResizeListener = ontextresizelistener;
    }

    public void setTextSize(float f) {
        super.setTextSize(f);
        mTextSize = getTextSize();
    }

    public void setTextSize(int i, float f) {
        super.setTextSize(i, f);
        mTextSize = getTextSize();
    }

    public static interface OnTextResizeListener {

        public abstract void onTextResize(TextView textview, float f, float f1);
    }
}
