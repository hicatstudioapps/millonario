package com.millonario.millionaire.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class AutoResizeButton extends Button {

    private float mTextSize;

    public AutoResizeButton(Context context) {
        super(context, null);
    }

    public AutoResizeButton(Context context, AttributeSet attributeset) {
        super(context, attributeset, 0x1010048);
        float f = context.getResources().getDisplayMetrics().density;
        mTextSize = getTextSize() / f;
    }

    public AutoResizeButton(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l) {
        super.onLayout(flag, i, j, k, l);
        int i1 = getMeasuredWidth();
        int j1 = getMeasuredHeight();
        float f = mTextSize;
        do {
            float f1 = f - 1.0F;
            setTextSize(f);
            measure(0, 0);
            if (getMeasuredWidth() <= i1) {
                setMeasuredDimension(i1, j1);
                return;
            }
            f = f1;
        } while (true);
    }
}
