package com.millonario.millionaire.popup;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;

public class PopUp {

    public static Context ctx;
    public static boolean isOpen = false;
    protected static View layout;
    protected static PopupWindow pw;
    private static int retry = 0;

    public PopUp() {
    }

    private static void constroiPopUp(int i, int j) {
        Typeface tf = Typeface.createFromAsset(ctx.getResources().getAssets(), "norwester.otf");
        LinearLayout linearlayout;
        LinearLayout linearlayout1;
        LinearLayout linearlayout2;
        layout = View.inflate(ctx, R.layout.popup_layout_base, null);

        linearlayout = (LinearLayout) layout.findViewById(R.id.ll_popup_base);
        linearlayout1 = (LinearLayout) layout.findViewById(R.id.ll_popup_extras);
        linearlayout2 = (LinearLayout) layout.findViewById(R.id.ll_popup_botoes);
        if (i <= 0) {
            linearlayout.removeView(linearlayout1);
        } else {
            int l = 0;
            while (l < i) {
                View.inflate(ctx, R.layout.popup_layout_edittext, linearlayout1);
                EditText edittext = (EditText) linearlayout1.getChildAt(l);
                edittext.setTag((new StringBuilder("txt_")).append(l + 1).toString());
                if (l == i - 1) {
                    edittext.setImeOptions(6);
                } else {
                    edittext.setImeOptions(5);
                }
                l++;
            }
        }
        if (j <= 0)
            return;
        int k = 0;
        while (k < j) {
            View.inflate(ctx, R.layout.popup_layout_button, linearlayout2);
            Button button = (Button) linearlayout2.getChildAt(k);
            button.setTypeface(tf);
            button.setTag((new StringBuilder("btn_")).append(k + 1).toString());
            if (k == j - 1) {
                android.widget.LinearLayout.LayoutParams layoutparams = (android.widget.LinearLayout.LayoutParams) button.getLayoutParams();
                layoutparams.bottomMargin = 0;
                button.setLayoutParams(layoutparams);
            }
            k++;
        }
    }

    public static EditText getEditTextInput(int i) {
        if (layout == null) {
            return null;
        }
        EditText edittext;
        try {
            edittext = (EditText) layout.findViewWithTag((new StringBuilder("txt_")).append(i).toString());
        } catch (Exception exception) {
            return null;
        }
        return edittext;
    }

    public static String getMensagem() {
        if (layout == null) {
            return "";
        }
        String s;
        try {
            s = ((TextView) layout.findViewById(R.id.txt_popup_mensagem)).getText().toString();
        } catch (Exception exception) {
            return "";
        }
        return s;
    }

    public static void setMensagem(String s) {

        if (layout == null) {
            return;
        }
        if (s == null) {
            s = "";
        }
        try {
            Typeface tf = Typeface.createFromAsset(ctx.getResources().getAssets(), "norwester.otf");
            ((TextView) layout.findViewById(R.id.txt_popup_mensagem)).setTypeface(tf);
            ((TextView) layout.findViewById(R.id.txt_popup_mensagem)).setText(s);
            return;
        } catch (Exception exception) {
            return;
        }
    }

    public static String getTextoInput(int i) {
        if (layout == null) {
            return "";
        }
        String s;
        try {
            s = ((TextView) layout.findViewWithTag((new StringBuilder("txt_")).append(i).toString())).getText().toString();
        } catch (Exception exception) {
            return "";
        }
        return s;
    }

    public static void hide() {
        if (pw != null) {
            try {
                pw.dismiss();
                layout = null;
                pw = null;
            } catch (Exception exception) {
            }
        }
        ctx = null;
        isOpen = false;
        App.isPopUpShowing = false;
    }

    private static void retryShow() {
        try {
            int i = ((Activity) ctx).getWindowManager().getDefaultDisplay().getWidth();
            pw = new PopupWindow(layout, i, -1, true);
            pw.showAtLocation(layout, 17, 0, 0);
            pw.update();
            isOpen = true;
            App.isPopUpShowing = true;
            return;
        } catch (Exception exception) {
            (new Handler()).postDelayed(new Runnable() {

                public void run() {
                    PopUp.retry = 1 + PopUp.retry;
                    if (PopUp.retry < 5) {
                        PopUp.retryShow();
                    }
                }

            }, 100L);
        }
    }

    public static void setListenerBotao(android.view.View.OnClickListener onclicklistener, int i) {
        if (layout == null || onclicklistener == null) {
            return;
        }
        try {
            ((Button) layout.findViewWithTag((new StringBuilder("btn_")).append(i).toString())).setOnClickListener(onclicklistener);
            return;
        } catch (Exception exception) {
            return;
        }
    }

    public static void setTextoBotao(String s, int i) {
        if (layout == null) {
            return;
        }
        if (s == null) {
            s = "";
        }
        try {
            ((Button) layout.findViewWithTag((new StringBuilder("btn_")).append(i).toString())).setText(s);
            return;
        } catch (Exception exception) {
            return;
        }
    }

    public static void setTextoEListenerBotao(String s, android.view.View.OnClickListener onclicklistener, int i) {
        setTextoBotao(s, i);
        setListenerBotao(onclicklistener, i);
    }

    public static void setTextoInput(String s, String s1, int i) {
        if (layout == null) {
            return;
        }
        if (s == null) {
            s = "";
        }
        try {
            ((TextView) layout.findViewWithTag((new StringBuilder("txt_")).append(i).toString())).setText(s);
            ((EditText) layout.findViewWithTag((new StringBuilder("txt_")).append(i).toString())).setHint(s1);
            return;
        } catch (Exception exception) {
            return;
        }
    }

    public static void show(Context context, int i, int j) {
        ctx = context;
        try {
            constroiPopUp(i, j);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        if (layout == null) {
            return;
        }
        try {
            int k = ((Activity) ctx).getWindowManager().getDefaultDisplay().getWidth();
            layout.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK)
                        pw.dismiss();
                    return false;
                }
            });
            pw = new PopupWindow(layout, k, -1, false);
            pw.showAtLocation(layout, 17, 0, 0);
            pw.update();
            //pw.setOnDismissListener();
            isOpen = true;
            App.isPopUpShowing = true;
            return;
        } catch (Exception exception1) {
            retry = 0;
        }
        retryShow();
    }


}
