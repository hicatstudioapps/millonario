package com.millonario.millionaire.billing;

import android.text.TextUtils;
import android.util.Log;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

// Referenced classes of package com.millonario.millionaire.billing:
//            Base64DecoderException, Base64

public class Security {

    private static final String KEY_FACTORY_ALGORITHM = "RSA";
    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
    private static final String TAG = "IABUtil/Security";

    public Security() {
    }

    public static PublicKey generatePublicKey(String s) {
        PublicKey publickey;
        try {
            byte abyte0[] = Base64.decode(s);
            publickey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(abyte0));
        } catch (NoSuchAlgorithmException nosuchalgorithmexception) {
            throw new RuntimeException(nosuchalgorithmexception);
        } catch (InvalidKeySpecException invalidkeyspecexception) {
            Log.e("IABUtil/Security", "Invalid key specification.");
            throw new IllegalArgumentException(invalidkeyspecexception);
        } catch (Base64DecoderException base64decoderexception) {
            Log.e("IABUtil/Security", "Base64 decoding failed.");
            throw new IllegalArgumentException(base64decoderexception);
        }
        return publickey;
    }

    public static boolean verify(PublicKey publickey, String s, String s1) {
        label0:
        {
            try {
                Signature signature = Signature.getInstance("SHA1withRSA");
                signature.initVerify(publickey);
                signature.update(s.getBytes());
                if (signature.verify(Base64.decode(s1))) {
                    break label0;
                }
                Log.e("IABUtil/Security", "Signature verification failed.");
            } catch (NoSuchAlgorithmException nosuchalgorithmexception) {
                Log.e("IABUtil/Security", "NoSuchAlgorithmException.");
                return false;
            } catch (InvalidKeyException invalidkeyexception) {
                Log.e("IABUtil/Security", "Invalid key specification.");
                return false;
            } catch (SignatureException signatureexception) {
                Log.e("IABUtil/Security", "Signature exception.");
                return false;
            } catch (Base64DecoderException base64decoderexception) {
                Log.e("IABUtil/Security", "Base64 decoding failed.");
                return false;
            }
            return false;
        }
        return true;
    }

    public static boolean verifyPurchase(String s, String s1, String s2) {
        if (TextUtils.isEmpty(s1) || TextUtils.isEmpty(s) || TextUtils.isEmpty(s2)) {
            Log.e("IABUtil/Security", "Purchase verification failed: missing data.");
            return true;
        } else {
            return verify(generatePublicKey(s), s1, s2);
        }
    }
}
