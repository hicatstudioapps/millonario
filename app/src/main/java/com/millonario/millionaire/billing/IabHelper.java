package com.millonario.millionaire.billing;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.millonario.millionaire.billing:
//            IabException, Purchase, IabResult, Security, 
//            Inventory, SkuDetails

public class IabHelper {
    public static final int BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = 3;
    public static final int BILLING_RESPONSE_RESULT_DEVELOPER_ERROR = 5;
    public static final int BILLING_RESPONSE_RESULT_ERROR = 6;
    public static final int BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED = 7;
    public static final int BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED = 8;
    public static final int BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE = 4;
    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    public static final int BILLING_RESPONSE_RESULT_USER_CANCELED = 1;
    public static final String GET_SKU_DETAILS_ITEM_LIST = "ITEM_ID_LIST";
    public static final String GET_SKU_DETAILS_ITEM_TYPE_LIST = "ITEM_TYPE_LIST";
    public static final int IABHELPER_BAD_RESPONSE = -1002;
    public static final int IABHELPER_ERROR_BASE = -1000;
    public static final int IABHELPER_INVALID_CONSUMPTION = -1010;
    public static final int IABHELPER_MISSING_TOKEN = -1007;
    public static final int IABHELPER_REMOTE_EXCEPTION = -1001;
    public static final int IABHELPER_SEND_INTENT_FAILED = -1004;
    public static final int IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE = -1009;
    public static final int IABHELPER_UNKNOWN_ERROR = -1008;
    public static final int IABHELPER_UNKNOWN_PURCHASE_RESPONSE = -1006;
    public static final int IABHELPER_USER_CANCELLED = -1005;
    public static final int IABHELPER_VERIFICATION_FAILED = -1003;
    public static final String INAPP_CONTINUATION_TOKEN = "INAPP_CONTINUATION_TOKEN";
    public static final String ITEM_TYPE_INAPP = "inapp";
    public static final String ITEM_TYPE_SUBS = "subs";
    public static final String RESPONSE_BUY_INTENT = "BUY_INTENT";
    public static final String RESPONSE_CODE = "RESPONSE_CODE";
    public static final String RESPONSE_GET_SKU_DETAILS_LIST = "DETAILS_LIST";
    public static final String RESPONSE_INAPP_ITEM_LIST = "INAPP_PURCHASE_ITEM_LIST";
    public static final String RESPONSE_INAPP_PURCHASE_DATA = "INAPP_PURCHASE_DATA";
    public static final String RESPONSE_INAPP_PURCHASE_DATA_LIST = "INAPP_PURCHASE_DATA_LIST";
    public static final String RESPONSE_INAPP_SIGNATURE = "INAPP_DATA_SIGNATURE";
    public static final String RESPONSE_INAPP_SIGNATURE_LIST = "INAPP_DATA_SIGNATURE_LIST";
    boolean mAsyncInProgress;
    String mAsyncOperation;
    Context mContext;
    boolean mDebugLog;
    String mDebugTag;
    boolean mDisposed;
    OnIabPurchaseFinishedListener mPurchaseListener;
    String mPurchasingItemType;
    int mRequestCode;
    //IInAppBillingService mService;
    ServiceConnection mServiceConn;
    boolean mSetupDone;
    String mSignatureBase64;
    boolean mSubscriptionsSupported;
    public IabHelper(Context context, String s) {
        mDebugLog = false;
        mDebugTag = "IabHelper";
        mSetupDone = false;
        mDisposed = false;
        mSubscriptionsSupported = false;
        mAsyncInProgress = false;
        mAsyncOperation = "";
        mSignatureBase64 = null;
        mContext = context.getApplicationContext();
        mSignatureBase64 = s;
        logDebug("IAB helper created.");
    }

    public static String getResponseDesc(int i) {
        String as[] = "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned".split("/");
        String as1[] = "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt".split("/");
        if (i <= -1000) {
            int j = -1000 - i;
            if (j >= 0 && j < as1.length) {
                return as1[j];
            } else {
                return (new StringBuilder(String.valueOf(String.valueOf(i)))).append(":Unknown IAB Helper Error").toString();
            }
        }
        if (i < 0 || i >= as.length) {
            return (new StringBuilder(String.valueOf(String.valueOf(i)))).append(":Unknown").toString();
        } else {
            return as[i];
        }
    }

    private void checkNotDisposed() {
        if (mDisposed) {
            throw new IllegalStateException("IabHelper was disposed of, so it cannot be used.");
        } else {
            return;
        }
    }

    void checkSetupDone(String s) {
        if (!mSetupDone) {
            logError((new StringBuilder("Illegal state for operation (")).append(s).append("): IAB helper is not set up.").toString());
            throw new IllegalStateException((new StringBuilder("IAB helper is not set up. Can't perform operation: ")).append(s).toString());
        } else {
            return;
        }
    }

    void consume(Purchase purchase)
            throws IabException {
        String s;
        String s1;
        checkNotDisposed();
        checkSetupDone("consume");
        if (!purchase.mItemType.equals("inapp")) {
            throw new IabException(-1010, (new StringBuilder("Items of type '")).append(purchase.mItemType).append("' can't be consumed.").toString());
        }


        s = purchase.getToken();
        s1 = purchase.getSku();
        if (s == null) {
            //break MISSING_BLOCK_LABEL_81;
            return;
        }
        if (!s.equals("")) {
            int i;
            logDebug((new StringBuilder("Consuming sku: ")).append(s1).append(", token: ").append(s).toString());
            i = 1;
            if (i != 0) {
                logDebug((new StringBuilder("Error consuming consuming sku ")).append(s1).append(". ").append(getResponseDesc(i)).toString());
                throw new IabException(i, (new StringBuilder("Error consuming sku ")).append(s1).toString());
            }
            logDebug((new StringBuilder("Successfully consumed sku: ")).append(s1).toString());
            return;
        }
        logError((new StringBuilder("Can't consume ")).append(s1).append(". No token.").toString());
        throw new IabException(-1007, (new StringBuilder("PurchaseInfo is missing token for sku: ")).append(s1).append(" ").append(purchase).toString());


    }

    public void consumeAsync(Purchase purchase, OnConsumeFinishedListener onconsumefinishedlistener) {
        checkNotDisposed();
        checkSetupDone("consume");
        ArrayList arraylist = new ArrayList();
        arraylist.add(purchase);
        consumeAsyncInternal(arraylist, onconsumefinishedlistener, null);
    }

    public void consumeAsync(List list, OnConsumeMultiFinishedListener onconsumemultifinishedlistener) {
        checkNotDisposed();
        checkSetupDone("consume");
        consumeAsyncInternal(list, null, onconsumemultifinishedlistener);
    }

    void consumeAsyncInternal(final List purchases, final OnConsumeFinishedListener singleListener, final OnConsumeMultiFinishedListener multiListener) {
        final Handler handler = new Handler();
        flagStartAsync("consume");
        (new Thread(new Runnable() {
            public void run() {
                final ArrayList arraylist = new ArrayList();
                Iterator iterator = purchases.iterator();
                do {
                    if (!iterator.hasNext()) {
                        flagEndAsync();
                        if (!mDisposed && singleListener != null) {
                            handler.post(new Runnable() {
                                public void run() {
                                    singleListener.onConsumeFinished((Purchase) purchases.get(0), (IabResult) arraylist.get(0));
                                }
                            });
                        }
                        if (!mDisposed && multiListener != null) {
                            handler.post(new Runnable() {
                                public void run() {
                                    multiListener.onConsumeMultiFinished(purchases, arraylist);
                                }
                            });
                        }
                        return;
                    }
                    Purchase purchase = (Purchase) iterator.next();
                    try {
                        consume(purchase);
                        arraylist.add(new IabResult(0, (new StringBuilder("Successful consume of sku ")).append(purchase.getSku()).toString()));
                    } catch (IabException iabexception) {
                        arraylist.add(iabexception.getResult());
                    }
                } while (true);
            }
        })).start();

    }

    public void dispose() {
        logDebug("Disposing.");
        mSetupDone = false;
        if (mServiceConn != null) {
            logDebug("Unbinding from service.");
            if (mContext != null) {
                mContext.unbindService(mServiceConn);
            }
        }
        mDisposed = true;
        mContext = null;
        mServiceConn = null;
        //mService = null;
        mPurchaseListener = null;
    }

    public void enableDebugLogging(boolean flag) {
        checkNotDisposed();
        mDebugLog = flag;
    }

    public void enableDebugLogging(boolean flag, String s) {
        checkNotDisposed();
        mDebugLog = flag;
        mDebugTag = s;
    }

    public void flagEndAsync() {
        logDebug((new StringBuilder("Ending async operation: ")).append(mAsyncOperation).toString());
        mAsyncOperation = "";
        mAsyncInProgress = false;
    }

    void flagStartAsync(String s) {
        if (mAsyncInProgress) {
            throw new IllegalStateException((new StringBuilder("Can't start async operation (")).append(s).append(") because another async operation(").append(mAsyncOperation).append(") is in progress.").toString());
        } else {
            mAsyncOperation = s;
            mAsyncInProgress = true;
            logDebug((new StringBuilder("Starting async operation: ")).append(s).toString());
            return;
        }
    }

    int getResponseCodeFromBundle(Bundle bundle) {
        Object obj = bundle.get("RESPONSE_CODE");
        if (obj == null) {
            logDebug("Bundle with null response code, assuming OK (known issue)");
            return 0;
        }
        if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        }
        if (obj instanceof Long) {
            return (int) ((Long) obj).longValue();
        } else {
            logError("Unexpected type for bundle response code.");
            logError(obj.getClass().getName());
            throw new RuntimeException((new StringBuilder("Unexpected type for bundle response code: ")).append(obj.getClass().getName()).toString());
        }
    }

    int getResponseCodeFromIntent(Intent intent) {
        Object obj = intent.getExtras().get("RESPONSE_CODE");
        if (obj == null) {
            logError("Intent with no response code, assuming OK (known issue)");
            return 0;
        }
        if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        }
        if (obj instanceof Long) {
            return (int) ((Long) obj).longValue();
        } else {
            logError("Unexpected type for intent response code.");
            logError(obj.getClass().getName());
            throw new RuntimeException((new StringBuilder("Unexpected type for intent response code: ")).append(obj.getClass().getName()).toString());
        }
    }

    public boolean handleActivityResult(int i, int j, Intent intent) {
        int k;
        String s;
        String s1;
        try {
            if (i != mRequestCode) {
                return false;
            }
            checkNotDisposed();
            checkSetupDone("handleActivityResult");
            flagEndAsync();
            if (intent == null) {
                logError("Null data in IAB activity result.");
                IabResult iabresult6 = new IabResult(-1002, "Null data in IAB result");
                if (mPurchaseListener != null) {
                    mPurchaseListener.onIabPurchaseFinished(iabresult6, null);
                }
                return true;
            }
            k = getResponseCodeFromIntent(intent);
            s = intent.getStringExtra("INAPP_PURCHASE_DATA");
            s1 = intent.getStringExtra("INAPP_DATA_SIGNATURE");
            if (j != -1 || k != 0) {
                if (j == -1) {
                    logDebug((new StringBuilder("Result code was OK but in-app billing response was not OK: ")).append(getResponseDesc(k)).toString());
                    if (mPurchaseListener != null) {
                        IabResult iabresult2 = new IabResult(k, "Problem purchashing item.");
                        mPurchaseListener.onIabPurchaseFinished(iabresult2, null);
                    }
                } else if (j == 0) {
                    logDebug((new StringBuilder("Purchase canceled - Response: ")).append(getResponseDesc(k)).toString());
                    IabResult iabresult1 = new IabResult(-1005, "User canceled.");
                    if (mPurchaseListener != null) {
                        mPurchaseListener.onIabPurchaseFinished(iabresult1, null);
                    }
                } else {
                    logError((new StringBuilder("Purchase failed. Result code: ")).append(Integer.toString(j)).append(". Response: ").append(getResponseDesc(k)).toString());
                    IabResult iabresult = new IabResult(-1006, "Unknown purchase response.");
                    if (mPurchaseListener != null) {
                        mPurchaseListener.onIabPurchaseFinished(iabresult, null);
                    }
                }
                return true;
            }
            logDebug("Successful resultcode from purchase activity.");
            logDebug((new StringBuilder("Purchase data: ")).append(s).toString());
            logDebug((new StringBuilder("Data signature: ")).append(s1).toString());
            logDebug((new StringBuilder("Extras: ")).append(intent.getExtras()).toString());
            logDebug((new StringBuilder("Expected item type: ")).append(mPurchasingItemType).toString());
            if (s == null || s1 == null) {
                logError("BUG: either purchaseData or dataSignature is null.");
                logDebug((new StringBuilder("Extras: ")).append(intent.getExtras().toString()).toString());
                IabResult iabresult3 = new IabResult(-1008, "IAB returned null purchaseData or dataSignature");
                if (mPurchaseListener != null) {
                    mPurchaseListener.onIabPurchaseFinished(iabresult3, null);
                }
                return true;
            }
            Purchase purchase = new Purchase(mPurchasingItemType, s, s1);
            String s2 = purchase.getSku();
            if (Security.verifyPurchase(mSignatureBase64, s, s1)) {
                logDebug("Purchase signature successfully verified.");
                if (mPurchaseListener != null) {
                    mPurchaseListener.onIabPurchaseFinished(new IabResult(0, "Success"), purchase);
                }
                return true;
            }
            logError((new StringBuilder("Purchase signature verification FAILED for sku ")).append(s2).toString());
            IabResult iabresult5 = new IabResult(-1003, (new StringBuilder("Signature verification failed for sku ")).append(s2).toString());
            if (mPurchaseListener != null) {
                mPurchaseListener.onIabPurchaseFinished(iabresult5, purchase);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void launchPurchaseFlow(Activity activity, String s, int i, OnIabPurchaseFinishedListener oniabpurchasefinishedlistener) {
        launchPurchaseFlow(activity, s, i, oniabpurchasefinishedlistener, "");
    }

    public void launchPurchaseFlow(Activity activity, String s, int i, OnIabPurchaseFinishedListener oniabpurchasefinishedlistener, String s1) {
        launchPurchaseFlow(activity, s, "inapp", i, oniabpurchasefinishedlistener, s1);
    }

    public void launchPurchaseFlow(Activity activity, String s, String s1, int i, OnIabPurchaseFinishedListener oniabpurchasefinishedlistener, String s2) {
        try {
            checkNotDisposed();
            checkSetupDone("launchPurchaseFlow");
            flagStartAsync("launchPurchaseFlow");
            if (!(!s1.equals("subs") || mSubscriptionsSupported)) {
                IabResult iabresult3 = new IabResult(-1009, "Subscriptions are not available.");
                flagEndAsync();
                if (oniabpurchasefinishedlistener != null) {
                    oniabpurchasefinishedlistener.onIabPurchaseFinished(iabresult3, null);
                }
                return;
            }
            Bundle bundle;
            int j;
            IabResult iabresult2;
            PendingIntent pendingintent;
            logDebug((new StringBuilder("Constructing buy intent for ")).append(s).append(", item type: ").append(s1).toString());
            //bundle = mService.getBuyIntent(3, mContext.getPackageName(), s, s1, s2);
            j = 1;
            if (j == 0) {
                //pendingintent = (PendingIntent)bundle.getParcelable("BUY_INTENT");
                logDebug((new StringBuilder("Launching buy intent for ")).append(s).append(". Request code: ").append(i).toString());
                mRequestCode = i;
                mPurchaseListener = oniabpurchasefinishedlistener;
                mPurchasingItemType = s1;
                // activity.startIntentSenderForResult(pendingintent.getIntentSender(), i, new Intent(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue());
                return;
            }
            logError((new StringBuilder("Unable to buy item, Error response: ")).append(getResponseDesc(j)).toString());
            flagEndAsync();
            iabresult2 = new IabResult(j, "Unable to buy item");
            if (oniabpurchasefinishedlistener != null) {
                oniabpurchasefinishedlistener.onIabPurchaseFinished(iabresult2, null);
                return;
            }
        } catch (Exception e) {
        }

    }

    public void launchSubscriptionPurchaseFlow(Activity activity, String s, int i, OnIabPurchaseFinishedListener oniabpurchasefinishedlistener) {
        launchSubscriptionPurchaseFlow(activity, s, i, oniabpurchasefinishedlistener, "");
    }

    public void launchSubscriptionPurchaseFlow(Activity activity, String s, int i, OnIabPurchaseFinishedListener oniabpurchasefinishedlistener, String s1) {
        launchPurchaseFlow(activity, s, "subs", i, oniabpurchasefinishedlistener, s1);
    }

    void logDebug(String s) {
        if (mDebugLog) {
            Log.d(mDebugTag, s);
        }
    }

    void logError(String s) {
        Log.e(mDebugTag, (new StringBuilder("In-app billing error: ")).append(s).toString());
    }

    void logWarn(String s) {
        Log.w(mDebugTag, (new StringBuilder("In-app billing warning: ")).append(s).toString());
    }

    public Inventory queryInventory(boolean flag, List list)
            throws IabException {
        return queryInventory(flag, list, null);
    }

    public Inventory queryInventory(boolean flag, List list, List list1)
            throws IabException {
        Inventory inventory;
        checkNotDisposed();
        checkSetupDone("queryInventory");
        int i;
        int l;
        try {
            inventory = new Inventory();
            i = queryPurchases(inventory, "inapp");
            if (i == 0) {
                if (flag) {
                    l = querySkuDetails("inapp", inventory, list);
                    if (l != 0) {
                        throw new IabException(l, "Error refreshing inventory (querying prices of items).");
                    }
                }
                int j;
                if (!mSubscriptionsSupported) {
                    return inventory;
                }
                j = queryPurchases(inventory, "subs");
                if (j == 0) {
                    if (!flag) {
                        return inventory;
                    }
                    int k = querySkuDetails("subs", inventory, list);
                    if (k == 0) {
                        return inventory;
                    }
                    throw new IabException(k, "Error refreshing inventory (querying prices of subscriptions).");

                }
                throw new IabException(j, "Error refreshing inventory (querying owned subscriptions).");
            }
            throw new IabException(i, "Error refreshing inventory (querying owned items).");
        } catch (RemoteException remoteexception) {
            throw new IabException(-1001, "Remote exception while refreshing inventory.", remoteexception);
        } catch (JSONException jsonexception) {
            throw new IabException(-1002, "Error parsing JSON response while refreshing inventory.", jsonexception);
        }
    }

    public void queryInventoryAsync(QueryInventoryFinishedListener queryinventoryfinishedlistener) {
        queryInventoryAsync(true, null, queryinventoryfinishedlistener);
    }

    public void queryInventoryAsync(boolean flag, QueryInventoryFinishedListener queryinventoryfinishedlistener) {
        queryInventoryAsync(flag, null, queryinventoryfinishedlistener);
    }

    public void queryInventoryAsync(final boolean querySkuDetails, final List moreSkus, final QueryInventoryFinishedListener listener) {
        final Handler handler = new Handler();
        checkNotDisposed();
        checkSetupDone("queryInventory");
        flagStartAsync("refresh inventory");
        (new Thread(new Runnable() {
            public void run() {
                IabResult iabresult;
                Inventory inventory;
                try {
                    iabresult = new IabResult(0, "Inventory refresh successful.");
                    Inventory inventory2 = queryInventory(querySkuDetails, moreSkus);
                    inventory = inventory2;
                } catch (IabException iabexception) {
                    iabresult = iabexception.getResult();
                    inventory = null;
                }
                flagEndAsync();
                final IabResult result_f = iabresult;
                final Inventory inventory1 = inventory;
                if (!mDisposed && listener != null) {
                    handler.post(new Runnable() {
                        public void run() {
                            listener.onQueryInventoryFinished(result_f, inventory1);
                        }
                    });
                }
            }
        })).start();
    }

    int queryPurchases(Inventory inventory, String s)
            throws JSONException, RemoteException {
        boolean flag;
        String s1;
        Bundle bundle;
        ArrayList arraylist;
        ArrayList arraylist1;
        ArrayList arraylist2;
        int j;
        String s2;
        String s3;
        String s4;
        Purchase purchase;
        char c;
        logDebug((new StringBuilder("Querying owned items, item type: ")).append(s).toString());
        logDebug((new StringBuilder("Package name: ")).append(mContext.getPackageName()).toString());
        flag = false;
        s1 = null;
        while (true) {
            logDebug((new StringBuilder("Calling getPurchases with continuation token: ")).append(s1).toString());
            //bundle = mService.getPurchases(3, mContext.getPackageName(), s, s1);
            int i = 1;//getResponseCodeFromBundle(bundle);
            logDebug((new StringBuilder("Owned items response: ")).append(String.valueOf(i)).toString());
            if (i != 0) {
                logDebug((new StringBuilder("getPurchases() failed: ")).append(getResponseDesc(i)).toString());
                return i;
            }
           /* if(!bundle.containsKey("INAPP_PURCHASE_ITEM_LIST") || !bundle.containsKey("INAPP_PURCHASE_DATA_LIST") || !bundle.containsKey("INAPP_DATA_SIGNATURE_LIST"))
	        {
	            logError("Bundle returned from getPurchases() doesn't contain required fields.");
	            return -1002;
	        }*/
	       /* arraylist = bundle.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
	        arraylist1 = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
	        arraylist2 = bundle.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
	        j = 0;
	        while(j < arraylist1.size())
	        {
	        	s2 = (String)arraylist1.get(j);
	            s3 = (String)arraylist2.get(j);
	            s4 = (String)arraylist.get(j);
	            if(Security.verifyPurchase(mSignatureBase64, s2, s3))
	            {
	                logDebug((new StringBuilder("Sku is owned: ")).append(s4).toString());
	                purchase = new Purchase(s, s2, s3);
	                if(TextUtils.isEmpty(purchase.getToken()))
	                {
	                    logWarn("BUG: empty/null token!");
	                    logDebug((new StringBuilder("Purchase data: ")).append(s2).toString());
	                }
	                inventory.addPurchase(purchase);
	            } else
	            {
	                logWarn("Purchase signature verification **FAILED**. Not adding item.");
	                logDebug((new StringBuilder("   Purchase data: ")).append(s2).toString());
	                logDebug((new StringBuilder("   Signature: ")).append(s3).toString());
	                flag = true;
	            }
	            j++;
	        }
	        s1 = bundle.getString("INAPP_CONTINUATION_TOKEN");
	        logDebug((new StringBuilder("Continuation token: ")).append(s1).toString());
	        if(TextUtils.isEmpty(s1))
	        {
	            if(flag)
	            {
	                c = '\uFC15';
	            } else
	            {
	                c = '\0';
	            }
	            return c;
	        }
        }*/

            return 1;
        }
    }

    int querySkuDetails(String s, Inventory inventory, List list)
            throws RemoteException, JSONException {
        ArrayList arraylist;
        String s1;
        logDebug("Querying SKU details.");
        arraylist = new ArrayList();
        arraylist.addAll(inventory.getAllOwnedSkus(s));
        if (list != null) {
            Iterator iterator1 = list.iterator();
            while (iterator1.hasNext()) {
                s1 = (String) iterator1.next();
                if (!arraylist.contains(s1)) {
                    arraylist.add(s1);
                }
            }
        }
        if (arraylist.size() == 0) {
            logDebug("queryPrices: nothing to do because there are no SKUs.");
        } else {
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("ITEM_ID_LIST", arraylist);
            Bundle bundle1 = null;//mService.getSkuDetails(3, mContext.getPackageName(), s, bundle);
            if (bundle != null && !bundle1.containsKey("DETAILS_LIST")) {
                int i = getResponseCodeFromBundle(bundle1);
                if (i != 0) {
                    logDebug((new StringBuilder("getSkuDetails() failed: ")).append(getResponseDesc(i)).toString());
                    return i;
                } else {
                    logError("getSkuDetails() returned a bundle with neither an error nor a detail list.");
                    return -1002;
                }
            }
            Iterator iterator = bundle1.getStringArrayList("DETAILS_LIST").iterator();
            while (iterator.hasNext()) {
                SkuDetails skudetails = new SkuDetails(s, (String) iterator.next());
                logDebug((new StringBuilder("Got sku details: ")).append(skudetails).toString());
                inventory.addSkuDetails(skudetails);
            }
        }
        return 0;
    }

    public void startSetup(final OnIabSetupFinishedListener listener) {
        checkNotDisposed();
        if (mSetupDone) {
            throw new IllegalStateException("IAB helper is already set up.");
        }
        logDebug("Starting in-app billing setup.");
        mServiceConn = new ServiceConnection() {
            public void onServiceConnected(ComponentName componentname, IBinder ibinder) {
                if (mDisposed)
                    return;
                String s;
                logDebug("Billing service connected.");
                //mService = com.android.vending.billing.IInAppBillingService.Stub.asInterface(ibinder);
                s = mContext.getPackageName();
                int i;
                // try
                // {
                logDebug("Checking for in-app billing 3 support.");
                i = 1;//mService.isBillingSupported(3, s, "inapp");
                if (i == 0) {
                    int j;
                    logDebug((new StringBuilder("In-app billing version 3 supported for ")).append(s).toString());
                    j = 1;//mService.isBillingSupported(3, s, "subs");
                    if (j != 0) {
                        logDebug((new StringBuilder("Subscriptions NOT AVAILABLE. Response: ")).append(j).toString());
                    } else {
                        logDebug("Subscriptions AVAILABLE.");
                        mSubscriptionsSupported = true;
                    }
                    mSetupDone = true;
                    if (listener == null)
                        return;
                    listener.onIabSetupFinished(new IabResult(0, "Setup successful."));
                    return;
                }
                if (listener != null) {
                    listener.onIabSetupFinished(new IabResult(i, "Error checking for billing v3 support."));
                }
                mSubscriptionsSupported = false;
                //}
                /*catch(RemoteException remoteexception)
                {
                    if(listener != null)
                    {
                        listener.onIabSetupFinished(new IabResult(-1001, "RemoteException while setting up in-app billing."));
                    }
                    remoteexception.printStackTrace();
                }*/
            }

            public void onServiceDisconnected(ComponentName componentname) {
                logDebug("Billing service disconnected.");
                //mService = null;
            }
        };
        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        if (!mContext.getPackageManager().queryIntentServices(intent, 0).isEmpty()) {
            mContext.bindService(intent, mServiceConn, Context.BIND_AUTO_CREATE);
        } else if (listener != null) {
            listener.onIabSetupFinished(new IabResult(3, "Billing service unavailable on device."));
            return;
        }
    }

    public boolean subscriptionsSupported() {
        checkNotDisposed();
        return mSubscriptionsSupported;
    }

    public static interface OnConsumeFinishedListener {

        public abstract void onConsumeFinished(Purchase purchase, IabResult iabresult);
    }

    public static interface OnConsumeMultiFinishedListener {

        public abstract void onConsumeMultiFinished(List list, List list1);
    }

    public static interface OnIabPurchaseFinishedListener {

        public abstract void onIabPurchaseFinished(IabResult iabresult, Purchase purchase);
    }


    public static interface OnIabSetupFinishedListener {

        public abstract void onIabSetupFinished(IabResult iabresult);
    }

    public static interface QueryInventoryFinishedListener {

        public abstract void onQueryInventoryFinished(IabResult iabresult, Inventory inventory);
    }
}
