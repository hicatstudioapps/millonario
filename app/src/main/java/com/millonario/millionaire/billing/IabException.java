package com.millonario.millionaire.billing;


// Referenced classes of package com.millonario.millionaire.billing:
//            IabResult

public class IabException extends Exception {

    private static final long serialVersionUID = 0x4acb25b90f223cf5L;
    IabResult mResult;

    public IabException(int i, String s) {
        this(new IabResult(i, s));
    }

    public IabException(int i, String s, Exception exception) {
        this(new IabResult(i, s), exception);
    }

    public IabException(IabResult iabresult) {
        this(iabresult, ((Exception) (null)));
    }

    public IabException(IabResult iabresult, Exception exception) {
        super(iabresult.getMessage(), exception);
        mResult = iabresult;
    }

    public IabResult getResult() {
        return mResult;
    }
}
