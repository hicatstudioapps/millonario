package com.millonario.millionaire.billing;

import org.json.JSONException;
import org.json.JSONObject;

public class Purchase {

    String mDeveloperPayload;
    String mItemType;
    String mOrderId;
    String mOriginalJson;
    String mPackageName;
    int mPurchaseState;
    long mPurchaseTime;
    String mSignature;
    String mSku;
    String mToken;

    public Purchase(String s, String s1, String s2)
            throws JSONException {
        mItemType = s;
        mOriginalJson = s1;
        JSONObject jsonobject = new JSONObject(mOriginalJson);
        mOrderId = jsonobject.optString("orderId");
        mPackageName = jsonobject.optString("packageName");
        mSku = jsonobject.optString("productId");
        mPurchaseTime = jsonobject.optLong("purchaseTime");
        mPurchaseState = jsonobject.optInt("purchaseState");
        mDeveloperPayload = jsonobject.optString("developerPayload");
        mToken = jsonobject.optString("token", jsonobject.optString("purchaseToken"));
        mSignature = s2;
    }

    public String getDeveloperPayload() {
        return mDeveloperPayload;
    }

    public String getItemType() {
        return mItemType;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public String getOriginalJson() {
        return mOriginalJson;
    }

    public String getPackageName() {
        return mPackageName;
    }

    public int getPurchaseState() {
        return mPurchaseState;
    }

    public long getPurchaseTime() {
        return mPurchaseTime;
    }

    public String getSignature() {
        return mSignature;
    }

    public String getSku() {
        return mSku;
    }

    public String getToken() {
        return mToken;
    }

    public String toString() {
        return (new StringBuilder("PurchaseInfo(type:")).append(mItemType).append("):").append(mOriginalJson).toString();
    }
}
