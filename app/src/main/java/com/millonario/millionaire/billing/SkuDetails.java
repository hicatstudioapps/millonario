package com.millonario.millionaire.billing;

import org.json.JSONException;
import org.json.JSONObject;

public class SkuDetails {

    String mDescription;
    String mItemType;
    String mJson;
    String mPrice;
    String mSku;
    String mTitle;
    String mType;

    public SkuDetails(String s)
            throws JSONException {
        this("inapp", s);
    }

    public SkuDetails(String s, String s1)
            throws JSONException {
        mItemType = s;
        mJson = s1;
        JSONObject jsonobject = new JSONObject(mJson);
        mSku = jsonobject.optString("productId");
        mType = jsonobject.optString("type");
        mPrice = jsonobject.optString("price");
        mTitle = jsonobject.optString("title");
        mDescription = jsonobject.optString("description");
    }

    public String getDescription() {
        return mDescription;
    }

    public String getPrice() {
        return mPrice;
    }

    public String getSku() {
        return mSku;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getType() {
        return mType;
    }

    public String toString() {
        return (new StringBuilder("SkuDetails:")).append(mJson).toString();
    }
}
