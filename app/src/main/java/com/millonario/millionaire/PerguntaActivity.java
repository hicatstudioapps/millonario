package com.millonario.millionaire;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.classes.SuperActivity;
import com.millonario.millionaire.fragments.GameOverFragment;
import com.millonario.millionaire.fragments.MilionarioFragment;
import com.millonario.millionaire.fragments.PatamarFragment;
import com.millonario.millionaire.fragments.PerguntaFragment;
import com.millonario.millionaire.fragments.SuperFragment;

// Referenced classes of package com.millonario.millionaire:
//            AjudasExtraActivity
@SuppressWarnings("ResourceType")
public class PerguntaActivity extends SuperActivity {

    SuperFragment currentFragment;
    private AdView mAdView;

    public PerguntaActivity() {
    }

    private void nextFragment() {
        nextFragment(false);
    }

    private void nextFragment(boolean flag) {
        FragmentTransaction fragmenttransaction = getSupportFragmentManager().beginTransaction();
        if (flag) {
            fragmenttransaction.setCustomAnimations(0x10a0002, 0x10a0003);
        } else {
            fragmenttransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        fragmenttransaction.replace(R.id.fl_gameContainer, currentFragment, currentFragment.getNameTag());
        fragmenttransaction.commit();
    }

    public void nextQuestion(boolean flag) {
        //Analytics.sendPageView("PerguntaActivity");
        currentFragment = new PerguntaFragment(App.PerguntaAtual);
        boolean flag1;
        if (flag) {
            flag1 = false;
        } else {
            flag1 = true;
        }
        nextFragment(flag1);
    }

    public void onBackPressed() {
        if (currentFragment != null) {
            currentFragment.onBackPressed();
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_base_pergunta);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                if (mAdView.getVisibility() != View.VISIBLE) {
                    mAdView.setVisibility(View.VISIBLE);
                }
            }
        });
        // Start loading the ad in the background.
        mAdView.loadAd(adRequest);
        currentFragment = new PerguntaFragment(App.PerguntaAtual);
        FragmentTransaction fragmenttransaction = getSupportFragmentManager().beginTransaction();
        fragmenttransaction.add(R.id.fl_gameContainer, currentFragment, currentFragment.getNameTag());
        fragmenttransaction.commit();
    }

    public void onPause() {
        if (App.isContraRelogio && (currentFragment instanceof PerguntaFragment)) {
            App.Musica.paraContraRelogio();
            App.Musica.start();
            App.Musica.realPause();
            ((PerguntaFragment) currentFragment).shouldPlayMusicaContraRelogio = false;
            ((PerguntaFragment) currentFragment).shouldPlayMusicaNormal = false;
            finish();
        }
        super.onPause();
    }

    public void reportarSucesso(boolean flag) {
        if (currentFragment != null) {
            currentFragment.reportarSucesso(flag);
        }
    }

    public boolean shouldPlayMusicaContraRelogio() {
        if (currentFragment != null && (currentFragment instanceof PerguntaFragment)) {
            return ((PerguntaFragment) currentFragment).shouldPlayMusicaContraRelogio;
        } else {
            return false;
        }
    }

    public boolean shouldPlayMusicaNormal() {
        if (currentFragment != null && (currentFragment instanceof PerguntaFragment)) {
            return ((PerguntaFragment) currentFragment).shouldPlayMusicaNormal;
        } else {
            return true;
        }
    }

    public void showExtraLifeLines() {
        startActivity(new Intent(this, com.millonario.millionaire.AjudasExtraActivity.class));
    }

    public void showGameOver() {
        //Analytics.sendPageView("GameOverActivity");
        currentFragment = new GameOverFragment();
        nextFragment();
    }

    public void showMilionario() {
        //Analytics.sendPageView("MilionarioActivity");
        currentFragment = new MilionarioFragment();
        nextFragment();
    }

    public void showPatamares() {
        //Analytics.sendPageView("PatamarActivity");
        currentFragment = new PatamarFragment();
        nextFragment();
    }
}
