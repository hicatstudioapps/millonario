package com.millonario.millionaire;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.classes.SuperActivity;
import com.millonario.millionaire.utils.TestConnection;
import com.millonario.millionaire.utils.Utils;
import com.nineoldandroids.animation.ObjectAnimator;

import java.security.MessageDigest;

// Referenced classes of package com.millonario.millionaire:
//            EcraPrincipalActivity

public class SplashActivity extends SuperActivity {

    public SplashActivity() {
    }



    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.splash_screen);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.millonario.millionaire",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception ex) {

        }
        ImageView imageview = (ImageView) findViewById(R.id.imageView1);
        String s;
        if (Utils.isChristmas()) {
            s = "img_principal_xmas";
        } else {
            s = "img_principal";
        }
        imageview.setImageResource(App.getMyDrawable(s));
        //loadCrossPromotion();
    }

    public void onResume() {
        super.onResume();
//        ObjectAnimator objectanimator = ObjectAnimator.ofFloat(findViewById(R.id.img_bg), "alpha", new float[]{
//                0.0F, 0.5F, 1.0F
//        });
//        objectanimator.setDuration(1000L);
//        objectanimator.setRepeatCount(0);
//        objectanimator.start();
//        ObjectAnimator objectanimator1 = ObjectAnimator.ofFloat(findViewById(R.id.imageView1), "alpha", new float[]{
//                0.0F, 0.5F, 1.0F
//        });
//        objectanimator1.setDuration(2000L);
//        objectanimator1.setRepeatCount(0);
//        objectanimator1.start();
        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashActivity.this, com.millonario.millionaire.EcraPrincipalActivity.class));
//                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }, 3000L);
    }
}
