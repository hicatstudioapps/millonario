package com.millonario.millionaire.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.text.Html;
import android.util.Patterns;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.millonario.millionaire.PerguntaActivity;
import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.objetos.Pergunta;
import com.millonario.millionaire.popup.PopUp;
//import com.millonario.millionaire.task.ReportarPergunta;
import com.millonario.millionaire.utils.Ajuda;
import com.millonario.millionaire.utils.Conquistas;
import com.millonario.millionaire.utils.Utils;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

// Referenced classes of package com.millonario.millionaire.fragments:
//            SuperFragment

@SuppressWarnings("ResourceType")
public class PerguntaFragment extends SuperFragment {
    public static final int SCREEN_EXTRA_LIFELINES = 5;
    public static final int SCREEN_GAME_OVER = 3;
    public static final int SCREEN_MILIONARIO = 4;
    public static final int SCREEN_PATAMAR = 2;
    public static final int SCREEN_QUESTION = 1;
    public boolean shouldPlayMusicaContraRelogio;
    public boolean shouldPlayMusicaNormal;
    SparseArray animacoes;
    int countAnimacao;
    Drawable defaultDrawable;
    String emailReportar;
    boolean interacaoAtiva;
    boolean isLoadingPergunta;
    String motivo;
    int numAjudasDisponivel;
    ProgressBar pb;
    Pergunta pergunta;
    int perguntaIndex;
    boolean respostaDada;
    int tempoBase;
    int tempoIntervalo;
    int tempoTotal;
    Timer timer;
    int trocaAjudaCount;
    private AdView mAdView;

    public PerguntaFragment() {
        perguntaIndex = 0;
        isLoadingPergunta = false;
        interacaoAtiva = true;
        respostaDada = false;
        pergunta = null;
        countAnimacao = 0;
        tempoBase = 10000;
        tempoIntervalo = 1200;
        numAjudasDisponivel = 2;
        shouldPlayMusicaContraRelogio = false;
        shouldPlayMusicaNormal = true;
        tempoTotal = 15;
        trocaAjudaCount = 0;
        motivo = null;
        emailReportar = null;
        perguntaIndex = App.PerguntaAtual;
    }

    @SuppressLint("ValidFragment")
    public PerguntaFragment(int i) {
        perguntaIndex = 0;
        isLoadingPergunta = false;
        interacaoAtiva = true;
        respostaDada = false;
        pergunta = null;
        countAnimacao = 0;
        tempoBase = 10000;
        tempoIntervalo = 1200;
        numAjudasDisponivel = 2;
        shouldPlayMusicaContraRelogio = false;
        shouldPlayMusicaNormal = true;
        tempoTotal = 15;
        trocaAjudaCount = 0;
        motivo = null;
        emailReportar = null;
        perguntaIndex = i;
    }

    private void GuardaDinheiro() {
        String s = App.DinheiroAtual;
        if (s == null) {
            s = "0";
        }
        String s1 = s.replace(" ", "").replace(".", "").replace(",", "").replace(Html.fromHtml(getString(R.string.money_symbol)).toString(), "").replace(Html.fromHtml("&euro;").toString(), "").replace("$", "");
        App.DB().updateDinheiro(Float.parseFloat(s1));
        String s2 = App.DB().getValue("Dinheiro");
        if (s2 == null || s2.equals("") || s2.equals("<null>")) {
            s2 = "0";
        }
        Conquistas.verificaConquista(Float.parseFloat(s2), getActivity());
        App.PerguntaAtual = 0;
        App.Perguntas = null;
        App.PerguntasTroca = null;
        App.DinheiroAtual = "0";
    }

    private void animateHelpLine(final View v, long l) {
        if (v.getId() == R.id.btn_ajuda_5050 && App.is5050Disponivel >= numAjudasDisponivel || v.getId() == R.id.btn_ajuda_publico && App.isPublicoDisponivel >= numAjudasDisponivel || v.getId() == R.id.btn_ajuda_troca && App.isTrocaDisponivel >= numAjudasDisponivel || v.getId() == R.id.btn_ajuda_telefone && App.isTelefoneDisponivel >= numAjudasDisponivel) {
            return;
        } else {
            ObjectAnimator objectanimator = ObjectAnimator.ofFloat(v, "rotationY", new float[]{
                    0.0F, 180F, 360F
            });
            objectanimator.setDuration(1500L);
            objectanimator.setRepeatCount(0);
            objectanimator.setStartDelay((long) tempoBase + l * (long) tempoIntervalo);
            objectanimator.addListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
                public void onAnimationCancel(Animator animator) {
                    if ((v.getId() != R.id.btn_ajuda_5050 || App.is5050Disponivel < numAjudasDisponivel) && (v.getId() != R.id.btn_ajuda_publico || App.isPublicoDisponivel < numAjudasDisponivel) && (v.getId() != R.id.btn_ajuda_troca || App.isTrocaDisponivel < numAjudasDisponivel) && (v.getId() != R.id.btn_ajuda_telefone || App.isTelefoneDisponivel < numAjudasDisponivel)) {
                        ViewHelper.setRotationY(v, 0.0F);
                    }
                }

                public void onAnimationEnd(Animator animator) {
                    animateHelpLine(v, 0L);
                }

                public void onAnimationRepeat(Animator animator) {
                }

                public void onAnimationStart(Animator animator) {
                    if ((v.getId() != R.id.btn_ajuda_5050 || App.is5050Disponivel < numAjudasDisponivel) && (v.getId() != R.id.btn_ajuda_publico || App.isPublicoDisponivel < numAjudasDisponivel) && (v.getId() != R.id.btn_ajuda_troca || App.isTrocaDisponivel < numAjudasDisponivel) && (v.getId() != R.id.btn_ajuda_telefone || App.isTelefoneDisponivel < numAjudasDisponivel) || !((ObjectAnimator) animator).getPropertyName().equals("rotationY")) {
                        return;
                    }
                    animator.cancel();
                }
            });
            objectanimator.start();
            animacoes.put(v.getId(), objectanimator);
            return;
        }
    }

    private void checkHelpLine(View view, int i) {
        if (i >= numAjudasDisponivel) {
            // view.setBackgroundResource(App.getMyDrawable("bg_ajuda_empty"));
            ViewHelper.setAlpha(view, 0.7F);
            view.setContentDescription((new StringBuilder()).append(view.getContentDescription()).append(" - ").append(getString(R.string.ajuda_ja_utilizada)).toString());
            return;
        }
        if (!App.isContraRelogio && i > 0) {
            // view.setBackgroundResource(App.getMyDrawable("bg_ajuda_half"));
        }
        ViewHelper.setAlpha(view, 1.0F);
    }

    private String getDinheiro(int i) {
        String s = getString(R.string.money_format, new Object[]{
                "0"
        });
        switch (i) {
            default:
                return s;

            case 0: // '\0'
                return getString(R.string.patamar_1);

            case 1: // '\001'
                return getString(R.string.patamar_2);

            case 2: // '\002'
                return getString(R.string.patamar_3);

            case 3: // '\003'
                return getString(R.string.patamar_4);

            case 4: // '\004'
                return getString(R.string.patamar_5);

            case 5: // '\005'
                return getString(R.string.patamar_6);

            case 6: // '\006'
                return getString(R.string.patamar_7);

            case 7: // '\007'
                return getString(R.string.patamar_8);

            case 8: // '\b'
                return getString(R.string.patamar_9);

            case 9: // '\t'
                return getString(R.string.patamar_10);

            case 10: // '\n'
                return getString(R.string.patamar_11);

            case 11: // '\013'
                return getString(R.string.patamar_12);

            case 12: // '\f'
                return getString(R.string.patamar_13);

            case 13: // '\r'
                return getString(R.string.patamar_14);

            case 14: // '\016'
                return getString(R.string.patamar_15);
        }
    }

    private void initViews() {
        animacoes = new SparseArray();
        App.getPerguntas(false);
        interacaoAtiva = true;
        loadPergunta();
        View view = findViewById(R.id.btn_ajuda_5050);
        View view1 = findViewById(R.id.btn_ajuda_publico);
        View view2 = findViewById(R.id.btn_ajuda_troca);
        View view3 = findViewById(R.id.btn_ajuda_telefone);
        checkHelpLine(view, App.is5050Disponivel);
        checkHelpLine(view1, App.isPublicoDisponivel);
        checkHelpLine(view2, App.isTrocaDisponivel);
        checkHelpLine(view3, App.isTelefoneDisponivel);
        animateHelpLine(view2, 0L);
        animateHelpLine(view1, 1L);
        animateHelpLine(view3, 2L);
        animateHelpLine(view, 3L);
        TextView textview = (TextView) findViewById(R.id.txt_totalAtual);
        int i = R.string.money_format;
        Object aobj[] = new Object[1];
        aobj[0] = App.DinheiroAtual.replace(getString(R.string.money_format, new Object[]{
                ""
        }), "");
        textview.setText(getString(i, aobj));
        TextView textview1 = (TextView) findViewById(R.id.txt_totalAtual);
        int j = R.string.money_format;
        Object aobj1[] = new Object[1];
        aobj1[0] = App.DinheiroAtual.replace(getString(R.string.money_format, new Object[]{
                ""
        }), "");
        textview1.setContentDescription(getString(j, aobj1));
        findViewById(R.id.btn_ajuda_5050).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view4) {
                Retira2Btn_click(view4);
            }
        });
        findViewById(R.id.btn_ajuda_publico).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view4) {
                PublicoBtn_click(view4);
            }
        });
        findViewById(R.id.btn_ajuda_telefone).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view4) {
                TelefoneBtn_click(view4);
            }
        });
        findViewById(R.id.btn_ajuda_troca).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view4) {
                TrocaBtn_click(view4);
            }
        });
        findViewById(R.id.btn_reportar).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view4) {
//                ReportarBtn_click(view4);
            }
        });
        findViewById(R.id.btn_sair).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view4) {
                SairCustomBtn_click(view4);
            }
        });
        findViewById(R.id.icn_arrowRight).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view4) {
                SairCustomBtn_click(view4);
            }
        });
    }

    private boolean isEmailValido(String s) {
        boolean flag = true;
        if (Utils.isEmpty(s) || !Patterns.EMAIL_ADDRESS.matcher(s).matches()) {
            flag = false;
            PopUp.show(getActivity(), 0, 1);
            PopUp.setMensagem(getString(R.string.erro_email_incorreto));
            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
                public void onClick(View view) {
                    PopUp.hide();
                    reportarSucesso(false);
                }
            }, 1);
        }
        return flag;
    }

    private void loadPergunta() {
        if (isLoadingPergunta) {
            return;
        }
        isLoadingPergunta = true;
        ArrayList arraylist;
        int i;
        String as[];
        Iterator iterator;
        if (!App.isContraRelogio) {
            findViewById(R.id.pb_contraRelogio).setVisibility(4);
        } else {
            setContraRelogio();
        }
        pergunta = App.getPerguntaAtual();
        ((TextView) findViewById(R.id.txt_pergunta)).setText(pergunta.Pergunta);
        ((TextView) findViewById(R.id.txt_pergunta)).setContentDescription(pergunta.Pergunta);
        arraylist = pergunta.getRespostas();
        i = 1;
        as = new String[4];
        as[0] = getString(R.string.resposta_a);
        as[1] = getString(R.string.resposta_b);
        as[2] = getString(R.string.resposta_c);
        as[3] = getString(R.string.resposta_d);
        iterator = arraylist.iterator();
        do {
            if (!iterator.hasNext()) {
                interacaoAtiva = true;
                isLoadingPergunta = false;
                return;
            }
            String s = (String) iterator.next();
            Button button = (Button) findViewById(getResources().getIdentifier((new StringBuilder("btn_resposta_")).append(i).toString(), "id", getActivity().getPackageName()));
            button.setText(s);
            button.setContentDescription((new StringBuilder(String.valueOf(as[i - 1]))).append(": ").append(s).toString());
            button.setEnabled(true);
            button.setVisibility(0);
            button.setOnClickListener(new android.view.View.OnClickListener() {
                public void onClick(View view) {
                    RespostaSelecionada(view);
                }
            });
            i++;
        } while (true);
    }

    public void PublicoBtn_click(View view) {
        label0:
        {
            if (interacaoAtiva && App.isPublicoDisponivel < numAjudasDisponivel) {
                interacaoAtiva = false;
                //Analytics.send("Ajuda - P\372blico", "", "");
                if (App.isPublicoDisponivel <= 0 || Utils.getNumeroAjudasExtra() != 0) {
                    break label0;
                }
                interacaoAtiva = true;
                startActivity(5);
            }
            return;
        }
        if (App.isPublicoDisponivel > 0) {
            Utils.removeAjudaExtra();
        }
        App.isPublicoDisponivel = 1 + App.isPublicoDisponivel;
        int ai[];
        View view1;
        int i;
        final Dialog dialog;
        if (App.isPublicoDisponivel >= numAjudasDisponivel) {
            //view.setBackgroundResource(App.getMyDrawable("bg_ajuda_empty"));
            ViewHelper.setAlpha(view, 0.7F);
            view.setContentDescription((new StringBuilder()).append(view.getContentDescription()).append(" - ").append(getString(R.string.ajuda_ja_utilizada)).toString());
        } else {
            if (!App.isContraRelogio && App.isPublicoDisponivel > 0) {
                // view.setBackgroundResource(App.getMyDrawable("bg_ajuda_half"));
            }
            ViewHelper.setAlpha(view, 1.0F);
        }
        ai = Ajuda.AjudaPublico(pergunta, Utils.getDificuldadeAtual());
        view1 = View.inflate(getActivity(), R.layout.activity_ajuda_publico, null);
        if (((Button) findViewById(R.id.btn_resposta_1)).getVisibility() == 0) {
            ((TextView) view1.findViewById(R.id.txt_percentagemA)).setText((new StringBuilder(String.valueOf(ai[0]))).append("%").toString());
            i = 0 + 1;
        } else {
            view1.findViewById(R.id.ll_respostaA).setVisibility(8);
            i = 0;
        }
        if (((Button) findViewById(R.id.btn_resposta_2)).getVisibility() == 0) {
            ((TextView) view1.findViewById(R.id.txt_percentagemB)).setText((new StringBuilder(String.valueOf(ai[i]))).append("%").toString());
            i++;
        } else {
            view1.findViewById(R.id.ll_respostaB).setVisibility(8);
        }
        if (((Button) findViewById(R.id.btn_resposta_3)).getVisibility() == 0) {
            ((TextView) view1.findViewById(R.id.txt_percentagemC)).setText((new StringBuilder(String.valueOf(ai[i]))).append("%").toString());
            i++;
        } else {
            view1.findViewById(R.id.ll_respostaC).setVisibility(8);
        }
        if (((Button) findViewById(R.id.btn_resposta_4)).getVisibility() == 0) {
            ((TextView) view1.findViewById(R.id.txt_percentagemD)).setText((new StringBuilder(String.valueOf(ai[i]))).append("%").toString());
            int _tmp = i + 1;
        } else {
            view1.findViewById(R.id.ll_respostaD).setVisibility(8);
        }
        dialog = new Dialog(getActivity(), 0x1030010);
        ((Button) view1.findViewById(R.id.btn_ok)).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view2) {
                if (dialog != null) {
                    interacaoAtiva = true;
                    dialog.dismiss();
                }
            }
        });
        dialog.setContentView(view1);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new android.content.DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialoginterface) {
                interacaoAtiva = true;
            }
        });
        dialog.show();
    }

    public void RespostaSelecionada(View view) {
        if (!interacaoAtiva || respostaDada || !((Button) view).isEnabled())
            return;
        final Button temp;
        interacaoAtiva = false;
        temp = (Button) view;
        temp.setEnabled(false);
        if (!App.DB().getValue("isConfirmacaoOn").equals("1")) {
            respostaDada = true;
            PopUp.hide();
            iniciaFlash(temp);
            return;
        }
        String s;
        defaultDrawable = temp.getBackground();
        temp.setBackgroundDrawable(getResources().getDrawable(App.getMyDrawable("bg_resposta_amarelo")));
        s = "";
        if (temp.getId() != R.id.btn_resposta_1) {
            if (temp.getId() == R.id.btn_resposta_2) {
                s = "B: ";
            } else if (temp.getId() == R.id.btn_resposta_3) {
                s = "C: ";
            } else if (temp.getId() == R.id.btn_resposta_4) {
                s = "D: ";
            }
        } else
            s = "A: ";
        PopUp.show(getActivity(), 0, 2);
        PopUp.setMensagem((new StringBuilder(String.valueOf(s))).append(temp.getText()).append("\n").append(getString(R.string.resposta_final)).toString());
        PopUp.setTextoEListenerBotao(getString(R.string.sim), new android.view.View.OnClickListener() {
            public void onClick(View view1) {
                respostaDada = true;
                PopUp.hide();
                temp.setBackgroundDrawable(defaultDrawable);
                iniciaFlash(temp);
            }
        }, 1);
        PopUp.setTextoEListenerBotao(getString(R.string.nao), new android.view.View.OnClickListener() {
            public void onClick(View view1) {
                interacaoAtiva = true;
                PopUp.hide();
                temp.setBackgroundDrawable(defaultDrawable);
                continuaJogo(temp);
            }
        }, 2);
    }

    public void Retira2Btn_click(View view) {
        if (!interacaoAtiva || App.is5050Disponivel >= numAjudasDisponivel)
            return;
        interacaoAtiva = false;
        //Analytics.send("Ajuda - 50-50", "", "");
        if (!(App.is5050Disponivel <= 0 || Utils.getNumeroAjudasExtra() != 0)) {
            interacaoAtiva = true;
            startActivity(5);
            return;
        }
        if (App.is5050Disponivel > 0) {
            Utils.removeAjudaExtra();
        }
        App.is5050Disponivel = 1 + App.is5050Disponivel;
        ArrayList arraylist;
        final ArrayList views;
        Iterator iterator;
        if (App.is5050Disponivel >= numAjudasDisponivel) {
            //view.setBackgroundResource(App.getMyDrawable("bg_ajuda_empty"));
            ViewHelper.setAlpha(view, 0.7F);
            view.setContentDescription((new StringBuilder()).append(view.getContentDescription()).append(" - ").append(getString(R.string.ajuda_ja_utilizada)).toString());
        } else {
            if (!App.isContraRelogio && App.is5050Disponivel > 0) {
                //view.setBackgroundResource(App.getMyDrawable("bg_ajuda_half"));
            }
            ViewHelper.setAlpha(view, 1.0F);
        }
        arraylist = new ArrayList();
        arraylist.add(Integer.valueOf(0));
        arraylist.add(Integer.valueOf(1));
        arraylist.add(Integer.valueOf(2));
        arraylist.add(Integer.valueOf(3));
        if (findViewById(R.id.btn_resposta_4).getVisibility() != 0) {
            arraylist.remove(3);
        }
        if (findViewById(R.id.btn_resposta_3).getVisibility() != 0) {
            arraylist.remove(2);
        }
        if (findViewById(R.id.btn_resposta_2).getVisibility() != 0) {
            arraylist.remove(1);
        }
        if (findViewById(R.id.btn_resposta_1).getVisibility() != 0) {
            arraylist.remove(0);
        }
        arraylist.remove(pergunta.getIndexRespostaCerta());
        if (arraylist.size() > 1) {
            arraylist.remove((new Random(System.currentTimeMillis())).nextInt(arraylist.size()));
        }
        views = new ArrayList();
        iterator = arraylist.iterator();
        do {
            if (!iterator.hasNext()) {
                Iterator iterator1 = views.iterator();
                while (iterator1.hasNext()) {
                    ViewPropertyAnimator.animate((View) iterator1.next()).setStartDelay(100L).setDuration(500L).xBy(getActivity().getWindowManager().getDefaultDisplay().getWidth()).setListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
                        public void onAnimationCancel(Animator animator) {
                        }

                        public void onAnimationEnd(Animator animator) {
                            Iterator iterator2 = views.iterator();
                            do {
                                if (!iterator2.hasNext()) {
                                    interacaoAtiva = true;
                                    return;
                                }
                                ((View) iterator2.next()).setVisibility(4);
                            } while (true);
                        }

                        public void onAnimationRepeat(Animator animator) {
                        }

                        public void onAnimationStart(Animator animator) {
                        }
                    }).start();
                }
                return;
            }
            switch (((Integer) iterator.next()).intValue()) {
                case 0: // '\0'
                    views.add(findViewById(R.id.btn_resposta_1));
                    pergunta.ocultaResposta(((TextView) findViewById(R.id.btn_resposta_1)).getText().toString());
                    break;

                case 1: // '\001'
                    views.add(findViewById(R.id.btn_resposta_2));
                    pergunta.ocultaResposta(((TextView) findViewById(R.id.btn_resposta_2)).getText().toString());
                    break;

                case 2: // '\002'
                    views.add(findViewById(R.id.btn_resposta_3));
                    pergunta.ocultaResposta(((TextView) findViewById(R.id.btn_resposta_3)).getText().toString());
                    break;

                case 3: // '\003'
                    views.add(findViewById(R.id.btn_resposta_4));
                    pergunta.ocultaResposta(((TextView) findViewById(R.id.btn_resposta_4)).getText().toString());
                    break;
            }
        } while (true);
    }

    public void SairBtn_click(View view) {
        SairCustomBtn_click(view);
    }

    public void SairCustomBtn_click(View view) {
        if (interacaoAtiva) {
            interacaoAtiva = false;
            PopUp.show(getActivity(), 0, 2);
            int i = R.string.deseja_sair;
            Object aobj[] = new Object[1];
            int j = R.string.money_format;
            Object aobj1[] = new Object[1];
            aobj1[0] = App.DinheiroAtual;
            aobj[0] = getString(j, aobj1);
            PopUp.setMensagem(getString(i, aobj));
            PopUp.setTextoEListenerBotao(getString(R.string.sim), new android.view.View.OnClickListener() {
                public void onClick(View view1) {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            respostaDada = true;
                            if (App.isContraRelogio) {
                                App.Musica.paraContraRelogio();
                                App.Musica.start();
                            }
                            try {
                                timer.cancel();
                                timer.purge();
                            } catch (Exception exception) {
                            }
                            shouldPlayMusicaContraRelogio = false;
                            shouldPlayMusicaNormal = true;
                            App.DB().updatePergunta(pergunta.ID, false);
                            PopUp.hide();
                            mostraRespostaCorreta(new Button(getActivity()), true);
                            GuardaDinheiro();
                        }
                    });
                }
            }, 1);
            PopUp.setTextoEListenerBotao(getString(R.string.nao), new android.view.View.OnClickListener() {
                public void onClick(View view1) {
                    interacaoAtiva = true;
                    PopUp.hide();
                }
            }, 2);
        }
    }

    public void TelefoneBtn_click(View view) {
        label0:
        {
            if (interacaoAtiva && App.isTelefoneDisponivel < numAjudasDisponivel) {
                interacaoAtiva = false;
                //Analytics.send("Ajuda - Telefone", "", "");
                if (App.isTelefoneDisponivel <= 0 || Utils.getNumeroAjudasExtra() != 0) {
                    break label0;
                }
                interacaoAtiva = true;
                startActivity(5);
            }
            return;
        }
        if (App.isTelefoneDisponivel > 0) {
            Utils.removeAjudaExtra();
        }
        App.isTelefoneDisponivel = 1 + App.isTelefoneDisponivel;
        ArrayList arraylist;
        String s;
        View view1;
        final Dialog dialog;
        if (App.isTelefoneDisponivel >= numAjudasDisponivel) {
            //view.setBackgroundResource(App.getMyDrawable("bg_ajuda_empty"));
            ViewHelper.setAlpha(view, 0.7F);
            view.setContentDescription((new StringBuilder()).append(view.getContentDescription()).append(" - ").append(getString(R.string.ajuda_ja_utilizada)).toString());
        } else {
            if (!App.isContraRelogio && App.isTelefoneDisponivel > 0) {
                //view.setBackgroundResource(App.getMyDrawable("bg_ajuda_half"));
            }
            ViewHelper.setAlpha(view, 1.0F);
        }
        arraylist = new ArrayList();
        if (((Button) findViewById(R.id.btn_resposta_1)).getVisibility() == 0) {
            arraylist.add("A");
        }
        if (((Button) findViewById(R.id.btn_resposta_2)).getVisibility() == 0) {
            arraylist.add("B");
        }
        if (((Button) findViewById(R.id.btn_resposta_3)).getVisibility() == 0) {
            arraylist.add("C");
        }
        if (((Button) findViewById(R.id.btn_resposta_4)).getVisibility() == 0) {
            arraylist.add("D");
        }
        s = Ajuda.AjudaTelefone(pergunta, Utils.getDificuldadeAtual(), (String[]) arraylist.toArray(new String[arraylist.size()]));
        view1 = View.inflate(getActivity(), R.layout.activity_ajuda_telefone, null);
        ((TextView) view1.findViewById(R.id.txt_respostaTelefone)).setText(s);
        dialog = new Dialog(getActivity(), 0x1030010);
        ((Button) view1.findViewById(R.id.btn_ok)).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view2) {
                if (dialog != null) {
                    interacaoAtiva = true;
                    dialog.dismiss();
                }
            }
        });
        dialog.setContentView(view1);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new android.content.DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialoginterface) {
                interacaoAtiva = true;
            }
        });
        dialog.show();
    }

    public void TrocaBtn_click(View view) {
        if (!interacaoAtiva || App.isTrocaDisponivel >= numAjudasDisponivel)
            return;
        interacaoAtiva = false;
        //Analytics.send("Ajuda - Troca", "", "");
        if (App.isTrocaDisponivel <= 0 || Utils.getNumeroAjudasExtra() != 0) {
            if (App.isTrocaDisponivel > 0) {
                Utils.removeAjudaExtra();
            }
            App.isTrocaDisponivel = 1 + App.isTrocaDisponivel;
            if (App.isTrocaDisponivel < numAjudasDisponivel) {
                if (!App.isContraRelogio && App.isTrocaDisponivel > 0) {
                    // view.setBackgroundResource(App.getMyDrawable("bg_ajuda_half"));
                }
                ViewHelper.setAlpha(view, 1.0F);
            } else {
                //view.setBackgroundResource(App.getMyDrawable("bg_ajuda_empty"));
                ViewHelper.setAlpha(view, 0.7F);
                view.setContentDescription((new StringBuilder()).append(view.getContentDescription()).append(" - ").append(getString(R.string.ajuda_ja_utilizada)).toString());
            }
            App.DB().updatePergunta(pergunta.ID, false);
            ArrayList arraylist = (ArrayList) App.Perguntas.clone();
            Pergunta pergunta1 = App.DB().getPergunta(((Integer) App.PerguntasTroca.get(Utils.getDificuldadeAtual())).intValue());
            final ArrayList views;
            final float xPergunta;
            float f;
            final float xResposta;
            Iterator iterator;
            if (pergunta.Pergunta.equals(pergunta1.Pergunta) || pergunta1.saiu) {
                arraylist.set(App.PerguntaAtual, (Integer) App.PerguntasTrocaExtra.get(Utils.getDificuldadeAtual()));
                pergunta1 = App.DB().getPergunta(((Integer) App.PerguntasTrocaExtra.get(Utils.getDificuldadeAtual())).intValue());
            } else {
                arraylist.set(App.PerguntaAtual, (Integer) App.PerguntasTroca.get(Utils.getDificuldadeAtual()));
            }
            pergunta1.saiu = true;
            App.Perguntas = (ArrayList) arraylist.clone();
            views = new ArrayList();
            views.add(findViewById(R.id.txt_pergunta));
            views.add(findViewById(R.id.btn_resposta_1));
            views.add(findViewById(R.id.btn_resposta_2));
            views.add(findViewById(R.id.btn_resposta_3));
            views.add(findViewById(R.id.btn_resposta_4));
            xPergunta = ViewHelper.getX(findViewById(R.id.txt_pergunta));
            f = ViewHelper.getX(findViewById(R.id.btn_resposta_1));
            if (f > (float) (-10 + getActivity().getWindowManager().getDefaultDisplay().getWidth())) {
                f = ViewHelper.getX(findViewById(R.id.btn_resposta_2));
            }
            if (f > (float) (-10 + getActivity().getWindowManager().getDefaultDisplay().getWidth())) {
                f = ViewHelper.getX(findViewById(R.id.btn_resposta_3));
            }
            if (f > (float) (-10 + getActivity().getWindowManager().getDefaultDisplay().getWidth())) {
                f = ViewHelper.getX(findViewById(R.id.btn_resposta_4));
            }
            xResposta = f;
            trocaAjudaCount = 0;
            iterator = views.iterator();
            while (iterator.hasNext()) {
                ViewPropertyAnimator.animate((View) iterator.next()).setStartDelay(0L).setDuration(250L).x(10 + getActivity().getWindowManager().getDefaultDisplay().getWidth()).setListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
                    public void onAnimationCancel(Animator animator) {
                    }

                    public void onAnimationEnd(Animator animator) {
                        PerguntaFragment perguntafragment = PerguntaFragment.this;
                        perguntafragment.trocaAjudaCount = 1 + perguntafragment.trocaAjudaCount;
                        if (trocaAjudaCount == 4) {
                            loadPergunta();
                            Iterator iterator1 = views.iterator();
                            while (iterator1.hasNext()) {
                                View view1 = (View) iterator1.next();
                                ViewHelper.setX(view1, -10 + (0 - view1.getWidth()));
                                ViewPropertyAnimator viewpropertyanimator = ViewPropertyAnimator.animate(view1).setStartDelay(0L).setDuration(250L);
                                float f1;
                                if (view1.getId() == R.id.txt_pergunta) {
                                    f1 = xPergunta;
                                } else {
                                    f1 = xResposta;
                                }
                                viewpropertyanimator.x(f1).setListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
                                    public void onAnimationCancel(Animator animator) {
                                    }

                                    public void onAnimationEnd(Animator animator) {
                                        interacaoAtiva = true;
                                    }

                                    public void onAnimationRepeat(Animator animator) {
                                    }

                                    public void onAnimationStart(Animator animator) {
                                    }
                                }).start();
                            }
                        }
                    }

                    public void onAnimationRepeat(Animator animator) {
                    }

                    public void onAnimationStart(Animator animator) {
                    }
                }).start();
            }
        } else {
            interacaoAtiva = true;
            startActivity(5);
        }
    }

    public void continuaJogo(Button button) {
        button.setEnabled(true);
    }

    public void flashOff(final Button tempBtn) {
        if (countAnimacao < 4) {
            switch (tempBtn.getId()){
                case R.id.btn_resposta_1:{
                    tempBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.resposta_a_02));
                    break;
                }
                case R.id.btn_resposta_2:{
                    tempBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.resposta_b_02));
                    break;
                }
                case R.id.btn_resposta_3:{
                    tempBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.resposta_c_02));
                    break;
                }
                case R.id.btn_resposta_4:{
                    tempBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.resposta_d_02));
                    break;
                }
            }
           // tempBtn.setBackgroundDrawable(defaultDrawable);
            countAnimacao = 1 + countAnimacao;
            Runnable runnable = new Runnable() {
                public void run() {
                    flashOn(tempBtn);
                }
            };
            (new Handler()).postDelayed(runnable, 500L);
        } else {
            mostraRespostaCorreta(tempBtn);
        }
    }

    public void flashOn(final Button tempBtn) {
        if (countAnimacao < 4) {
            App.Musica.tocaSom(4);
            defaultDrawable = tempBtn.getBackground();
            switch (tempBtn.getId()){
                case R.id.btn_resposta_1:{
                    tempBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.flash_a_02));
                    break;
                }
                case R.id.btn_resposta_2:{
                    tempBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.flash_b_02));
                    break;
                }
                case R.id.btn_resposta_3:{
                    tempBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.flash_c_02));
                    break;
                }
                case R.id.btn_resposta_4:{
                    tempBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.flash_d_02));
                    break;
                }
            }

           //tempBtn.setBackgroundDrawable(getResources().getDrawable(App.getMyDrawable("bg_resposta_amarelo")));
            countAnimacao = 1 + countAnimacao;
            Runnable runnable = new Runnable() {
                public void run() {
                    flashOff(tempBtn);
                }
            };
            (new Handler()).postDelayed(runnable, 500L);
            return;
        } else {
            mostraRespostaCorreta(tempBtn);
            return;
        }
    }

    public String getNameTag() {
        return (new StringBuilder("PerguntaFragment-")).append(perguntaIndex).toString();
    }

    public void iniciaFlash(Button button) {
        if (timer != null) {
            try {
                timer.cancel();
                timer.purge();
            } catch (Exception exception) {
            }
        }
        shouldPlayMusicaContraRelogio = false;
        shouldPlayMusicaNormal = false;
        countAnimacao = 0;
        if (App.DB().getValue("isSonsJogoOn").equals("1")) {
            if (App.isContraRelogio) {
                App.Musica.paraContraRelogio();
            } else {
                App.Musica.realPause();
            }
        }
        flashOn(button);
    }

    public void mostraRespostaCorreta(Button button) {
        mostraRespostaCorreta(button, false);
    }

    public void mostraRespostaCorreta(Button button, boolean flag) {
        int i;
        int j;
        String s;
        Runnable runnable1;
        Handler handler;
        respostaDada = true;
        shouldPlayMusicaContraRelogio = false;
        shouldPlayMusicaNormal = false;
        if (pergunta.isRespostaCorreta(button.getText().toString())) {
            App.Musica.tocaSom(2, true);
            switch (button.getId()){
                case R.id.btn_resposta_1:{
                    button.setBackgroundDrawable(getResources().getDrawable(R.drawable.correcta_a_02));
                    break;
                }
                case R.id.btn_resposta_2:{
                    button.setBackgroundDrawable(getResources().getDrawable(R.drawable.correcta_b_02));
                    break;
                }
                case R.id.btn_resposta_3:{
                    button.setBackgroundDrawable(getResources().getDrawable(R.drawable.correcta_c_02));
                    break;
                }
                case R.id.btn_resposta_4:{
                    button.setBackgroundDrawable(getResources().getDrawable(R.drawable.correcta_d_02));
                    break;
                }
            }

            App.DB().updatePergunta(pergunta.ID, true);
            Runnable runnable2 = new Runnable() {
                public void run() {
                    mudaEcra("Certo");
                }
            };
            Handler handler1 = new Handler();
            if (Utils.isVoiceOverActive()) {
                handler1.postDelayed(new Runnable() {
                    public void run() {
                        if (Utils.isVoiceOverActive()) {
                            ((Vibrator) getActivity().getSystemService("vibrator")).vibrate(500L);
                        }
                    }
                }, 500L);
            }
            handler1.postDelayed(runnable2, 2000L);
            return;
        }
        if (!flag) {
            App.Musica.tocaSom(1, true);
        }
        switch (button.getId()){
            case R.id.btn_resposta_1:{
                button.setBackgroundDrawable(getResources().getDrawable(R.drawable.incorrecta_c_02));
                break;
            }
            case R.id.btn_resposta_2:{
                button.setBackgroundDrawable(getResources().getDrawable(R.drawable.incorrecta_b_02));
                break;
            }
            case R.id.btn_resposta_3:{
                button.setBackgroundDrawable(getResources().getDrawable(R.drawable.incoreccta_c_02));
                break;
            }
            case R.id.btn_resposta_4:{
                button.setBackgroundDrawable(getResources().getDrawable(R.drawable.incorrecta_d_02));
                break;
            }
        }
        //button.setBackgroundDrawable(getResources().getDrawable(App.getMyDrawable("bg_resposta_vermelho")));
        if (!pergunta.isRespostaCorreta(((Button) findViewById(R.id.btn_resposta_1)).getText().toString())) {
            if (pergunta.isRespostaCorreta(((Button) findViewById(R.id.btn_resposta_2)).getText().toString())) {
                ((Button) findViewById(R.id.btn_resposta_2)).setBackgroundDrawable(getResources().getDrawable(R.drawable.correcta_b_02));
            } else if (pergunta.isRespostaCorreta(((Button) findViewById(R.id.btn_resposta_3)).getText().toString())) {
                ((Button) findViewById(R.id.btn_resposta_3)).setBackgroundDrawable(getResources().getDrawable(R.drawable.correcta_c_02));
            } else
                try {
                    if (pergunta.isRespostaCorreta(((Button) findViewById(R.id.btn_resposta_4)).getText().toString())) {
                        ((Button) findViewById(R.id.btn_resposta_4)).setBackgroundDrawable(getResources().getDrawable(R.drawable.correcta_d_02));
                    }
                } catch (Exception exception) {
                }
        } else
            ((Button) findViewById(R.id.btn_resposta_1)).setBackgroundDrawable(getResources().getDrawable(R.drawable.correcta_a_02));
        if (flag) {
            Runnable runnable = new Runnable() {
                public void run() {
                    mudaEcra("Sair");
                }
            };
            (new Handler()).postDelayed(runnable, 2000L);
            return;
        }
        App.DB().updatePergunta(pergunta.ID, false);
        if (App.PerguntaAtual <= 4 || App.PerguntaAtual >= 10) {
            i = App.PerguntaAtual;
            j = 0;
            if (i > 9) {
                j = 32000;
            }
        } else
            j = 1000;
        App.DinheiroAtual = (new StringBuilder()).append(j).toString();
        App.DB().updateDinheiro(Float.parseFloat(App.DinheiroAtual));
        s = App.DB().getValue("Dinheiro");
        if (s == null || s.equals("") || s.equals("<null>")) {
            s = "0";
        }
        Conquistas.verificaConquista(Float.parseFloat(s), getActivity());
        runnable1 = new Runnable() {
            public void run() {
                mudaEcra("Errado");
            }
        };
        handler = new Handler();
        if (Utils.isVoiceOverActive()) {
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (Utils.isVoiceOverActive()) {
                        ((Vibrator) getActivity().getSystemService("vibrator")).vibrate(1000L);
                    }
                }
            }, 500L);
        }
        handler.postDelayed(runnable1, 2000L);
    }

    public void mudaEcra(String s) {
        shouldPlayMusicaContraRelogio = false;
        shouldPlayMusicaNormal = true;
        if (s.equals("Certo")) {
            if (App.PerguntaAtual == 14) {
                App.DB().updateDinheiro(1000000F);
                String s1 = App.DB().getValue("Dinheiro");
                if (s1 == null || s1.equals("") || s1.equals("<null>")) {
                    s1 = "0";
                }
                Conquistas.verificaConquista(Float.parseFloat(s1), getActivity());
                startActivity(4);
                return;
            }
            if (App.isContraRelogio) {
                App.DinheiroAtual = getDinheiro(App.PerguntaAtual);
                App.PerguntaAtual = 1 + App.PerguntaAtual;
                startActivity(1);
                return;
            } else {
                App.DinheiroAtual = getDinheiro(App.PerguntaAtual);
               // App.PerguntaAtual = 1 + App.PerguntaAtual;
                startActivity(2);
                return;
            }
        }
        if (s.equals("Sair")) {
            getActivity().finish();
            getActivity().sendBroadcast(new Intent("killGlobal"));
            return;
        }
        if (Utils.isVoiceOverActive()) {
            PopUp.show(getActivity(), 0, 1);
            PopUp.setMensagem((new StringBuilder(String.valueOf(getString(R.string.resposta_correta)))).append(" ").append(pergunta.RespostaCerta).toString());
            PopUp.setTextoEListenerBotao(getString(0x104000a), new android.view.View.OnClickListener() {
                public void onClick(View view) {
                    PopUp.hide();
                    startActivity(3);
                }
            }, 1);
            return;
        } else {
            startActivity(3);
            return;
        }
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        initViews();
    }

    public void onBackPressed() {
        if (interacaoAtiva) {
            SairCustomBtn_click(null);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {

        Typeface tf = Typeface.createFromAsset(this.getResources().getAssets(), "norwester.otf");
        View root = LayoutInflater.from(getActivity()).inflate(R.layout.activity_pergunta, viewgroup, false);
        ((TextView) root.findViewById(R.id.txt_pergunta)).setTypeface(tf);
        ((Button) root.findViewById(R.id.btn_resposta_1)).setTypeface(tf);
        ((Button) root.findViewById(R.id.btn_resposta_2)).setTypeface(tf);
        ((Button) root.findViewById(R.id.btn_resposta_3)).setTypeface(tf);
        ((Button) root.findViewById(R.id.btn_resposta_4)).setTypeface(tf);
//        mAdView = (AdView) root.findViewById(R.id.view);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                if (mAdView.getVisibility() != View.VISIBLE) {
//                    mAdView.setVisibility(View.VISIBLE);
//                }
//            }
//        });
//        // Start loading the ad in the background.
//        mAdView.loadAd(adRequest);
        return root;
    }

    public void onDestroy() {
        if (timer != null) {
            try {
                timer.cancel();
                timer.purge();
            } catch (Exception exception) {
            }
        }
        super.onDestroy();
    }

    public void onPause() {
        super.onPause();
        //mAdView.pause();
        try {
            PowerManager powermanager = (PowerManager) getActivity().getSystemService("power");
            if (App.isContraRelogio && (App.isApplicationSentToBackground() || !powermanager.isScreenOn())) {
                App.Musica.paraContraRelogio();
            }
            return;
        } catch (Exception exception) {
            return;
        }
    }

    public void onResume() {
        super.onResume();
        //mAdView.resume();
        try {
            if (App.isContraRelogio) {
                App.Musica.tocaContraRelogio();
            }
            return;
        } catch (Exception exception) {
            return;
        }
    }

    public void onStop() {
        super.onStop();

        try {
            if (App.isContraRelogio && App.isApplicationSentToBackground()) {
                App.Musica.paraContraRelogio();
            }
            return;
        } catch (Exception exception) {
            return;
        }
    }

    public void reportarSucesso(boolean flag) {
        if (flag) {
            motivo = null;
            emailReportar = null;
            return;
        } else {
//            ReportarBtn_click(null);
            return;
        }
    }

    protected void setContraRelogio() {
        pb = (ProgressBar) findViewById(R.id.pb_contraRelogio);
        pb.setIndeterminate(false);
        pb.setMax(tempoTotal);
        pb.setProgress(tempoTotal);
        timer = new Timer();
        timer.schedule(new CountDownTimerTask(), 1000L, 1000L);
        numAjudasDisponivel = 1;
        App.Musica.tocaContraRelogio();
        shouldPlayMusicaContraRelogio = true;
        shouldPlayMusicaNormal = false;
    }

    public void setPerguntaIndex(int i) {
        perguntaIndex = i;
        if (getView() != null) {
            loadPergunta();
        }
    }

    public void startActivity(int i) {
        PopUp.hide();
        PerguntaActivity perguntaactivity;
        if (timer != null) {
            try {
                timer.cancel();
                timer.purge();
            } catch (Exception exception1) {
            }
        }
        try {
            perguntaactivity = (PerguntaActivity) getActivity();
        } catch (Exception exception) {
            return;
        }
        switch (i) {
            default:
                break;
            case 1:
                perguntaactivity.nextQuestion(true);
                break;
            case 2:
                perguntaactivity.showPatamares();
                break;
            case 3:
                perguntaactivity.showGameOver();
                break;
            case 4:
                perguntaactivity.showMilionario();
                break;
            case 5:
                perguntaactivity.showExtraLifeLines();
                break;
        }
    }

    class CountDownTimerTask extends TimerTask {
        CountDownTimerTask() {
            super();
        }

        public void run() {
            if (!respostaDada) {
                int i = -1 + pb.getProgress();
                pb.setProgress(i);
                if (i <= 0) {
                    respostaDada = true;
                    interacaoAtiva = false;
                    shouldPlayMusicaContraRelogio = false;
                    shouldPlayMusicaNormal = false;
                    timer.cancel();
                    timer.purge();
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            mostraRespostaCorreta(new Button(getActivity()));
                        }
                    });
                    return;
                }
            }
        }
    }


}
