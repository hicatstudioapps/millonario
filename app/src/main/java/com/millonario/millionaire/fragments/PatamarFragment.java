package com.millonario.millionaire.fragments;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.millonario.millionaire.PerguntaActivity;
import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;

// Referenced classes of package com.millonario.millionaire.fragments:
//            SuperFragment

public class PatamarFragment extends SuperFragment {
    ViewPropertyAnimator vpa;


    public PatamarFragment() {
    }

    private void animaIsso() {
        int i = App.PerguntaAtual;
        View view = findViewById(R.id.view_indicador_patamar);
        TextView textview = (TextView) findViewById(getResources().getIdentifier((new StringBuilder("txt_patamar_")).append(15 - i).toString(), "id", getPackageName()));
        TextView textview1 = (TextView) findViewById(getResources().getIdentifier((new StringBuilder("txt_patamar_")).append(-1 + (15 - i)).toString(), "id", getPackageName()));
        App.DinheiroAtual = textview.getText().toString().replace(getString(R.string.money_symbol), "");
        App.PerguntaAtual = 1 + App.PerguntaAtual;
        int ai[] = new int[2];
        textview1.getLocationOnScreen(ai);
        int ai1[] = new int[2];
        textview.getLocationOnScreen(ai1);
        int j = (int) TypedValue.applyDimension(1, 4F, getResources().getDisplayMetrics());
        vpa = ViewPropertyAnimator.animate(view).setStartDelay(500L).setDuration(500L).yBy(ai[1] - ai1[1] - j).setListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
                vpa = null;
                startActivity2();
            }

            public void onAnimationEnd(Animator animator) {
                if (vpa == null) {
                    return;
                } else {
                    Runnable runnable = new Runnable() {
                        public void run() {
                            if (vpa == null) {
                                return;
                            } else {
                                startActivity2();
                                return;
                            }
                        }
                    };
                    (new Handler()).postDelayed(runnable, 500L);
                    return;
                }
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
            }
        });
        vpa.start();
    }

    private void initViews() {
        int i = App.PerguntaAtual;
        View view = findViewById(R.id.view_indicador_patamar);
        android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutparams.addRule(6, getResources().getIdentifier((new StringBuilder("txt_patamar_")).append(15 - i).toString(), "id", getPackageName()));
        view.setLayoutParams(layoutparams);
        meteAlphas(i);
        WaitForIt waitforit = new WaitForIt(null);
        TextView atextview[] = new TextView[1];
        atextview[0] = (TextView) findViewById(getResources().getIdentifier((new StringBuilder("txt_patamar_")).append(15 - i).toString(), "id", getPackageName()));
        waitforit.execute(atextview);
    }

    private void meteAlphas(int i) {
        int j = 15;
        do {
            if (j <= 14 - i) {
                return;
            }
            ViewHelper.setAlpha((TextView) findViewById(getResources().getIdentifier((new StringBuilder("txt_patamar_")).append(j).toString(), "id", getPackageName())), 0.5F);
            j--;
        } while (true);
    }

    public String getNameTag() {
        return "PatamarFragment";
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        initViews();
    }

    public void onBackPressed() {
        if (vpa != null) {
            vpa.cancel();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {
        Typeface tf = Typeface.createFromAsset(this.getResources().getAssets(), "norwester.otf");
        View root = LayoutInflater.from(getActivity()).inflate(R.layout.activity_patamar, viewgroup, false);
        ((TextView) root.findViewById(R.id.txt_patamar_1)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_2)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_3)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_4)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_5)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_6)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_7)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_8)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_9)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_10)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_11)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_12)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_13)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_14)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_patamar_15)).setTypeface(tf);

        return root;
    }

    public void startActivity2() {
        try {
            ((PerguntaActivity) getActivity()).nextQuestion(false);
            return;
        } catch (Exception exception) {
            return;
        }
    }

    private class WaitForIt extends AsyncTask {
        private WaitForIt() {
            super();
        }

        WaitForIt(WaitForIt waitforit) {
            this();
        }

        protected Object doInBackground(Object aobj[]) {
            return doInBackground((TextView[]) aobj);
        }

        protected String doInBackground(TextView atextview[]) {
            int ai[] = new int[2];
            TextView textview = atextview[0];
            textview.getLocationOnScreen(ai);
            do {
                if (ai[1] != 0) {
                    return null;
                }
                textview.getLocationOnScreen(ai);
            } while (true);
        }

        protected void onPostExecute(Object obj) {
            onPostExecute((String) obj);
        }

        protected void onPostExecute(String s) {
            animaIsso();
        }
    }
}
