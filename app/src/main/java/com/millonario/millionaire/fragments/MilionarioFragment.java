package com.millonario.millionaire.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.millonario.millionaire.PerguntaActivity;
import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.popup.PopUp;
import com.millonario.millionaire.utils.TestConnection;
import com.millonario.millionaire.utils.Utils;
import com.nineoldandroids.view.ViewHelper;

// Referenced classes of package com.millonario.millionaire.fragments:
//            SuperFragment

public class MilionarioFragment extends SuperFragment {

    public MilionarioFragment() {
    }

    private void initViews() {
        App.Musica.tocaSom(3);
        if (App.isContraRelogio) {
            ((TextView) findViewById(R.id.txt_tempoTotal)).setText(Utils.paraTimer());
            ViewHelper.setAlpha(findViewById(R.id.view_tempo), 0.8F);
        } else {
            findViewById(R.id.rl_tempoTotal).setVisibility(8);
        }
        ((Button) findViewById(R.id.btn_jogar)).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view) {
                JogarBtn_click(view);
            }
        });
        ((Button) findViewById(R.id.btn_sair)).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view) {
                SairBtn_click(view);
            }
        });
        ((Button) findViewById(R.id.btn_partilharMilionario)).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view) {
                PartilharCustomBtn_click(view);
            }
        });

    }

    public void JogarBtn_click(View view) {
        getActivity().sendBroadcast(new Intent("killGlobal"));
        ((PerguntaActivity) getActivity()).NovoJogoBtn_click(true);
    }

    public void PartilharCustomBtn_click(View view) {
        if (!TestConnection.test()) {
            PopUp.show(getActivity(), 0, 1);
            PopUp.setMensagem(getString(R.string.necessario_conexao));
            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
                public void onClick(View view1) {
                    PopUp.hide();
                }
            }, 1);
            return;
        } else {
            //Analytics.send("Partilhar", "Ecr\343 final", null);
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.TEXT", (new StringBuilder(String.valueOf(getString(R.string.frase_partilhar_milionario)))).append(" ").append(getString(R.string.link_google_play)).toString());
            startActivity(Intent.createChooser(intent, getString(R.string.partilha_pergunta)));
            return;
        }
    }

    public void SairBtn_click(View view) {
        App.PerguntaAtual = 0;
        App.Perguntas = null;
        App.PerguntasTroca = null;
        App.DinheiroAtual = "0";
        App.is5050Disponivel = 0;
        App.isPublicoDisponivel = 0;
        App.isTelefoneDisponivel = 0;
        App.isTrocaDisponivel = 0;
        getActivity().sendBroadcast(new Intent("killGlobal"));
        getActivity().finish();
    }

    public String getNameTag() {
        return "MilionarioFragment";
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        initViews();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.activity_milionario, viewgroup, false);
    }
}
