package com.millonario.millionaire.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dolphinwang.imagecoverflow.CoverFlowAdapter;
import com.dolphinwang.imagecoverflow.CoverFlowView;
import com.google.android.gms.games.Games;
import com.millonario.millionaire.AjudasExtraActivity;
import com.millonario.millionaire.PerguntaActivity;
import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.interfaces.VideoRewardInterface;
import com.millonario.millionaire.objetos.CrossApp;
import com.millonario.millionaire.popup.PopUp;
//import com.millonario.millionaire.task.ReportarPergunta;
import com.millonario.millionaire.utils.Ads;
import com.millonario.millionaire.utils.TestConnection;
import com.millonario.millionaire.utils.Utils;
//import com.millonario.millionaire.views.FlakeViewFlutuar;
//import com.sromku.simple.fb.SimpleFacebook;
//import com.sromku.simple.fb.entities.Score;
//import com.sromku.simple.fb.listeners.OnPublishListener;

import java.util.ArrayList;

// Referenced classes of package com.millonario.millionaire.fragments:
//            SuperFragment

@SuppressWarnings("ResourceType")
public class GameOverFragment extends SuperFragment
        implements VideoRewardInterface {
    private static final String VIDEO_URLS[][];
    Dialog dialog_crossPromotion;
    String emailReportar;
//    FlakeViewFlutuar flakeView;
    String motivo;
    String valorGanho;

//    private SimpleFacebook mFacebookSDK;

    public GameOverFragment() {
        valorGanho = "0";
        motivo = null;
        emailReportar = null;
    }

    private void VerVideoBtn_click() {
//        if (!TestConnection.test()) {
//            PopUp.show(getActivity(), 0, 1);
//            PopUp.setMensagem(getString(R.string.necessario_conexao));
//            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
//                public void onClick(View view) {
//                    PopUp.hide();
//                }
//            }, 1);
//            return;
//        } else {
//            //Analytics.send("GameOver", "Ver v\355deo", null);
//            //Analytics.send("V\355deo", "Ecr\343 GameOver", null);
//            Ads.showVideoReward(this);
//            return;
//        }
        startActivity(new Intent(getActivity(),AjudasExtraActivity.class));
    }

    private void initViews() {
        //Analytics.send("Game Over", (new StringBuilder("Pergunta: ")).append(App.getPerguntaAtual().Pergunta).toString(), null);
        String s = App.DinheiroAtual;
        if (s == null) {
            s = "0";
        }
        //guardo el mony que gano
        //Long.parseLong(s)+Long.parseLong(App.DB().getValue("Dinheiro"))+"";
        /*String total= App.DB().getValue("Dinheiro")+"";
        App.DB().insertValue(total+"","Dinheiro");
        //actualizo mejor resultado
        if(Long.parseLong(s)> Long.parseLong(App.DB().getValue("DinheiroBest")))
        {
            App.DB().insertValue(s+"","DinheiroBest");
        }*/
        String s1 = "";
        int i = 0;
        int j = -1 + s.length();
        do {
            if (j < 0) {
                valorGanho = getString(R.string.money_format, new Object[]{
                        s1
                });
                if (App.isSignedIn()) {
                    Games.Leaderboards.submitScore(App.mGoogleApiClient, getString(R.string.word_attack_leaderboard), Long.parseLong(Utils.converteDinheiro(App.DB().getValue("Dinheiro"))));
                }
                ((TextView) findViewById(R.id.txt_valorGanho)).setText(valorGanho);
                ((TextView) findViewById(R.id.txt_valorGanho)).setContentDescription(valorGanho);
                App.Musica.start();
                if (!Utils.isEmptyDB(App.DB().getValue("idUtilizador")) && TestConnection.test() && App.isContraRelogio) {
                    //aki va lo de subir el score si esta logueado.
                    // (new EnviaDinheiroTask()).execute(new Activity[0]);
                }
                //initCoverFlow();
                findViewById(R.id.btn_jogar).setOnClickListener(new android.view.View.OnClickListener() {
                    public void onClick(View view) {
                        NovoJogoBtn_click(view);
                    }
                });
                findViewById(R.id.btn_partilhar).setOnClickListener(new android.view.View.OnClickListener() {
                    public void onClick(View view) {
                        ShareBtn_click(view);
                    }
                });
                findViewById(R.id.btn_sair).setOnClickListener(new android.view.View.OnClickListener() {
                    public void onClick(View view) {
                        SairBtn_click(view);
                    }
                });
                findViewById(R.id.btn_verVideo).setOnClickListener(new android.view.View.OnClickListener() {
                    public void onClick(View view) {
                        VerVideoBtn_click();
                    }
                });
                (new Handler()).postDelayed(new Runnable() {
                    public void run() {
                        try {
                            if (!App.mostraPublicidade(getActivity())) {
                                // showOfflineAd();
                            }
                            return;
                        } catch (Exception exception) {
                            return;
                        }
                    }
                }, 1000L);
                return;
            }
            s1 = (new StringBuilder(String.valueOf(s.charAt(j)))).append(s1).toString();
            if (++i == 3) {
                i = 0;
                s1 = (new StringBuilder(" ")).append(s1).toString();
            }
            j--;
        } while (true);
    }

    private boolean isEmailValido(String s) {
        boolean flag = true;
        if (Utils.isEmpty(s) || !Patterns.EMAIL_ADDRESS.matcher(s).matches()) {
            flag = false;
            PopUp.show(getActivity(), 0, 1);
            PopUp.setMensagem(getString(R.string.erro_email_incorreto));
            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
                public void onClick(View view) {
                    PopUp.hide();
                    reportarSucesso(false);
                }
            }, 1);
        }
        return flag;
    }



    public void NovoJogoBtn_click(View view) {
        getActivity().sendBroadcast(new Intent("killGlobal"));
        ((PerguntaActivity) getActivity()).NovoJogoBtn_click(true);
    }



    public void SairBtn_click(View view) {
        App.PerguntaAtual = 0;
        App.Perguntas = null;
        App.PerguntasTroca = null;
        App.DinheiroAtual = "0";
        App.is5050Disponivel = 0;
        App.isPublicoDisponivel = 0;
        App.isTelefoneDisponivel = 0;
        App.isTrocaDisponivel = 0;
        getActivity().sendBroadcast(new Intent("killGlobal"));
        getActivity().finish();
    }

    public void ShareBtn_click(View view) {

        {
            //Analytics.send("Partilhar", "Ecr\343 GameOver", null);
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            int i = R.string.frase_partilhar_gameover;
            Object aobj[] = new Object[1];
            aobj[0] = valorGanho;
            String x=String.format(getString(R.string.frase_partilhar_gameover),valorGanho,getString(R.string.frase_partilhar_gameover));
            intent.putExtra("android.intent.extra.TEXT", String.format(getString(R.string.frase_partilhar_gameover),valorGanho,getString(R.string.link_google_play)));
            startActivity(Intent.createChooser(intent, getString(R.string.partilha_pergunta)));
            return;
        }
    }

    public String getNameTag() {
        return "GameOverFragment";
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        initViews();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);


    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {
        Typeface tf = Typeface.createFromAsset(this.getResources().getAssets(), "norwester.otf");
        View root = LayoutInflater.from(getActivity()).inflate(R.layout.activity_game_over, viewgroup, false);
        ((TextView) root.findViewById(R.id.txt_gameover)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_fraseTerminou)).setTypeface(tf);
        ((TextView) root.findViewById(R.id.txt_valorGanho)).setTypeface(tf);
        return root;
    }

    public void onInternalVideoPlay() {
        App.Musica.start();
        String s = App.DB().getValue("AjudaExtraVerVideoTempo");
        if (Utils.isEmptyDB(s)) {
            s = "0";
        }
        if (System.currentTimeMillis() >= 0x5265c00L + Long.parseLong(s)) {
            App.DB().insertValue((new StringBuilder()).append(System.currentTimeMillis()).toString(), "AjudaExtraVerVideoTempo");
            App.DB().insertValue("nao", "ViuVideoAteFim");
            String s1 = App.DB().getValue("KEY_VIDEO_CURRENT_INDEX");
            if (Utils.isEmptyDB(s1)) {
                s1 = "0";
            }
            int i = 1 + Integer.parseInt(s1);
            if (i >= VIDEO_URLS.length || i < 0) {
                i = 0;
            }
            App.DB().insertValue((new StringBuilder()).append(i).toString(), "KEY_VIDEO_CURRENT_INDEX");
            String as[] = VIDEO_URLS[i];
            Intent intent = new Intent(null, Uri.parse((new StringBuilder("ytv://")).append(as[2]).toString()), getActivity(), com.millonario.millionaire.utils.youtube2.OpenYouTubePlayerActivity.class);
            intent.putExtra("KEY_VIDEO_NAME", getString(Integer.parseInt(as[0])));
            intent.putExtra("KEY_VIDEO_LINK", getString(Integer.parseInt(as[1])));
            startActivity(intent);
            return;
        } else {
            PopUp.show(getActivity(), 0, 1);
            PopUp.setMensagem(App.getMyString(R.string.video_nao_disponivel));
            PopUp.setTextoEListenerBotao(App.getMyString(R.string.ok), new android.view.View.OnClickListener() {
                public void onClick(View view) {
                    PopUp.hide();
                }
            }, 1);
            return;
        }
    }

    public void onPause() {
        super.onPause();
//        if (flakeView != null) {
//            flakeView.pause();
//        }
    }

    public void onResume() {
        super.onResume();
//        if (flakeView != null) {
//            flakeView.resume();
//        }
        if (App.DB().getValue("ViuVideoAteFim").equals("sim")) {
            App.DB().insertValue("nao", "ViuVideoAteFim");
            Utils.adicionaAjudaExtra(1);
        }
    }

    public void onRewardVideoFinished(boolean flag, String s) {
        if (flag) {
            //Analytics.send("V\355deo", s, null);
            Utils.adicionaAjudaExtra(1);
        }
        App.Musica.start();
    }

    public void onWillDisplayRewardVideo() {
        App.Musica.realPause();
    }

    public void reportarSucesso(boolean flag) {
        if (flag) {
            motivo = null;
            return;
        } else {

            return;
        }
    }
    static {
        String as[][] = new String[3][];
        String as1[] = new String[3];
        as1[0] = (new StringBuilder()).append(R.string.trivial_app_name).toString();
        as1[1] = (new StringBuilder()).append(R.string.trivial_link_app_store).toString();
        as1[2] = "V5vJW6L2XgI";
        as[0] = as1;
        String as2[] = new String[3];
        as2[0] = (new StringBuilder()).append(R.string.wozzle_app_name).toString();
        as2[1] = (new StringBuilder()).append(R.string.wozzle_link_app_store).toString();
        as2[2] = "l4s0XR3gQJc";
        as[1] = as2;
        String as3[] = new String[3];
        as3[0] = (new StringBuilder()).append(R.string.swapper_app_name).toString();
        as3[1] = (new StringBuilder()).append(R.string.swapper_link_app_store).toString();
        as3[2] = "xe_S2l3t470";
        as[2] = as3;
        VIDEO_URLS = as;
    }

    private class MyCoverFlowAdapter extends CoverFlowAdapter {
        public ArrayList apps;

        private MyCoverFlowAdapter() {
            super();
        }

        MyCoverFlowAdapter(MyCoverFlowAdapter mycoverflowadapter) {
            this();
        }

        public int getCount() {
            return apps.size();
        }

        public Bitmap getImage(int i) {
            android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
            options.inPreferredConfig = android.graphics.Bitmap.Config.ARGB_8888;
            return BitmapFactory.decodeFile(((CrossApp) apps.get(i)).img, options);
        }
    }
}
