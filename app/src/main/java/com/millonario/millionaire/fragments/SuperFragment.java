package com.millonario.millionaire.fragments;

import android.support.v4.app.Fragment;
import android.view.View;

public class SuperFragment extends Fragment {

    public SuperFragment() {
    }

    public View findViewById(int i) {
        if (getView() == null) {
            return null;
        } else {
            return getView().findViewById(i);
        }
    }

    public String getNameTag() {
        return "";
    }

    public String getPackageName() {
        return getActivity().getPackageName();
    }

    public void onBackPressed() {
    }

    public void reportarSucesso(boolean flag) {
    }
}
