package com.millonario.millionaire;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ListView;

import com.millonario.millionaire.classes.SuperActivity;

import java.util.ArrayList;

import badabing.lib.Utils;
import badabing.lib.adapter.MoreAppsAdapter;
import badabing.lib.model.MoreAppsModel;

public class OutrasAppsActivity extends SuperActivity {
    private MoreAppsAdapter adapter;
    private ListView listView;
    private View progressBar;
    private View tvEmpty;
    private Handler handler;

    public OutrasAppsActivity() {
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_outras_apps);
        handler = new Handler();
        View.OnClickListener finishListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        };

        listView = (ListView) findViewById(R.id.listView);
        progressBar = findViewById(R.id.progressBar);
        tvEmpty = findViewById(R.id.textView);
        getMoreApps();
    }

    private void getMoreApps() {

        if (adapter == null) {
            progressBar.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
            listView.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }

        Utils.requestMoreApps(this, new Utils.MoreAppsListener() {

            @Override
            public void onRefresh(ArrayList<MoreAppsModel> list) {

            }

            @Override
            public void onMoreApps(boolean isFromCache,
                                   ArrayList<MoreAppsModel> list) {
                progressBar.setVisibility(View.GONE);
                tvEmpty.setVisibility(View.GONE);
                adapter = new MoreAppsAdapter(OutrasAppsActivity.this, list,
                        R.drawable.ic_launcher);
                listView.setAdapter(adapter);
                listView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {
                if (adapter == null) {

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            tvEmpty.setVisibility(View.VISIBLE);
                        }
                    });

                }
            }
        });
    }
}
