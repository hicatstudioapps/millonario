package com.millonario.millionaire.classes;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

//import com.facebook.FacebookSdk;
//import com.facebook.SessionDefaultAudience;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.millonario.millionaire.AjudasExtraActivity;
import com.millonario.millionaire.R;
import com.millonario.millionaire.billing.IabHelper;
import com.millonario.millionaire.objetos.Pergunta;
import com.millonario.millionaire.popup.PopUp;
import com.millonario.millionaire.utils.DBAdapter;
import com.millonario.millionaire.utils.MusicaFundo;
import com.millonario.millionaire.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
//import com.sromku.simple.fb.Permission;
//import com.sromku.simple.fb.SimpleFacebook;
//import com.sromku.simple.fb.SimpleFacebookConfiguration;

import java.util.ArrayList;
import java.util.List;

import badabing.lib.handler.ExceptionHandler;

public class App extends Application implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener

{

    public static final boolean isDebug = true;
    private static final int IN_APP_BILLING_CODE = 0x27009;
    public static String DinheiroAtual;
    public static ArrayList IN_APP_PRODUCTS = null;
    public static List IN_APP_SKUS = new ArrayList();
    public static MusicaFundo Musica;
    public static int PerguntaAtual = 0;
    public static ArrayList Perguntas = null;
    public static ArrayList PerguntasTroca = null;
    public static ArrayList PerguntasTrocaExtra = null;
    public static boolean hasFocus = false;
    public static int is5050Disponivel = 0;
    public static boolean isContraRelogio = false;
    public static boolean isPopUpShowing = false;
    public static int isPublicoDisponivel = 0;
    public static int isTelefoneDisponivel = 0;
    public static int isTrocaDisponivel = 0;
    public static boolean mostrouPublicidade = false;
    public static int perguntaPublicidade = -1;
    public static boolean primeiraVez = true;
    public static boolean veioDoRegistoComSucesso = false;
    // Client used to interact with Google APIs
    public static GoogleApiClient mGoogleApiClient;
    protected static DBAdapter db;
    protected static App instance;
    protected static InterstitialAd interstitial;
    protected static int versao = -1;
    private static String GP_B64 = "";
    private static IabHelper mHelper;
    private Context ctx;

    private static final String APP_ID = "1630339273844588";
    private static final String APP_NAMESPACE = "morewebapp";




    public App() {
    }

    public static boolean isSignedIn() {
        return (mGoogleApiClient != null && mGoogleApiClient.isConnected());
    }

    public static DBAdapter DB() {
        try {
            synchronized (com.millonario.millionaire.classes.App.class) {
                DBAdapter dbadapter;
                if (db == null) {
                    db = new DBAdapter(getContext());
                    db.open();
                }
                dbadapter = db;
                return dbadapter;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean activityResult(int i, int j, Intent intent) {
        if (mHelper == null) {
            return false;
        } else {
            return mHelper.handleActivityResult(i, j, intent);
        }
    }

    public static void carregaPublicidade(Context context) {
        if (isPremium()) {
            return;
        } else {
            interstitial = new InterstitialAd(context);
            interstitial.setAdUnitId(getMyString(R.string.intertitial_ID));
            interstitial.setAdListener(new AdListener() {

                public void onAdClosed() {
                    if (App.instance.ctx != null) {
                        App.instance.ctx = null;
                    }
                }

            });
            com.google.android.gms.ads.AdRequest adrequest = (new com.google.android.gms.ads.AdRequest.Builder()).build();
            interstitial.loadAd(adrequest);
            return;
        }
    }

    public static void carregaPublicidadeVideo(final Context ctx) {
        interstitial = new InterstitialAd(ctx);
        interstitial.setAdUnitId("ca-app-pub-2863074179801949/5962712718");
        interstitial.setAdListener(new AdListener() {
            public void onAdClosed() {
                if (App.instance.ctx != null) {
                    App.instance.ctx = null;
                }
                ((AjudasExtraActivity) ctx).CompraConcluida(true, 1);
            }

            public void onAdFailedToLoad(int i) {
                Log.e("AdFail", (new StringBuilder("ErroAd: ")).append(i).toString());
                PopUp.show(ctx, 0, 1);
                PopUp.setMensagem(App.getMyString(R.string.video_nao_disponivel));
                PopUp.setTextoEListenerBotao(App.getMyString(R.string.ok), new android.view.View.OnClickListener() {
                    public void onClick(View view) {
                        PopUp.hide();
                    }
                }, 1);
            }

            public void onAdLoaded() {
                App.interstitial.show();
            }
        });
        com.google.android.gms.ads.AdRequest adrequest = (new com.google.android.gms.ads.AdRequest.Builder()).build();
        interstitial.loadAd(adrequest);
    }

    public static Context getContext() {
        return instance.getBaseContext();
    }

    public static int getMyDrawable(String s) {
        if (getMyResources().getBoolean(R.bool.isTablet)) {
            int i = getMyResources().getIdentifier((new StringBuilder(String.valueOf(s))).append("_tablet").toString(), "drawable", getContext().getPackageName());
            if (i != -1 && i != 0) {
                return i;
            }
        }
        return getMyResources().getIdentifier(s, "drawable", getContext().getPackageName());
    }

    public static Resources getMyResources() {
        return instance.getResources();
    }

    public static String getMyString(int i) {
        return instance.getResources().getString(i);
    }

    public static String getMyString(int i, String s) {
        return instance.getResources().getString(i, new Object[]{
                s
        });
    }

    public static Pergunta getPerguntaAtual() {
        if (Perguntas == null || Perguntas.size() <= PerguntaAtual) {
            return null;
        } else {
            return DB().getPergunta(((Integer) Perguntas.get(PerguntaAtual)).intValue());
        }
    }

    public static ArrayList getPerguntas(boolean flag) {
        int i;
        if (!flag && Perguntas != null && Perguntas.size() != 0) {
            return null;
        }
        i = 0;
        Perguntas = DB().getPerguntas();
        while (Perguntas.size() < 25 && i < 3) {
            Perguntas = DB().getPerguntas();
            i++;
        }
        if (i != 4) {
            try {
                PerguntasTroca = new ArrayList();
                PerguntasTrocaExtra = new ArrayList();
                PerguntasTrocaExtra.add((Integer) Perguntas.get(4));
                PerguntasTrocaExtra.add((Integer) Perguntas.get(9));
                PerguntasTrocaExtra.add((Integer) Perguntas.get(14));
                PerguntasTrocaExtra.add((Integer) Perguntas.get(19));
                PerguntasTrocaExtra.add((Integer) Perguntas.get(24));
                PerguntasTroca.add((Integer) Perguntas.get(3));
                PerguntasTroca.add((Integer) Perguntas.get(8));
                PerguntasTroca.add((Integer) Perguntas.get(13));
                PerguntasTroca.add((Integer) Perguntas.get(18));
                PerguntasTroca.add((Integer) Perguntas.get(23));
                Perguntas.remove(24);
                Perguntas.remove(23);
                Perguntas.remove(19);
                Perguntas.remove(18);
                Perguntas.remove(14);
                Perguntas.remove(13);
                Perguntas.remove(9);
                Perguntas.remove(8);
                Perguntas.remove(4);
                Perguntas.remove(3);
            } catch (Exception exception) {
                Perguntas = new ArrayList();
                exception.printStackTrace();
            }
            return Perguntas;
        }
        Perguntas = new ArrayList();
        return Perguntas;
    }


    public static boolean isApplicationSentToBackground() {
        return !hasFocus;
    }

    public static boolean isPremium() {
        String s = DB().getValue("IsPremium");
        if (!Utils.isEmptyDB(s)) {
            if (s.equals("1")) {
                s = "sim";
                DB().insertValue(s, "IsPremium");
            }
        } else {
            s = "nao";
            DB().insertValue(s, "IsPremium");
        }
        return s.equals("sim");
    }

    public static boolean mostraPublicidade(Context context) {
        if (interstitial != null && interstitial.isLoaded() && !mostrouPublicidade) {
            mostrouPublicidade = true;
            instance.ctx = context;
            interstitial.show();
            return true;
        } else {
            return false;
        }
    }


    public void onCreate() {

//        Permission[] permissions = new Permission[] {
//                Permission.PUBLIC_PROFILE,
//                Permission.PUBLISH_ACTION,
//                Permission.USER_FRIENDS
//        };
//
//        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
//                .setAppId(APP_ID)
//                .setNamespace(APP_NAMESPACE)
//                .setPermissions(permissions)
//                .setDefaultAudience(SessionDefaultAudience.FRIENDS)
//                .setAskForAllPermissionsAtOnce(false)
//                .build();
//        SimpleFacebook.setConfiguration(configuration);


        SharedPreferences pm=PreferenceManager.getDefaultSharedPreferences(this);
        if(pm.getInt("first",-1)==-1){
            pm.edit().putInt("first",0).commit();
        }
        ExceptionHandler.register(
                this,
                "Error report Millonario",
                "morejonleslie@gmail.com");
//        FacebookSdk.sdkInitialize(this);
        try {
            synchronized (this) {
                super.onCreate();
                instance = this;
                Musica = MusicaFundo.getInstance();
                db = new DBAdapter(this);
                db.open();
                com.nostra13.universalimageloader.core.ImageLoaderConfiguration imageloaderconfiguration = ((new com.nostra13.universalimageloader.core.ImageLoaderConfiguration.Builder(this)).memoryCacheExtraOptions(480, 800)).memoryCacheSize(0xa00000).discCacheSize(0x6400000).threadPoolSize(10).build();
                ImageLoader.getInstance().init(imageloaderconfiguration);
                //setUpInApp(getContext(), null, null);
            }
        } catch (Exception e) {
        }
    }

    public void onTerminate() {
        Log.d("Termine T", "");
        MusicaFundo.getInstance().stop();
        super.onTerminate();
    }

    static {
        isContraRelogio = false;
        hasFocus = true;
        is5050Disponivel = 0;
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
