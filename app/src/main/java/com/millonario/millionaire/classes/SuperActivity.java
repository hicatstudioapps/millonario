package com.millonario.millionaire.classes;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kskkbys.rate.RateThisApp;
import com.millonario.millionaire.EcraPrincipalActivity;
import com.millonario.millionaire.PerguntaActivity;
import com.millonario.millionaire.R;
import com.millonario.millionaire.popup.PopUp;

import com.millonario.millionaire.utils.Ads;
import com.millonario.millionaire.utils.MusicaFundo;
import com.millonario.millionaire.utils.TestConnection;
import com.millonario.millionaire.utils.Utils;

import java.util.Calendar;

// Referenced classes of package com.millonario.millionaire.classes:
//            App

public class SuperActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    BroadcastReceiver br_erro_update;
    BroadcastReceiver br_killMain;
    BroadcastReceiver br_nao_existe_Update;
    BroadcastReceiver br_update;
    BroadcastReceiver br_verificaUpdate;

    Dialog dialog_promoTrivial;
    boolean isRunning;
    boolean isStartingActivity;



    public SuperActivity() {
        isRunning = false;
        isStartingActivity = false;
    }

    private void checkGCM() {
    }



    public void DefinicoesBtn_click(View view) {
        startActivity(new Intent(this, com.millonario.millionaire.DefinicoesActivity.class));
    }

    public void NovoJogoBtn_click(View view) {
        if (!podeContinuar()) {
            return;
        } else {
            NovoJogoBtn_click(false);
            return;
        }
    }

    public void NovoJogoBtn_click(boolean flag) {
        if (!podeContinuar()) {
            return;
        }
        if (flag) {
            goToNovoJogo();
            return;
        } else {
            mostraPopUpTipoJogo();
            return;
        }
    }

    public void finish() {
        isStartingActivity = true;
        super.finish();
    }

    protected void goToNovoJogo() {
        //Analytics.send("Novo Jogo", null, null);
        App.mostrouPublicidade = false;
        App.Perguntas = null;
        App.PerguntasTroca = null;
        App.getPerguntas(true);
        if (App.Perguntas != null && App.Perguntas.size() == 15) {
            App.PerguntaAtual = 0;
            App.DinheiroAtual = "0";
            App.is5050Disponivel = 0;
            App.isPublicoDisponivel = 0;
            App.isTelefoneDisponivel = 0;
            App.isTrocaDisponivel = 0;
            if (App.isContraRelogio) {
                Utils.iniciaTimer();
                MusicaFundo.getInstance().realPause();
            }
            if (!App.isPremium()) {
                App.carregaPublicidade(this);
            }
            startActivity((new Intent(this, com.millonario.millionaire.PerguntaActivity.class)).setFlags(0x4000000));
        }
    }

    protected void mostraPopUpPremium() {
        startActivity(new Intent(this, com.millonario.millionaire.AjudasExtraActivity.class));
    }

    protected void mostraPopUpTipoJogo() {
        PopUp.show(this, 0, 3);
        PopUp.setMensagem(getString(R.string.modo_jogo));
        PopUp.setTextoEListenerBotao(getString(R.string.normal), new android.view.View.OnClickListener() {
            public void onClick(View view) {
                PopUp.hide();
                App.isContraRelogio = false;
                goToNovoJogo();
            }
        }, 1);
        PopUp.setTextoEListenerBotao(getString(R.string.contrarrelogio), new android.view.View.OnClickListener() {
            public void onClick(View view) {
                PopUp.hide();
                App.isContraRelogio = true;
                goToNovoJogo();
            }
        }, 2);
        PopUp.setTextoEListenerBotao(getString(R.string.cancelar), new android.view.View.OnClickListener() {
            public void onClick(View view) {
                PopUp.hide();
            }
        }, 3);
    }

    public void onBackPressed() {
        if (Ads.onBackPressed()) {
            return;
        } else {
            isStartingActivity = true;
            super.onBackPressed();
            return;
        }
    }


    protected void onCreate(Bundle bundle) {
        String s;
        super.onCreate(bundle);

        try {
            //setUpBroadcast();
            // Analytics.getTracker(com.millonario.millionaire.utils.Analytics.TrackerName.APP_TRACKER);
            Intent intent = getIntent();
            boolean flag = false;
            Calendar calendar;
            if (intent != null) {
                boolean flag1 = intent.hasExtra("notificacao");
                flag = false;
                if (flag1) {
                    if (intent.getBooleanExtra("notificacao", false)) {
                        flag = true;
                    } else {
                        flag = false;
                    }
                }
            }
            if (App.primeiraVez) {
                //this instanceof SplashActivity;
            }
            if (!App.primeiraVez && !flag) {
                Ads.onCreate(this);
                return;
            }
            Ads.init(this);
            App.primeiraVez = false;
            //checkGCM();
            //App.isPremium();
            /*s = App.DB().getValue("proximo_update");
	        if(!s.equals("") && !s.equals("(null)") && !s.equals("<null>")){
	        	long l = Long.parseLong(s);
	            if(Calendar.getInstance().getTimeInMillis() > l)
	            {
	                Calendar calendar1 = Calendar.getInstance();
	                calendar1.add(5, 5);
	                App.DB().insertValue((new StringBuilder()).append(calendar1.getTimeInMillis()).toString(), "proximo_update");
	                VerificaUpdate verificaupdate = new VerificaUpdate(this);
	                check = verificaupdate;
	                verificaupdate.execute(new String[] {
	                    ""
	                });
	            }
	        }else{
		        calendar = Calendar.getInstance();
		        calendar.add(5, 5);
		        App.DB().insertValue((new StringBuilder()).append(calendar.getTimeInMillis()).toString(), "proximo_update");
	        }*/
            String temp = App.DB().getValue("Dinheiro");
            if (Float.parseFloat(Utils.converteDinheiro(App.DB().getValue("Dinheiro"))) <= 2000000F && TestConnection.test()) {
                //(new EnviaStatsTask()).execute(new String[0]);
            }
            RateThisApp.onStart(this);
            RateThisApp.showRateDialogIfNeeded(this);
            Ads.onCreate(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void onDestroy() {
        Ads.onDestroy(this);
        if (br_verificaUpdate != null) {
            unregisterReceiver(br_verificaUpdate);
            br_verificaUpdate = null;
        }
        if (br_nao_existe_Update != null) {
            unregisterReceiver(br_nao_existe_Update);
            br_nao_existe_Update = null;
        }
        if (br_update != null) {
            unregisterReceiver(br_update);
            br_update = null;
        }
        if (br_erro_update != null) {
            unregisterReceiver(br_erro_update);
            br_erro_update = null;
        }
        if (br_killMain != null) {
            unregisterReceiver(br_killMain);
            br_killMain = null;
        }

        try {
            if (PopUp.isOpen && PopUp.ctx.getClass().getName().equals(getClass().getName())) {
                PopUp.hide();
            }
        } catch (Exception exception) {
        }
        super.onDestroy();
        Log.e(getLocalClassName(), "DESTROY");
    }

    protected void onPause() {
        super.onPause();
        Ads.onPause(this);
        isRunning = false;
        String s;
        StringBuilder stringbuilder;
        String s1;
        try {
            PowerManager powermanager = (PowerManager) getSystemService("power");
            App.Musica.pause();
            App.Musica.paraContraRelogio();
        } catch (Exception exception) {
        }
        s = getLocalClassName();
        stringbuilder = new StringBuilder("F: ");
        if (isFinishing()) {
            s1 = "TRUE";
        } else {
            s1 = "FALSE";
        }
        Log.e(s, stringbuilder.append(s1).toString());
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    protected void onResume() {
        super.onResume();
        isRunning = true;
        if (!App.Musica.isPlaying) {
            App.Musica.start();
        }
    }

    public void onStart() {
        super.onStart();
        //Analytics.onStart(this);
    }

    public void onStop() {
        super.onStop();
        try {
            //Analytics.onStop(this);
            if (!App.isApplicationSentToBackground()) {
                //break MISSING_BLOCK_LABEL_33;
                return;
            }
            if (App.Musica.isPlaying) {
                App.Musica.pause();
            }
        } catch (Exception e) {
        }
    }

    public void onWindowFocusChanged(boolean flag) {
        super.onWindowFocusChanged(flag);
        if (!flag && isRunning && !isStartingActivity && !App.isPopUpShowing) {
            App.hasFocus = false;
        }
        if (!flag || !isRunning) {
            if (isRunning || App.isPopUpShowing || isStartingActivity) {
                if (App.isPopUpShowing) {
                    App.isPopUpShowing = false;
                    return;
                }
                if (isStartingActivity) {
                    isStartingActivity = false;
                    return;
                }
                return;
            }
            App.Musica.paraContraRelogio();
            App.Musica.pause();
            return;
        }
        App.hasFocus = true;
        if(this instanceof PerguntaActivity)
            App.Musica.tocaContraRelogio();
        else
            App.Musica.start();
        /*if (!App.isContraRelogio || !(this instanceof PerguntaActivity) || !((PerguntaActivity) this).shouldPlayMusicaContraRelogio()) {
            if (!App.Musica.isPlaying && (!(this instanceof PerguntaActivity) || ((PerguntaActivity) this).shouldPlayMusicaNormal())) {
                App.Musica.start();
                return;
            }
        }
        if (!App.Musica.isPlayingCR) {
         //   App.Musica.tocaContraRelogio();
        }*/
    }

    protected boolean podeContinuar() {
        return true;
    }

    public void startActivity(Intent intent) {
        isStartingActivity = true;
        super.startActivity(intent);
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
