package com.millonario.millionaire;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.millonario.millionaire.classes.App;

// Referenced classes of package com.millonario.millionaire:
//            EcraPrincipalActivity, GcmBroadcastReceiver

public class GcmIntentService extends IntentService {

    public static final int NOTIFICATION_ID = 0x27009;
    android.support.v4.app.NotificationCompat.Builder builder;
    private NotificationManager mNotificationManager;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    private void sendNotification(String s) {
        mNotificationManager = (NotificationManager) getSystemService("notification");
        PendingIntent pendingintent = PendingIntent.getActivity(this, 0, (new Intent(this, com.millonario.millionaire.EcraPrincipalActivity.class)).setFlags(0x24000000).putExtra("notificacao", true), 0);
        android.support.v4.app.NotificationCompat.Builder builder1 = (new android.support.v4.app.NotificationCompat.Builder(this)).setSmallIcon(R.drawable.ic_launcher).setContentTitle(App.getMyString(R.string.app_name)).setStyle((new android.support.v4.app.NotificationCompat.BigTextStyle()).bigText(App.getMyString(R.string.notificacao_novas_perguntas))).setAutoCancel(true).setDefaults(-1).setContentText(App.getMyString(R.string.notificacao_novas_perguntas));
        builder1.setContentIntent(pendingintent);
        mNotificationManager.notify(0x27009, builder1.build());
    }

    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        String s = GoogleCloudMessaging.getInstance(this).getMessageType(intent);
        if (!bundle.isEmpty() && "gcm".equals(s)) {
            sendNotification(bundle.toString());
            Log.i("GCM", (new StringBuilder("Received: ")).append(bundle.toString()).toString());
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }
}
