package com.millonario.millionaire;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

//import com.facebook.AccessToken;
//import com.facebook.AccessTokenTracker;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.login.DefaultAudience;
//import com.facebook.login.LoginManager;
//import com.facebook.share.Sharer;
//import com.facebook.share.model.AppInviteContent;
//import com.facebook.share.model.GameRequestContent;
//import com.facebook.share.model.ShareContent;
//import com.facebook.share.model.ShareLinkContent;
//import com.facebook.share.widget.AppInviteDialog;
//import com.facebook.share.widget.GameRequestDialog;
//import com.facebook.share.widget.MessageDialog;
//import com.facebook.share.widget.ShareDialog;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.classes.SuperActivity;
import com.millonario.millionaire.interfaces.VideoRewardInterface;
import com.millonario.millionaire.popup.PopUp;
import com.millonario.millionaire.utils.Ads;
import com.millonario.millionaire.utils.TestConnection;
import com.millonario.millionaire.utils.Utils;
//import com.nineoldandroids.animation.Animator;
//import com.nineoldandroids.animation.ObjectAnimator;
//import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;
//import com.sromku.simple.fb.SimpleFacebook;
//import com.sromku.simple.fb.entities.Feed;
//import com.sromku.simple.fb.listeners.OnInviteListener;
//import com.sromku.simple.fb.listeners.OnPublishListener;

import java.util.Arrays;
import java.util.List;

import badabing.lib.apprater.AppRater;

public class AjudasExtraActivity extends SuperActivity
        implements VideoRewardInterface {

    private static final String VIDEO_URLS[][];
    private static final String TAG = "AjudasExtraActivity";
    private static final String ADDITIONAL_PERMISSIONS = "publish_actions";
    static int NumAjudasAdicionar = 0;
    static boolean deveAdicionarAjudas = false;
    private static boolean INTENT_PARTILHA = false;

//    private SimpleFacebook mSimpleFacebook;
    ValueAnimator pulseAnim;
//    private CallbackManager callbackManager;
//    private GameRequestDialog gameRequestDialog;
//    private AccessTokenTracker accessTokenTracker;
//    private ShareDialog shareDialog;
//    private MessageDialog messageDialog;
//    private AppInviteDialog appInviteDialog;

    public AjudasExtraActivity() {

    }

    private void AvaliarAppBtn_click() {
        /*if (!TestConnection.test()) {
            PopUp.show(this, 0, 1);
            PopUp.setMensagem(getString(R.string.necessario_conexao));
            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
                public void onClick(View view) {
                    PopUp.hide();
                }
            }, 1);
        } else if (!App.DB().getValue("AjudaExtraAvaliar").equals("1")) {
            //Analytics.send("Ajudas Extra", "Avaliar", null);
            App.DB().insertValue("1", "AjudaExtraAvaliar");*/
            NumAjudasAdicionar = 1;
            deveAdicionarAjudas = true;
            AppRater.rateNow(this);
            return;
      //  }
    }

    private void PartilharFacebookBtn_click() {
        if(!TestConnection.test())
        {
            PopUp.show(this, 0, 1);
            PopUp.setMensagem(getString(R.string.necessario_conexao));
            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
                public void onClick(View view)
                {
                    PopUp.hide();
                }
            }, 1);
        } else
        {
            App.DB().getValue("AjudaExtraPartilharFacebook").equals("1");
            String s = App.DB().getValue("AjudaExtraPartilharFacebookTempo");
            if (Utils.isEmptyDB(s)) {
                s = "0";
            }
            boolean flag;
            if (System.currentTimeMillis() < 0x240c8400L + Long.parseLong(s)) {
                flag = true;
            } else {
                flag = false;
            }
            if (!flag) {
//                mSimpleFacebook.publish(feed, true, onPublishListener);
//
//                if(AccessToken.getCurrentAccessToken()!=null && AccessToken.getCurrentAccessToken().getPermissions().contains(ADDITIONAL_PERMISSIONS)) {
//                    ShareLinkContent link = new ShareLinkContent.Builder()
//                            .setContentTitle("Share")
//                            .setContentUrl(Uri.parse(getString(R.string.link_google_play)))
//                            .build();
//                    shareDialog.show(link, ShareDialog.Mode.AUTOMATIC);
//                }else
//                LoginManager.getInstance()
//                        .setDefaultAudience(DefaultAudience.FRIENDS)
//                        .logInWithPublishPermissions(
//                                AjudasExtraActivity.this,
//                                Arrays.asList(ADDITIONAL_PERMISSIONS));

            }
        }
    }

//    Feed feed ;
//
//    OnPublishListener onPublishListener = new OnPublishListener() {
//        @Override
//        public void onComplete(String postId) {
////            Log.i(TAG, "Published successfully. The new post id = " + postId);
//
//        }
//
//    };

    private void PartilharTwitterBtn_click() {
        if (!TestConnection.test()) {
            PopUp.show(this, 0, 1);
            PopUp.setMensagem(getString(R.string.necessario_conexao));
            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
                public void onClick(View view) {
                    PopUp.hide();
                }
            }, 1);
        } else {
//            App.DB().getValue("AjudaExtraPartilharTwitter").equals("1");
//            String s = App.DB().getValue("AjudaExtraPartilharTwitterTempo");
//            if (Utils.isEmptyDB(s)) {
//                s = "0";
//            }
//            boolean flag;
//            if (System.currentTimeMillis() < 0x240c8400L + Long.parseLong(s)) {
//                flag = true;
//            } else {
//                flag = false;
//            }
//            if (!flag) {
                INTENT_PARTILHA = true;
                //Analytics.send("Partilhar", "Ajudas Extra", null);
                //Analytics.send("Ajudas Extra", "Twitter", null);
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("text/plain");
                intent.putExtra("android.intent.extra.TEXT", (new StringBuilder(String.valueOf(getString(R.string.twitter_description)))).append(" ").append(getString(R.string.link_google_play)).toString());
                startActivity(Intent.createChooser(intent, getString(R.string.partilha_pergunta)));
                return;
//            }
        }
    }

    private void VerVideoBtn_click() {
//        if (!TestConnection.test()) {
//            PopUp.show(this, 0, 1);
//            PopUp.setMensagem(getString(R.string.necessario_conexao));
//            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
//                public void onClick(View view) {
//                    PopUp.hide();
//                }
//            }, 1);
//            return;
//        } else {
            //Analytics.send("Ajudas Extra", "Ver v\355deo", null);
            //Analytics.send("V\355deo", "Ecr\343 Ajudas Extra", null);
            Ads.showVideoReward(this);
            return;
        //}
    }

    private void animaUpdateAjudasExtra() {
        if (pulseAnim != null) {
            try {
                pulseAnim.end();
            } catch (Exception exception) {
            }
            pulseAnim = null;
            ViewHelper.setRotationY(findViewById(R.id.rl_ajudasAtuais), 0.0F);
        }
        pulseAnim = ObjectAnimator.ofFloat(findViewById(R.id.rl_ajudasAtuais), "rotationY", new float[]{
                0.0F, 180F, 360F
        });
        pulseAnim.setDuration(1500L);
        pulseAnim.setRepeatCount(0);
        pulseAnim.addListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
                ViewHelper.setRotationY(findViewById(R.id.rl_ajudasAtuais), 0.0F);
                pulseAnim = null;
            }

            public void onAnimationEnd(Animator animator) {
                ViewHelper.setRotationY(findViewById(R.id.rl_ajudasAtuais), 0.0F);
                pulseAnim = null;
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
            }
        });
        pulseAnim.start();
    }

    private void compraAjuda(String s) {
        if (!TestConnection.test()) {
            PopUp.show(this, 0, 1);
            PopUp.setMensagem(getString(R.string.necessario_conexao));
            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
                public void onClick(View view) {
                    PopUp.hide();
                }
            }, 1);
            return;
        } else {
            //Analytics.send("Ajudas Extra", "InApp", s);
            //  App.iniciaCompra(this, s);
            return;
        }
    }



    private void publishFeedDialog() {
        //Analytics.send("Ajudas Extra", "Facebook", null);
//        Bundle bundle = new Bundle();
//        bundle.putString("name", getString(R.string.facebook_nome));
//        bundle.putString("caption", getString(R.string.facebook_caption));
//        bundle.putString("description", getString(R.string.twitter_description));
//        bundle.putString("link", getString(R.string.link_google_play));
//        bundle.putString("picture", getString(R.string.facebook_logo));
//        ((com.facebook.widget.WebDialog.FeedDialogBuilder)(new com.facebook.widget.WebDialog.FeedDialogBuilder(this, Session.getActiveSession(), bundle)).setOnCompleteListener(new com.facebook.widget.WebDialog.OnCompleteListener() {
//            public void onComplete(Bundle bundle1, FacebookException facebookexception)
//            {
//                if(facebookexception == null)
//                {
//                    if(bundle1.getString("post_id") != null)
//                    {
//                        Toast.makeText(AjudasExtraActivity.this, R.string.publicado_sucesso, 0).show();
//                        AjudasExtraActivity.deveAdicionarAjudas = true;
//                        AjudasExtraActivity.NumAjudasAdicionar = 2;
//                        App.DB().insertValue("1", "AjudaExtraPartilharFacebook");
//                        App.DB().insertValue((new StringBuilder()).append(System.currentTimeMillis()).toString(), "AjudaExtraPartilharFacebookTempo");
//                        verificaNovaAjudas();
//                    }
//                    return;
//                } else
//                {
//                    Toast.makeText(AjudasExtraActivity.this, R.string.erro_geral, 0).show();
//                    return;
//                }
//            }
//        })).build().show();
    }

    private void verificaNovaAjudas() {
        if (deveAdicionarAjudas) {
            deveAdicionarAjudas = false;
            if (NumAjudasAdicionar > 0) {
                Utils.adicionaAjudaExtra(NumAjudasAdicionar);
                NumAjudasAdicionar = 0;
                ((TextView)findViewById(R.id.txt_ajudasAtuais)).setText((new StringBuilder()).append(Utils.getNumeroAjudasExtra()).toString());
                animaUpdateAjudasExtra();
            }
        }
    }

    public void CompraConcluida(boolean flag, int i) {
        if (flag) {
            deveAdicionarAjudas = true;
            NumAjudasAdicionar = i;
        } else {
            deveAdicionarAjudas = false;
            NumAjudasAdicionar = 0;
        }
        verificaNovaAjudas();
    }

    public void onActivityResult(int i, int j, Intent intent) {
        super.onActivityResult(i, j, intent);
//        mSimpleFacebook.onActivityResult(this, i, j, intent);

        try {
            App.activityResult(i, j, intent);
        } catch (Exception exception) {
        }
        try {
            //callbackManager.onActivityResult(i, j, intent);
            super.onActivityResult(i, j, intent);
            return;
        } catch (Exception exception1) {
            return;
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        Typeface tf = Typeface.createFromAsset(this.getResources().getAssets(), "norwester.otf");
        setContentView(R.layout.activity_ajudas_extra);

//        feed = new Feed.Builder()
//                .setName("Millonario")
//                .setCaption("Play the game, win the money.")
//                .setDescription("A classic game, enjoy and have fun for a while. You can share content and watch your friend's progress on facebook.")
//                .setPicture(getString(R.string.foto_publicar_facebook))
//                .setLink("https://play.google.com/store/apps/details?id=com.millonario.millonaire")
//                .build();
//
//        mSimpleFacebook = SimpleFacebook.getInstance(this);
        NumAjudasAdicionar = 0;
        deveAdicionarAjudas = false;
        INTENT_PARTILHA = false;
//        findViewById(R.id.rl_ajuda_facebook).setOnClickListener(new android.view.View.OnClickListener() {
//            public void onClick(View view) {
//            if(mSimpleFacebook.isLogin()){
//                mSimpleFacebook.publish(feed, true, onPublishListener);
//            }
////                PartilharFacebookBtn_click();
//            }
//        });


        ((TextView) findViewById(R.id.txt_ajudasAtuais)).setTypeface(tf);

        ((TextView) findViewById(R.id.txt_partilharAvaliar)).setTypeface(tf);
        ((TextView) findViewById(R.id.txt_partilharFacebook)).setTypeface(tf);
        ((TextView) findViewById(R.id.txt_partilharTwitter)).setTypeface(tf);
        ((TextView) findViewById(R.id.txt_partilharVideo)).setTypeface(tf);
        ((TextView) findViewById(R.id.friend)).setTypeface(tf);
        ((TextView) findViewById(R.id.messenger)).setTypeface(tf);
        findViewById(R.id.rl_ajuda_twitter).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view) {
                PartilharTwitterBtn_click();
            }
        });
        findViewById(R.id.rl_ajuda_avaliar).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view) {
                AvaliarAppBtn_click();
            }
        });
        findViewById(R.id.rl_ajuda_video).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view) {
                VerVideoBtn_click();
            }
        });
//        findViewById(R.id.rlFriends).setOnClickListener(new android.view.View.OnClickListener() {
//            public void onClick(View view) {
//                presentAppInviteDialog();
//            }
//        });
        findViewById(R.id.rlMessenger).setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View view) {
                shareUsingMessengerDialog();
            }
        });

//        gameRequestDialog = new GameRequestDialog(this);
//        callbackManager = CallbackManager.Factory.create();
//        gameRequestDialog.registerCallback(
//                callbackManager,
//                new FacebookCallback<GameRequestDialog.Result>() {
//                    @Override
//                    public void onCancel() {
//                        Log.d(TAG, "Canceled");
//                    }
//
//                    @Override
//                    public void onError(FacebookException error) {
//                        Log.d(TAG, String.format("Error: %s", error.toString()));
//                    }
//
//                    @Override
//                    public void onSuccess(GameRequestDialog.Result result) {
//                        Log.d(TAG, "Success!");
//                    }
//                });
//        FacebookCallback<Sharer.Result> callback =
//                new FacebookCallback<Sharer.Result>() {
//                    @Override
//                    public void onCancel() {
//                        Log.d(TAG, "Canceled");
//                    }
//
//                    @Override
//                    public void onError(FacebookException error) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getApplication());
//                        builder.setTitle(R.string.error_dialog_title).
//                                setMessage(error.toString()).
//                                setPositiveButton(R.string.ok, null);
//                        builder.show();
//                        Log.d(TAG, String.format("Error: %s", error.toString()));
//                    }
//
//                    @Override
//                    public void onSuccess(Sharer.Result result) {
//                        deveAdicionarAjudas=true;
//                        NumAjudasAdicionar=2;
//                        App.DB().insertValue("1", "AjudaExtraPartilharFacebook");
//                        App.DB().insertValue((new StringBuilder()).append(System.currentTimeMillis()).toString(), "AjudaExtraPartilharFacebookTempo");
//                        Toast.makeText(getApplicationContext(),getString(R.string.earn),Toast.LENGTH_SHORT).show();
//                        verificaNovaAjudas();
//                        Log.d(TAG, "Success!");
//                    }
//                };
//        shareDialog = new ShareDialog(this);
//        shareDialog.registerCallback(callbackManager, callback);
//        messageDialog = new MessageDialog(this);
//        messageDialog.registerCallback(callbackManager, callback);
//
//        FacebookCallback<AppInviteDialog.Result> appInviteCallback =
//                new FacebookCallback<AppInviteDialog.Result>() {
//                    @Override
//                    public void onSuccess(AppInviteDialog.Result result) {
//                        NumAjudasAdicionar=2;
//                        deveAdicionarAjudas=true;
//                        Toast.makeText(getApplicationContext(),getString(R.string.earn),Toast.LENGTH_SHORT).show();
//                        verificaNovaAjudas();
//                        Log.d(TAG, "Success!");
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        Log.d(TAG, "Canceled");
//                    }
//
//                    @Override
//                    public void onError(FacebookException error) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getApplication());
//                        builder.setTitle(R.string.error_dialog_title).
//                                setMessage(error.toString()).
//                                setPositiveButton(R.string.ok, null);
//                        builder.show();
//                        Log.d(TAG, String.format("Error: %s", error.toString()));
//                    }
//                };
//        appInviteDialog = new AppInviteDialog(this);
//        appInviteDialog.registerCallback(callbackManager, appInviteCallback);
        ((TextView)findViewById(R.id.txt_ajudasAtuais)).setText((new StringBuilder()).append(Utils.getNumeroAjudasExtra()).toString());
    }

//    OnInviteListener onInviteListener = new OnInviteListener() {
//        @Override
//        public void onComplete(List<String> invitedFriends, String requestId) {
//            Log.i(TAG, "Invitation was sent to " + invitedFriends.size() + " users with request id " + requestId);
//        }
//
//        @Override
//        public void onCancel() {
//            Log.i(TAG, "Canceled the dialog");
//        }
//        @Override
//        public void onException(Throwable throwable){
//
//        }
//        @Override
//        public void onFail(String s){
//
//        }
//    };
//
//
//
//    public void presentAppInviteDialog() {
//        if(mSimpleFacebook.isLogin())
//            mSimpleFacebook.invite("I invite you to use this app", onInviteListener,null);
//    }

    public void shareUsingMessengerDialog() {
//        if(AccessToken.getCurrentAccessToken()!=null && AccessToken.getCurrentAccessToken().getPermissions().contains(ADDITIONAL_PERMISSIONS)){
//        ShareContent content = getLinkContent();
//        // share the app
//        if (messageDialog.canShow(content)) {
//            messageDialog.show(content);
//        }}
//        else
//            LoginManager.getInstance()
//                    .setDefaultAudience(DefaultAudience.FRIENDS)
//                    .logInWithPublishPermissions(
//                            AjudasExtraActivity.this,
//                            Arrays.asList(ADDITIONAL_PERMISSIONS));
        startActivity(new Intent(this,OutrasAppsActivity.class));
    }

//    private ShareContent getLinkContent() {
//        return new ShareLinkContent.Builder()
//                .setContentTitle("Share")
//                .setContentUrl(Uri.parse(getString(R.string.link_google_play)))
//                .build();
//    }
//
//    private void gameChalenge(){
//        if(AccessToken.getCurrentAccessToken()!=null && AccessToken.getCurrentAccessToken().getPermissions().contains(ADDITIONAL_PERMISSIONS)){
//        String body= String.format(getString(R.string.challenge_dialog_message),Utils.converteDinheiro(App.DB().getValue("Dinheiro")),getApplicationInfo().name);
//        GameRequestContent newGameRequestContent = new GameRequestContent.Builder()
//                .setTitle(getString(R.string.challenge_dialog_title))
//                .setMessage(body)
//                .build();
//        gameRequestDialog.show(this, newGameRequestContent);}
//        else
//            LoginManager.getInstance()
//                    .setDefaultAudience(DefaultAudience.FRIENDS)
//                    .logInWithPublishPermissions(
//                            AjudasExtraActivity.this,
//                            Arrays.asList(ADDITIONAL_PERMISSIONS));
//
//    }

    public void onInternalVideoPlay() {
        App.Musica.start();
        String s = App.DB().getValue("AjudaExtraVerVideoTempo");
        if (Utils.isEmptyDB(s)) {
            s = "0";
        }
        if (System.currentTimeMillis() >= 0x5265c00L + Long.parseLong(s)) {
            App.DB().insertValue((new StringBuilder()).append(System.currentTimeMillis()).toString(), "AjudaExtraVerVideoTempo");
            App.DB().insertValue("nao", "ViuVideoAteFim");
            String s1 = App.DB().getValue("KEY_VIDEO_CURRENT_INDEX");
            if (Utils.isEmptyDB(s1)) {
                s1 = "0";
            }
            int i = 1 + Integer.parseInt(s1);
            if (i >= VIDEO_URLS.length || i < 0) {
                i = 0;
            }
            App.DB().insertValue((new StringBuilder()).append(i).toString(), "KEY_VIDEO_CURRENT_INDEX");
            String as[] = VIDEO_URLS[i];
            Intent intent = new Intent(null, Uri.parse((new StringBuilder("ytv://")).append(as[2]).toString()), this, com.millonario.millionaire.utils.youtube2.OpenYouTubePlayerActivity.class);
            intent.putExtra("KEY_VIDEO_NAME", getString(Integer.parseInt(as[0])));
            intent.putExtra("KEY_VIDEO_LINK", getString(Integer.parseInt(as[1])));
            startActivity(intent);
            return;
        } else {
            PopUp.show(this, 0, 1);
            PopUp.setMensagem(App.getMyString(R.string.video_nao_disponivel));
            PopUp.setTextoEListenerBotao(App.getMyString(R.string.ok), new android.view.View.OnClickListener() {
                public void onClick(View view) {
                    PopUp.hide();
                }
            }, 1);
            return;
        }
    }

    protected void onResume() {
        super.onResume();
        if (INTENT_PARTILHA) {
            INTENT_PARTILHA = false;
            NumAjudasAdicionar = 2;
            deveAdicionarAjudas = true;
            App.DB().insertValue("1", "AjudaExtraPartilharTwitter");
            App.DB().insertValue((new StringBuilder()).append(System.currentTimeMillis()).toString(), "AjudaExtraPartilharTwitterTempo");
        }
        verificaNovaAjudas();
        if (App.DB().getValue("ViuVideoAteFim").equals("sim")) {
            App.DB().insertValue("nao", "ViuVideoAteFim");
            NumAjudasAdicionar = 1;
            deveAdicionarAjudas = true;
            verificaNovaAjudas();
        }
    }

    public void onRewardVideoFinished(boolean flag, String s) {
        if (flag) {
            //Analytics.send("V\355deo", s, null);
            NumAjudasAdicionar = 1;
            deveAdicionarAjudas = true;
            verificaNovaAjudas();
        }
        App.Musica.start();
    }

    public void onWillDisplayRewardVideo() {
        App.Musica.realPause();
    }

    static {
        String as[][] = new String[3][];
        String as1[] = new String[3];
        as1[0] = (new StringBuilder()).append(R.string.trivial_app_name).toString();
        as1[1] = (new StringBuilder()).append(R.string.trivial_link_app_store).toString();
        as1[2] = "V5vJW6L2XgI";
        as[0] = as1;
        String as2[] = new String[3];
        as2[0] = (new StringBuilder()).append(R.string.wozzle_app_name).toString();
        as2[1] = (new StringBuilder()).append(R.string.wozzle_link_app_store).toString();
        as2[2] = "l4s0XR3gQJc";
        as[1] = as2;
        String as3[] = new String[3];
        as3[0] = (new StringBuilder()).append(R.string.swapper_app_name).toString();
        as3[1] = (new StringBuilder()).append(R.string.swapper_link_app_store).toString();
        as3[2] = "xe_S2l3t470";
        as[2] = as3;
        VIDEO_URLS = as;
    }


}
