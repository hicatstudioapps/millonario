package com.millonario.millionaire.interfaces;


public interface VideoRewardInterface {

    public abstract void onInternalVideoPlay();

    public abstract void onRewardVideoFinished(boolean flag, String s);

    public abstract void onWillDisplayRewardVideo();
}
