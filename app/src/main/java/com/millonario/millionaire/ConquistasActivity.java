package com.millonario.millionaire;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.classes.SuperActivity;
import com.millonario.millionaire.utils.Conquistas;
import com.millonario.millionaire.utils.Utils;
import com.nineoldandroids.view.ViewHelper;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;
import java.util.Arrays;

// Referenced classes of package com.millonario.millionaire:
//            LoginActivity

public class ConquistasActivity extends SuperActivity {

    public ConquistasActivity() {
    }

    private void loadAchivements() {
        String s = App.DB().getValue("Conquistas");
        ArrayList arraylist = new ArrayList();
        if (!Utils.isEmptyDB(s)) {
            arraylist = new ArrayList(Arrays.asList(s.split(",")));
        }
        int i = 0;
        int j = 1;
        do {
            if (j >= 10) {
                setUserDetails(i);
                return;
            }
            boolean flag = arraylist.contains((new StringBuilder()).append(j).toString());
            if (flag) {
                i = j;
            }
            View view = findViewById(getResources().getIdentifier((new StringBuilder("img_conquista")).append(j).toString(), "id", getPackageName()));
            float f;
            TextView textview;
            int k;
            Object aobj[];
            if (flag) {
                f = 1.0F;
            } else {
                f = 0.5F;
            }
            ViewHelper.setAlpha(view, f);
            textview = (TextView) findViewById(getResources().getIdentifier((new StringBuilder("txt_valor_conquista")).append(j).toString(), "id", getPackageName()));
            k = R.string.money_format;
            aobj = new Object[1];
            aobj[0] = getString(getResources().getIdentifier((new StringBuilder("valor_conquista")).append(j).toString(), "string", getPackageName()));
            textview.setText(getString(k, aobj));
            j++;
        } while (true);
    }

    private void setUserDetails(int i) {
        float f = 1.0F;
        String s = Utils.converteDinheiro(App.DB().getValue("Dinheiro"));
        if (s == null) {
            s = "0";
        }
        String s1 = "";
        int j = 0;
        int k = -1 + s.length();
        do {
            if (k < 0) {
                View view = findViewById(R.id.txt_conquistaUtilizador);
                float f1;
                View view1;
                String s2;
                int l;
                com.nostra13.universalimageloader.core.DisplayImageOptions displayimageoptions;
                TextView textview;
                String s3;
                if (i > 0) {
                    f1 = f;
                } else {
                    f1 = 0.5F;
                }
                ViewHelper.setAlpha(view, f1);
                view1 = findViewById(R.id.img_conquistaUtilizador);
                if (i <= 0) {
                    f = 0.5F;
                }
                ViewHelper.setAlpha(view1, f);
                if (i == 0) {
                    i = 1;
                }
                s2 = App.DB().getValue("FotoUtilizador");
                l = (int) TypedValue.applyDimension(1, 6F, getResources().getDisplayMetrics());
                displayimageoptions = (new com.nostra13.universalimageloader.core.DisplayImageOptions.Builder()).displayer(new RoundedBitmapDisplayer(l)).cacheOnDisc().build();
                if (!Utils.isEmpty(s2)) {
                    if (s2.equals("-")) {
                        ((ImageView) findViewById(R.id.img_utilizador)).setImageBitmap(null);
                    } else {
                        ImageLoader.getInstance().displayImage((new StringBuilder("file://")).append(s2).toString(), (ImageView) findViewById(R.id.img_utilizador), displayimageoptions);
                    }
                } else {
                    ImageLoader.getInstance().displayImage((new StringBuilder("drawable://")).append(App.getMyDrawable("img_default_user")).toString(), (ImageView) findViewById(R.id.img_utilizador), displayimageoptions);
                }
                textview = (TextView) findViewById(R.id.txt_nomeUtilizador);
                if (!Utils.isEmptyDB(App.DB().getValue("idUtilizador"))) {
                    s3 = App.DB().getValue("idUtilizador");
                } else {
                    s3 = getString(R.string.you);
                }
                textview.setText(s3);
                ((TextView) findViewById(R.id.txt_conquistaUtilizador)).setText(Conquistas.getAchivementName(i));
                ((ImageView) findViewById(R.id.img_conquistaUtilizador)).setImageResource(App.getMyDrawable((new StringBuilder("medalha_conquista_")).append(i).append("_lista").toString()));
                ((TextView) findViewById(R.id.txt_ganhosAcumulados)).setText(getString(R.string.money_format, new Object[]{
                        s1
                }));
                findViewById(R.id.ll_ganhosAcumulados).setOnClickListener(new android.view.View.OnClickListener() {
                    public void onClick(View view2) {
                        LoginBtn_click(view2);
                    }
                });
                return;
            }
            s1 = (new StringBuilder(String.valueOf(s.charAt(k)))).append(s1).toString();
            if (++j == 3) {
                j = 0;
                s1 = (new StringBuilder(" ")).append(s1).toString();
            }
            k--;
        } while (true);
    }

    public void LoginBtn_click(View view) {
        if (Utils.isEmptyDB(App.DB().getValue("idUtilizador"))) {
//            startActivity(new Intent(this, com.millonario.millionaire.LoginActivity.class));
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_conquistas);
        Typeface tf = Typeface.createFromAsset(this.getResources().getAssets(), "norwester.otf");
        ((TextView) findViewById(R.id.txt_nomeUtilizador)).setTypeface(tf);
        ((TextView) findViewById(R.id.txt_conquistaUtilizador)).setTypeface(tf);
        ((TextView) findViewById(R.id.txt_ganhosAcumulados)).setTypeface(tf);
    }

    protected void onResume() {
        super.onResume();
        loadAchivements();
    }
}
