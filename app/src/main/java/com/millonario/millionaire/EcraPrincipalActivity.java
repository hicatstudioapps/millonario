package com.millonario.millionaire;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.images.ImageManager;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.plus.Plus;
//import com.google.example.games.basegameutils.BaseGameUtils;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.classes.SuperActivity;
import com.millonario.millionaire.popup.PopUp;
import com.millonario.millionaire.utils.Conquistas;
import com.millonario.millionaire.utils.Utils;
import com.millonario.millionaire.views.SnowFlakeView;
import com.nineoldandroids.view.ViewHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
//import com.sromku.simple.fb.SimpleFacebook;
//import com.sromku.simple.fb.entities.Profile;
//import com.sromku.simple.fb.entities.Profile.Properties;
//import com.sromku.simple.fb.entities.Score;
//import com.sromku.simple.fb.listeners.OnProfileListener;
//import com.sromku.simple.fb.listeners.OnScoresListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import badabing.lib.ServerUtilities;
// Referenced classes of package com.millonario.millionaire:
//            AjudasExtraActivity, ConquistasActivity, DefinicoesActivity, LoginActivity, 
//            OutrasAppsActivity, RankingActivity

public class EcraPrincipalActivity extends SuperActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ImageManager.OnImageLoadedListener {
    // request codes we use when invoking an external activity
    public static final int RC_RESOLVE = 5000;
    public static final int RC_UNUSED = 5001;
    public static final int RC_SIGN_IN = 9001;
    // Client used to interact with Google APIs
    public static GoogleApiClient mGoogleApiClient;
    // Are we currently resolving a connection failure?
    public static boolean mResolvingConnectionFailure = false;
    // Has the user clicked the sign-in button?
    public static boolean mSignInClicked = false;
    // Automatically start the sign-in flow when the Activity starts
    public static boolean mAutoStartSignInFlow = true;
    boolean isLoaded;
    boolean isSnowing;

    private String USER_LOGGED_ID = "";
    SnowFlakeView snowView;
    AdView mAdView;
    AlarmManager alarmManager;
    PendingIntent pendingIntent;

    private AQuery asyncImageLoader;

//    private SimpleFacebook mSimpleFacebook;

    public EcraPrincipalActivity() {
        isLoaded = false;
        isSnowing = false;
    }

    public boolean isSignedIn() {
        return (mGoogleApiClient != null && mGoogleApiClient.isConnected());
    }

    @Override
    public void onStart() {
        super.onStart();
       // mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        Log.d("Termine", "");
        super.onStop();
       /* if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }*/
    }

    public void AjudasExtraBtn_click(View view) {
        //startActivity(new Intent(this, com.millonario.millionaire.AjudasExtraActivity.class));
        App.Musica.pause();
        App.mostraPublicidade(this);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        int interval = 10000; // 10 seconds
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + Long.parseLong(getString(R.string.time)), pendingIntent);
        //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        finish();
    }

    public void ConquistasBtn_click(View view) {
        startActivity(new Intent(this, ConquistasActivity.class));
    }

    public void DefinicoesBtn_click(View view) {
        startActivity(new Intent(this, DefinicoesActivity.class));
    }

    public void LoginBtn_click(View view) {
        mGoogleApiClient.connect();
    }

    public void login() {
        if (mSignInClicked)
            return;
        mSignInClicked = true;
        mGoogleApiClient.connect();
    }

    public void OutrasAppsBtn_click(View view) {
        startActivity(new Intent(this, OutrasAppsActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

//        mSimpleFacebook.onActivityResult(this, requestCode, resultCode, intent);
//        if (requestCode == RC_SIGN_IN) {
//            mSignInClicked = false;
//            mResolvingConnectionFailure = false;
//            if (resultCode == RESULT_OK) {
//                mGoogleApiClient.connect();
//            } else {
//                BaseGameUtils.showActivityResultError(this, requestCode, resultCode, R.string.signin_other_error);
//            }
//        }
    }

    public void RankingBtn_click(View view) {
//        if (App.isSignedIn()) {
//            startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(mGoogleApiClient),
//                    RC_UNUSED);
//        } else {
//            PopUp.show(this, 0, 1);
//            PopUp.setMensagem(getString(R.string.necessario_conexao));
//            PopUp.setTextoEListenerBotao(getString(R.string.ok), new View.OnClickListener() {
//                public void onClick(View view1) {
//                    PopUp.hide();
//                }
//            }, 1);
//            //BaseGameUtils.makeSimpleDialog(this, getString(R.string.R.string.necessario_conexao)).show();
//        }
       /* if(!TestConnection.test())
        {*/
            /*PopUp.show(this, 0, 1);
            PopUp.setMensagem(getString(R.string.necessario_conexao));
            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
                public void onClick(View view1)
                {
                    PopUp.hide();
                }
            }, 1);*/
            /*return;
        } else
        {
            startActivity(new Intent(this, com.millonario.millionaire.RankingActivity.class));
            return;
        }*/
    }


    public void onBackPressed() {
        if (App.isPopUpShowing) {
            PopUp.hide();
            return;
        }
        super.onBackPressed();
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        int interval = 10000; // 10 seconds
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + Long.parseLong(getString(R.string.time)), pendingIntent);
        App.mostraPublicidade(this);

    }

//    ProgressDialog thinkingDialog = new ProgressDialog(EcraPrincipalActivity.this);
//    thinkingDialog.setMessage("Espere...");
//
//    OnScoresListener mScoreListener = new OnScoresListener() {
//
//        @Override
//        public void onThinking() {
//            //thinkingDialog.show();
//        }
//
//        @Override
//        public void onComplete(List< Score > scores) {
//            //thinkingDialog.dismiss();
//
//            for (Score score : scores) {
//                if(score.getUser().getId().equals(USER_LOGGED_ID)){
//                    TextView valor = (TextView)findViewById(R.id.txt_ganhosAcumulados);
//                    valor.setText(score.getScore()+" €");
//                }
//            }
//        }
//
//        public void onException(Throwable throwable) {
//            //thinkingDialog.dismiss();
//            //buildErrorToast(throwable.getMessage()).show();
//        }
//
//        public void onFail(String reason) {
//            //thinkingDialog.dismiss();
//            //buildErrorToast(reason).show();
//        }
//    };
//
//    OnProfileListener onProfileListener = new OnProfileListener() {
//        @Override
//        public void onComplete(Profile profile) {
//            TextView textview = (TextView) findViewById(R.id.txt_nomeUtilizador);
//            textview.setText(profile.getFirstName());
//            ImageView user_picture = (ImageView)findViewById(R.id.img_utilizador);
//            asyncImageLoader.id(user_picture).image(profile.getPicture());
//            USER_LOGGED_ID = profile.getId();
//             mSimpleFacebook.getScores(mScoreListener);
//        }
//
//    /*
//     * You can override other methods here:
//     * onThinking(), onFail(String reason), onException(Throwable throwable)
//     */
//    };
//
//    Profile.Properties properties = new Profile.Properties.Builder()
//            .add(Properties.FIRST_NAME)
//            .add(Properties.PICTURE)
//            .add(Properties.ID)
//            .build();

    private void setUserDetails(int i) {
        float f = 1.0F;
        String s = Utils.converteDinheiro(App.DB().getValue("Dinheiro"));
        if (s == null) {
            s = "0";
        }
        String s1 = "";
        int j = 0;
        int k = -1 + s.length();
        do {
            if (k < 0) {
                View view = findViewById(R.id.txt_conquistaUtilizador);
                float f1;
                View view1;
                String s2;
                int l;
                com.nostra13.universalimageloader.core.DisplayImageOptions displayimageoptions;
                TextView textview;
                String s3;
                if (i > 0) {
                    f1 = f;
                } else {
                    f1 = 0.5F;
                }
                ViewHelper.setAlpha(view, f1);
                view1 = findViewById(R.id.img_conquistaUtilizador);
                if (i <= 0) {
                    f = 0.5F;
                }
                ViewHelper.setAlpha(view1, f);
                if (i == 0) {
                    i = 1;
                }
                s2 = App.DB().getValue("FotoUtilizador");
                l = (int) TypedValue.applyDimension(1, 6F, getResources().getDisplayMetrics());
                displayimageoptions = (new com.nostra13.universalimageloader.core.DisplayImageOptions.Builder()).displayer(new RoundedBitmapDisplayer(l)).cacheOnDisc().build();
                if (!Utils.isEmpty(s2)) {
                    if (s2.equals("-")) {
                        ((ImageView) findViewById(R.id.img_utilizador)).setImageBitmap(null);
                    } else {
                        ImageLoader.getInstance().displayImage((new StringBuilder("file://")).append(s2).toString(), (ImageView) findViewById(R.id.img_utilizador), displayimageoptions);
                    }
                } else {
                    ImageLoader.getInstance().displayImage((new StringBuilder("drawable://")).append(App.getMyDrawable("img_default_user")).toString(), (ImageView) findViewById(R.id.img_utilizador), displayimageoptions);
                }
                textview = (TextView) findViewById(R.id.txt_nomeUtilizador);
                if (!Utils.isEmptyDB(App.DB().getValue("idUtilizador"))) {
                    s3 = App.DB().getValue("idUtilizador");
                } else {
                    s3 = getString(R.string.you);
                }
                textview.setText(s3);
                ((TextView) findViewById(R.id.txt_conquistaUtilizador)).setText(Conquistas.getAchivementName(i));
                ((ImageView) findViewById(R.id.img_conquistaUtilizador)).setImageResource(App.getMyDrawable((new StringBuilder("medalha_conquista_")).append(i).append("_lista").toString()));
                ((TextView) findViewById(R.id.txt_ganhosAcumulados)).setText(getString(R.string.money_format, new Object[]{
                        s1
                }));
                findViewById(R.id.ll_ganhosAcumulados).setOnClickListener(new android.view.View.OnClickListener() {
                    public void onClick(View view2) {
                        LoginBtn_click(view2);
                    }
                });
                return;
            }
            s1 = (new StringBuilder(String.valueOf(s.charAt(k)))).append(s1).toString();
            if (++j == 3) {
                j = 0;
                s1 = (new StringBuilder(" ")).append(s1).toString();
            }
            k--;
        } while (true);
    }
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ServerUtilities.registerWithGCM(getApplicationContext());
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.millonario.millionaire",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (Exception e) {
//
//        }

//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
//                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
//                .build();
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        alarmIntent.putExtra("M", true);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        Typeface tf = Typeface.createFromAsset(this.getResources().getAssets(), "norwester.otf");
        setContentView(R.layout.activity_ecra_principal);
        ((Button) findViewById(R.id.btn_novoJogo)).setTypeface(tf);
        ((Button) findViewById(R.id.btn_ranking)).setTypeface(tf);
        ((Button) findViewById(R.id.btn_conquistas)).setTypeface(tf);
        ((TextView) findViewById(R.id.txt_nomeUtilizador)).setTypeface(tf);
        ((TextView) findViewById(R.id.txt_ganhosAcumulados)).setTypeface(tf);
        ((TextView) findViewById(R.id.txt_conquistaUtilizador)).setTypeface(tf);
//        ((ImageButton) findViewById(R.id.plus)).setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent i = new Intent(EcraPrincipalActivity.this, FacebookActivity.class );
//                        startActivity(i);
//                    }
//                }
//        );
        ((RelativeLayout) findViewById(R.id.rl_ecraPrincipal)).getViewTreeObserver().addOnGlobalLayoutListener(new android.view.ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                findViewById(R.id.rl_ecraPrincipal).getViewTreeObserver().removeGlobalOnLayoutListener(this);
                isLoaded = true;
            }
        });

        asyncImageLoader = new AQuery(EcraPrincipalActivity.this);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                if (mAdView.getVisibility() != View.VISIBLE) {
                    mAdView.setVisibility(View.VISIBLE);
                }
            }
        });

        mAdView.loadAd(adRequest);

    }

    protected void onPause() {
        super.onPause();
        mAdView.pause();
        if (snowView != null) {
            snowView.pause();
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        DefinicoesBtn_click(null);
        return true;
    }

    protected void onResume() {
        super.onResume();

//        mSimpleFacebook = SimpleFacebook.getInstance(this);
        mAdView.resume();
        setUserDetails(0);
        if (pendingIntent != null && alarmManager != null)
            alarmManager.cancel(pendingIntent);
//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
//            findViewById(R.id.plus).setVisibility(View.GONE);
        //Conquistas.verificaConquista(50000000, getApplicationContext());
        //mGoogleApiClient.connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Termine", "");
      //  mGoogleApiClient.disconnect();
    }

    public void setValorGanhoEBotaoLogin() {
        //Conquistas.verificaConquista(Float.parseFloat(Utils.converteDinheiro(App.DB().getValue("Dinheiro"))),getApplicationContext());
        if (mGoogleApiClient.isConnected()) {
            findViewById(R.id.plus).setVisibility(View.GONE);
        }
        String s = App.DB().getValue("Conquistas");
        boolean flag = Utils.isEmptyDB(s);
        int i = 0;
        if (!flag) {
            ArrayList arraylist = new ArrayList(Arrays.asList(s.split(",")));
            int j = arraylist.size();
            i = 0;
            if (j > 0) {
                i = Integer.parseInt((String) arraylist.get(-1 + arraylist.size()));
            }
        }
        String s1 = Utils.converteDinheiro(App.DB().getValue("Dinheiro"));
        Long.parseLong(s1);
        if (s1 == null) {
            s1 = "0";
        }
        String s2 = "";
        int k = 0;
        int l = -1 + s1.length();
        do {
            if (l < 0) {
                View view = findViewById(R.id.txt_conquistaUtilizador);
                float f;
                View view1;
                float f1;
                String s3;
                int i1;
                DisplayImageOptions displayimageoptions;
                TextView textview;
                String s4;
                if (i > 0) {
                    f = 1.0F;
                } else {
                    f = 0.5F;
                }
                ViewHelper.setAlpha(view, f);
                view1 = findViewById(R.id.img_conquistaUtilizador);
                if (i > 0) {
                    f1 = 1.0F;
                } else {
                    f1 = 0.5F;
                }
                ViewHelper.setAlpha(view1, f1);
                if (i == 0) {
                    i = 1;
                }
                s3 = App.DB().getValue("FotoUtilizador");
                i1 = (int) TypedValue.applyDimension(1, 6F, getResources().getDisplayMetrics());
                displayimageoptions = (new DisplayImageOptions.Builder()).displayer(new RoundedBitmapDisplayer(i1)).cacheOnDisc().build();
                if (!Utils.isEmpty(s3)) {

                    //ImageLoader.getInstance().displayImage((new StringBuilder("file://")).append(s3).toString(), (ImageView) findViewById(R.id.img_utilizador), displayimageoptions);

                } else {
                    //ImageLoader.getInstance().displayImage((new StringBuilder("drawable://")).append(App.getMyDrawable("img_default_user")).toString(), (ImageView) findViewById(R.id.img_utilizador), displayimageoptions);
                }
                //textview = (TextView) findViewById(R.id.txt_nomeUtilizador);
                String x = App.DB().getValue("idUtilizador");
                Log.d("idUtilizador", x);
                if (!x.equals("")) {
                    s4 = x;
                } else {
                    s4 = getString(R.string.you);
                }
                //textview.setText(s4);
                ((TextView) findViewById(R.id.txt_conquistaUtilizador)).setText(Conquistas.getAchivementName(i));
                ((ImageView) findViewById(R.id.img_conquistaUtilizador)).setImageResource(App.getMyDrawable((new StringBuilder("medalha_conquista_")).append(i).append("_lista").toString()));
//                ((TextView) findViewById(R.id.txt_ganhosAcumulados)).setText(getString(R.string.money_format, new Object[]{
//                        s2
//                }));
                findViewById(R.id.ll_ganhosAcumulados).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view2) {
                        LoginBtn_click(view2);
                    }
                });

                return;
            }
            s2 = (new StringBuilder(String.valueOf(s1.charAt(l)))).append(s2).toString();
            if (++k == 3) {
                k = 0;
                s2 = (new StringBuilder(" ")).append(s2).toString();
            }
            l--;
        } while (true);
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {
            Log.d("", "onConnected(): connected to Google APIs");
            // Show sign-out button on main menu
            App.mGoogleApiClient = mGoogleApiClient;
            Games.Leaderboards.submitScore(mGoogleApiClient, getString(R.string.word_attack_leaderboard), 0);
            findViewById(R.id.plus).setVisibility(View.GONE);

            Player p = Games.Players.getCurrentPlayer(mGoogleApiClient);
            ImageManager.create(this).loadImage(this, p.getIconImageUri());
            String displayName;

            if (p == null) {
                Log.w("", "mGamesClient.getCurrentPlayer() is NULL!");
                displayName = "???";
            } else {
                displayName = p.getDisplayName();

                //((TextView) findViewById(R.id.txt_nomeUtilizador)).setText(displayName);
                //pongo el user en la bd
                String userDB = App.DB().getValue("idUtilizador");

                if (!displayName.equals(userDB)) {
                    final long score;
                    App.DB().insertValue(displayName, "idUtilizador");
                    App.DB().insertValue(displayName, "UserName");

                    Games.Leaderboards.loadCurrentPlayerLeaderboardScore(mGoogleApiClient,
                            getString(R.string.word_attack_leaderboard),
                            LeaderboardVariant.TIME_SPAN_ALL_TIME,
                            LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(
                            new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
                                @Override
                                public void onResult(Leaderboards.LoadPlayerScoreResult arg0) {
                                    if (arg0 != null) {
                                        LeaderboardScore c = arg0.getScore();
                                        long current = Long.parseLong(Utils.converteDinheiro(App.DB().getValue("Dinheiro")));
                                        long online = c.getRawScore();
                                        Log.d("Online", online + "");
                                        if (online > 0 && current < online) {
                                            Conquistas.verificaConquista(Float.parseFloat(online + ""), getApplicationContext());
                                            App.DB().insertValue(online + ".0", "Dinheiro");

                                        } else {
                                            Games.Leaderboards.submitScore(mGoogleApiClient, getString(R.string.word_attack_leaderboard), current);
                                        }
                                    }
                                }

                            });
                } else {
                    Games.Leaderboards.loadCurrentPlayerLeaderboardScore(mGoogleApiClient,
                            getString(R.string.word_attack_leaderboard),
                            LeaderboardVariant.TIME_SPAN_ALL_TIME,
                            LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(
                            new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
                                @Override
                                public void onResult(Leaderboards.LoadPlayerScoreResult arg0) {
                                    if (arg0 != null) {
                                        LeaderboardScore c = arg0.getScore();
                                        long current = Long.parseLong(Utils.converteDinheiro(App.DB().getValue("Dinheiro")));
                                        long online = c.getRawScore();
                                        Log.d("Online", online + "");
                                        if (online > 0 && current < online) {
                                            Conquistas.verificaConquista(Float.parseFloat(online + ""), getApplicationContext());
                                            App.DB().insertValue(online + ".0", "Dinheiro");
                                        } else {
                                            Games.Leaderboards.submitScore(mGoogleApiClient, getString(R.string.word_attack_leaderboard), current);
                                        }
                                    }
                                }
                            });
                }
            }
            //actualizo el nombre en la barra de login
            setValorGanhoEBotaoLogin();
        } catch (Exception ex) {
            findViewById(R.id.plus).setVisibility(View.VISIBLE);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("", "onConnectionSuspended(): attempting to connect");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
//        Log.d("", "onConnectionFailed(): attempting to resolve");
//        if (mResolvingConnectionFailure) {
//            Log.d("", "onConnectionFailed(): already resolving");
//            return;
//        }
//        if (mSignInClicked) {
//            findViewById(R.id.plus).setVisibility(View.VISIBLE);
//            mAutoStartSignInFlow = false;
//            mSignInClicked = false;
//            mResolvingConnectionFailure = true;
//            if (!BaseGameUtils.resolveConnectionFailure(this, mGoogleApiClient, connectionResult,
//                    RC_SIGN_IN, getString(R.string.signin_other_error))) {
//                mResolvingConnectionFailure = false;
//            }
//        }
//
//        // Sign-in failed, so show sign-in button on main menu
//        setValorGanhoEBotaoLogin();
    }

    @Override
    public void onImageLoaded(Uri uri, Drawable drawable, boolean b) {
        final int width = !drawable.getBounds().isEmpty() ? drawable.getBounds().width() : drawable.getIntrinsicWidth();
        final int height = !drawable.getBounds().isEmpty() ? drawable.getBounds().height() : drawable.getIntrinsicHeight();
        // Now we check we are > 0
        final Bitmap bitmap = Bitmap.createBitmap(width <= 0 ? 1 : width, height <= 0 ? 1 : height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        String path = Environment.getExternalStorageDirectory().toString() + "/" + getPackageName() + "/";
        File directory = new File(path);
        if (!directory.exists())
            directory.mkdir();
        OutputStream fOut = null;
        File file = new File(path, "login.jpg"); // the File to save to
        try {
            fOut = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
        try {
            fOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fOut.close(); // do not forget to close the stream
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        App.DB().insertValue(file.getAbsolutePath(), "FotoUtilizador");
        String s3 = App.DB().getValue("FotoUtilizador");
        int i1 = (int) TypedValue.applyDimension(1, 6F, getResources().getDisplayMetrics());
        DisplayImageOptions displayimageoptions = (new DisplayImageOptions.Builder()).displayer(new RoundedBitmapDisplayer(i1)).cacheOnDisc().build();
        if (!Utils.isEmpty(s3)) {
            //ImageLoader.getInstance().displayImage((new StringBuilder("file://")).append(s3).toString(), (ImageView) findViewById(R.id.img_utilizador), displayimageoptions);
        } else {
            //ImageLoader.getInstance().displayImage((new StringBuilder("drawable://")).append(App.getMyDrawable("img_default_user")).toString(), (ImageView) findViewById(R.id.img_utilizador), displayimageoptions);
        }
    }
}
