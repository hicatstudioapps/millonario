package com.millonario.millionaire.listitem;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.millonario.millionaire.R;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.objetos.Utilizador;
import com.millonario.millionaire.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class RankingListItem extends LinearLayout {

    ImageView img_foto;
    Utilizador item;
    int posicao;
    TextView txt_nome;
    TextView txt_posicao;
    TextView txt_valor;

    public RankingListItem(Context context) {
        super(context);
    }

    public RankingListItem(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    public void setListItem(Utilizador utilizador, int i, boolean flag) {
        String s;
        String s1;
        int k;
        int i1;
        long l;
        long l1;
        item = utilizador;
        posicao = i;
        txt_nome = (TextView) findViewById(R.id.txt_nome);
        txt_valor = (TextView) findViewById(R.id.txt_valor);
        txt_posicao = (TextView) findViewById(R.id.txt_posicao);
        img_foto = (ImageView) findViewById(R.id.img_utilizador);
        txt_nome.setText(item.Nome);
        if (flag) {
            if (item.Tempo == null || !item.Tempo.equals("-")) {
                if (!item.Tempo.contains(".") && !item.Tempo.contains(",")) {
                    l1 = Long.parseLong(item.Tempo);
                    l = l1;
                } else
                    l = (long) Math.ceil(Double.parseDouble(item.Tempo));
                txt_valor.setText(Utils.constroiStringTempo(l));
            } else
                txt_valor.setText("-");
        } else {
            s = item.Dinheiro.replace(".0", "");
            s1 = "";
            k = 0;
            i1 = -1 + s.length();
            while (i1 >= 0) {
                s1 = (new StringBuilder(String.valueOf(s.charAt(i1)))).append(s1).toString();
                if (++k == 3) {
                    k = 0;
                    s1 = (new StringBuilder(" ")).append(s1).toString();
                }
                i1--;
            }
            txt_valor.setText((new StringBuilder(String.valueOf(s1))).append(" ").append(getContext().getString(R.string.money_symbol)).toString());
        }
        txt_posicao.setText((new StringBuilder(String.valueOf(posicao))).append("\272").toString());
        int j = (int) TypedValue.applyDimension(1, 6F, getResources().getDisplayMetrics());
        com.nostra13.universalimageloader.core.DisplayImageOptions displayimageoptions = (new com.nostra13.universalimageloader.core.DisplayImageOptions.Builder()).displayer(new RoundedBitmapDisplayer(j)).cacheOnDisc().build();
        if (!Utils.isEmpty(item.Foto)) {
            if (item.Foto.equals("-"))
                img_foto.setImageBitmap(null);
            else
                ImageLoader.getInstance().displayImage(item.Foto.replace("?type=large", "?width=300&height=300"), img_foto, displayimageoptions);
        } else
            ImageLoader.getInstance().displayImage((new StringBuilder("drawable://")).append(App.getMyDrawable("img_default_user")).toString(), img_foto, displayimageoptions);
        //txt_valor.setText("-");
    }

    public void setReticencias() {
        txt_posicao.setText("...");
        txt_valor.setText("");
        txt_nome.setText("");
        img_foto.setImageBitmap(null);
    }

    public void setSouEu(boolean flag, int i) {
        if (flag) {
            txt_posicao.setText((new StringBuilder(String.valueOf(i))).append("\272").toString());
            txt_posicao.setTextColor(getContext().getResources().getColor(R.color.orange));
            txt_valor.setTextColor(getContext().getResources().getColor(R.color.orange));
            txt_nome.setTextColor(getContext().getResources().getColor(R.color.orange));
        }
    }
}
