package com.millonario.millionaire;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.millonario.millionaire.classes.App;
import com.millonario.millionaire.classes.SuperActivity;
import com.millonario.millionaire.popup.PopUp;

//import com.millonario.millionaire.utils.AcessoExterno;
import com.millonario.millionaire.utils.DBAdapter;
import com.millonario.millionaire.utils.FileCopy;
import com.millonario.millionaire.utils.TestConnection;
import com.millonario.millionaire.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;

import badabing.lib.apprater.AppRater;

// Referenced classes of package com.millonario.millionaire:
//            LoginActivity

public class DefinicoesActivity extends SuperActivity
        implements android.widget.CompoundButton.OnCheckedChangeListener, android.view.View.OnClickListener {
    boolean loginFacebook;

    private AdView mAdView;







    public void AtualizarBaseDadosBtn_click(View view) {
//        if (TestConnection.test()) {
//            (new VerificaUpdate(this, true)).execute(new String[0]);
//            return;
//        } else {
//            PopUp.show(this, 0, 1);
//            PopUp.setMensagem(getString(R.string.necessario_conexao));
//            PopUp.setTextoEListenerBotao(getString(R.string.ok), new android.view.View.OnClickListener() {
//                public void onClick(View view1) {
//                    PopUp.hide();
//                }
//            }, 1);
//            return;
//        }
    }

    public void AvaliarBtn_click(View view) {
        AppRater.rateNow(this);
    }

    public void FacebookBtn_click(View view) {
       /* try
        {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("fb://profile/582295078477599")));
            return;
        }
        catch(Exception exception)
        {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.facebook.com/WalkMeMobileSolutions")));
        }*/

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String text = String.format(getString(R.string.share_app_default_text), "Millonario", "https://play.google.com/store/apps/details?id=com.millonaire.millonario");
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);

    }

    public void SincronizarBtn_click(View view) {
        boolean flag;
        if (Utils.isEmptyDB(App.DB().getValue("idUtilizador"))) {
            flag = false;
        } else {
            flag = true;
        }
        if (flag) {
            PopUp.show(this, 0, 2);
            PopUp.setMensagem(getString(R.string.confirmar_logout));
            PopUp.setTextoEListenerBotao(getString(R.string.sim), new android.view.View.OnClickListener() {
                public void onClick(View view1) {
                    PopUp.hide();
                    App.DB().logout();
                    // mostraEscondeViews();
                }
            }, 1);
            PopUp.setTextoEListenerBotao(getString(R.string.nao), new android.view.View.OnClickListener() {
                public void onClick(View view1) {
                    PopUp.hide();
                }
            }, 2);
        } else {
//            startActivity(new Intent(this, com.millonario.millionaire.LoginActivity.class));
        }
    }

    public void SugestoesBtn_click(View view) {
        /*String s;
        String s1;
        s = getString(R.string.email_titulo);
        s1 = "NaN";
        try{
	        String s3 = (new StringBuilder()).append(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode).toString();
	        s1 = s3;
        }catch(Exception e){}
        String s2 = (new StringBuilder("\n\n\n\n\n\n\n\n\nApp Name: ")).append(getString(R.string.app_name)).append(" \nModel: ").append(Build.MANUFACTURER).append(" (").append(Build.MODEL).append(")").append(" \nSystem Version: ").append(android.os.Build.VERSION.SDK_INT).append(" \nLanguage: ").append(Locale.getDefault().getDisplayLanguage()).append(" \nCountry: ").append(getResources().getConfiguration().locale.getDisplayCountry()).append(" \nApp Version: ").append(s1).toString();
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.EMAIL", new String[] {
            "info@walkme.pt"
        });
        intent.setFlags(0x10000000);
        intent.putExtra("android.intent.extra.TEXT", s2);
        intent.putExtra("android.intent.extra.SUBJECT", s);
        intent.setType("vnd.android.cursor.dir/email");
        startActivity(Intent.createChooser(intent, null));*/
        startActivity(new Intent(this, OutrasAppsActivity.class));
    }

    public void onActivityResult(int i, int j, Intent intent) {
        super.onActivityResult(i, j, intent);
        //uiHelper.onActivityResult(i, j, intent);
    }

    public void onCheckedChanged(CompoundButton compoundbutton, boolean flag) {
        int i = compoundbutton.getId();
        String s;
        int j;
        String s1;
        if (i != R.id.checkbox_musicaFundo) {
            if (i == R.id.checkbox_sonsJogo) {
                s = "isSonsJogoOn";
            } else {
                j = R.id.checkbox_confirmarResposta;
                s = null;
                if (i == j) {
                    s = "isConfirmacaoOn";
                }
            }
        }
        s = "isMusicaOn";
        if (s == null)
            return;
        DBAdapter dbadapter = App.DB();
        if (flag) {
            s1 = "1";
        } else {
            s1 = "0";
        }
        dbadapter.insertValue(s1, s);
        if (!flag || R.id.checkbox_musicaFundo != compoundbutton.getId()) {
            if (R.id.checkbox_musicaFundo != compoundbutton.getId())
                return;
            App.Musica.realPause();
            return;
        }
        App.Musica.start();
    }

    public void onClick(View view) {
        loginFacebook = true;
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_definicoes);
        mAdView = (AdView) findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                if (mAdView.getVisibility() != View.VISIBLE) {
//                    mAdView.setVisibility(View.VISIBLE);
//                }
//            }
//        });
        Typeface tf = Typeface.createFromAsset(this.getResources().getAssets(), "norwester.otf");
        ((TextView) findViewById(R.id.txt_tituloEnviarPergunta)).setTypeface(tf);
        ((TextView) findViewById(R.id.txt_tituloMusicaSons)).setTypeface(tf);
        ((TextView) findViewById(R.id.txt_tituloFeedback)).setTypeface(tf);
        ((TextView) findViewById(R.id.btn_avaliarApp)).setTypeface(tf);
        ((TextView) findViewById(R.id.btn_enviarMail)).setTypeface(tf);
        ((TextView) findViewById(R.id.btn_facebook)).setTypeface(tf);
        // Start loading the ad in the background.
        //mAdView.loadAd(adRequest);
        ((CheckBox) findViewById(R.id.checkbox_musicaFundo)).setChecked(App.DB().getValue("isMusicaOn").equals("1"));
        ((CheckBox) findViewById(R.id.checkbox_musicaFundo)).setOnCheckedChangeListener(this);
        ((CheckBox) findViewById(R.id.checkbox_sonsJogo)).setChecked(App.DB().getValue("isSonsJogoOn").equals("1"));
        ((CheckBox) findViewById(R.id.checkbox_sonsJogo)).setOnCheckedChangeListener(this);
        ((CheckBox) findViewById(R.id.checkbox_confirmarResposta)).setChecked(App.DB().getValue("isConfirmacaoOn").equals("1"));
        ((CheckBox) findViewById(R.id.checkbox_confirmarResposta)).setOnCheckedChangeListener(this);
        //((LoginButton)findViewById(R.id.btn_loginFacebook)).setOnClickListenerNotify(this);
        //uiHelper = new UiLifecycleHelper(this, callback);
        //uiHelper.onCreate(bundle);
        if (getString(R.string.lingua).equalsIgnoreCase("fr") || getString(R.string.lingua).equalsIgnoreCase("de") || getString(R.string.lingua).equalsIgnoreCase("it")) {
//            findViewById(R.id.btn_enviarMail).setVisibility(8);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        //uiHelper.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        //uiHelper.onPause();
    }

    protected void onResume() {
        super.onResume();
        //uiHelper.onResume();
        // mostraEscondeViews();
    }

    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        //uiHelper.onSaveInstanceState(bundle);
    }


}
