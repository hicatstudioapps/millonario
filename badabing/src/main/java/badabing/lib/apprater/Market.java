package badabing.lib.apprater;

import android.content.Context;
import android.net.Uri;

public interface Market {
    public Uri getMarketURI(Context context);
}
