package nl.matshofman.saxrssreader;

import java.io.*;
import java.net.URL;
import javax.xml.parsers.*;
import org.xml.sax.*;

// Referenced classes of package nl.matshofman.saxrssreader:
//            RssHandler, RssFeed

public class RssReader
{

    public RssReader()
    {
    }

    public static RssFeed read(InputStream inputstream)
        throws SAXException, IOException
    {
        RssFeed rssfeed;
        try
        {
            XMLReader xmlreader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            RssHandler rsshandler = new RssHandler();
            InputSource inputsource = new InputSource(inputstream);
            xmlreader.setContentHandler(rsshandler);
            xmlreader.parse(inputsource);
            rssfeed = rsshandler.getResult();
        }
        catch(ParserConfigurationException parserconfigurationexception)
        {
            throw new SAXException();
        }
        return rssfeed;
    }

    public static RssFeed read(String s)
        throws SAXException, IOException
    {
        return read(((InputStream) (new ByteArrayInputStream(s.getBytes()))));
    }

    public static RssFeed read(URL url)
        throws SAXException, IOException
    {
        return read(url.openStream());
    }
}
