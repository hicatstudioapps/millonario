package nl.matshofman.saxrssreader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

// Referenced classes of package nl.matshofman.saxrssreader:
//            RssFeed, RssItem

public class RssHandler extends DefaultHandler
{

    private RssFeed rssFeed;
    private RssItem rssItem;
    private StringBuilder stringBuilder;

    public RssHandler()
    {
    }

    public void characters(char ac[], int i, int j)
    {
        stringBuilder.append(ac, i, j);
    }

    public void endElement(String s, String s1, String s2)
    {
    	try{
	        if(rssFeed == null || rssItem != null){
	        	if(rssItem == null)
	            	return;
	            if(s2.equals("content:encoded"))
	            {
	                s2 = "content";
	            }
	            String s3 = (new StringBuilder("set")).append(s2.substring(0, 1).toUpperCase()).append(s2.substring(1)).toString();
	            Method method = rssItem.getClass().getMethod(s3, new Class[] {
	                java.lang.String.class
	            });
	            RssItem rssitem = rssItem;
	            Object aobj[] = new Object[1];
	            aobj[0] = stringBuilder.toString();
	            method.invoke(rssitem, aobj);
	            return;
	        }
	        if(s2 == null)
	        {
	            //break MISSING_BLOCK_LABEL_115;
	        	return;
	        }
	        if(s2.length() > 0)
	        {
	            String s4 = (new StringBuilder("set")).append(s2.substring(0, 1).toUpperCase()).append(s2.substring(1)).toString();
	            Method method1 = rssFeed.getClass().getMethod(s4, new Class[] {
	                java.lang.String.class
	            });
	            RssFeed rssfeed = rssFeed;
	            Object aobj1[] = new Object[1];
	            aobj1[0] = stringBuilder.toString();
	            method1.invoke(rssfeed, aobj1);
	        }
    	}catch(Exception exception)
        {
            return;
        }
    }

    public RssFeed getResult()
    {
        return rssFeed;
    }

    public void startDocument()
    {
        rssFeed = new RssFeed();
    }

    public void startElement(String s, String s1, String s2, Attributes attributes)
    {
        stringBuilder = new StringBuilder();
        if(s2.equals("item") && rssFeed != null)
        {
            rssItem = new RssItem();
            rssItem.setFeed(rssFeed);
            rssFeed.addRssItem(rssItem);
        }
    }
}
