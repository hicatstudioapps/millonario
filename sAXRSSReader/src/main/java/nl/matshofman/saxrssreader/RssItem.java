package nl.matshofman.saxrssreader;

import android.os.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

// Referenced classes of package nl.matshofman.saxrssreader:
//            RssFeed

public class RssItem
    implements Comparable, Parcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        public RssItem createFromParcel(Parcel parcel)
        {
            return new RssItem(parcel);
        }

        public RssItem[] newArray(int i)
        {
            return new RssItem[i];
        }
    };
    private String content;
    private String description;
    private RssFeed feed;
    private String guid;
    private String link;
    private Date pubDate;
    private String title;

    public RssItem()
    {
    }

    public RssItem(Parcel parcel)
    {
        Bundle bundle = parcel.readBundle();
        title = bundle.getString("title");
        link = bundle.getString("link");
        pubDate = (Date)bundle.getSerializable("pubDate");
        description = bundle.getString("description");
        content = bundle.getString("content");
        feed = (RssFeed)bundle.getParcelable("feed");
        guid = (String)bundle.getString("guid");
    }

    public int compareTo(Object obj)
    {
        return compareTo((RssItem)obj);
    }

    public int compareTo(RssItem rssitem)
    {
        if(getPubDate() != null && rssitem.getPubDate() != null)
        {
            return getPubDate().compareTo(rssitem.getPubDate());
        } else
        {
            return 0;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String getContent()
    {
        return content;
    }

    public String getDescription()
    {
        return description;
    }

    public RssFeed getFeed()
    {
        return feed;
    }

    public String getGuid()
    {
        return guid;
    }

    public String getLink()
    {
        return link;
    }

    public Date getPubDate()
    {
        return pubDate;
    }

    public String getTitle()
    {
        return title;
    }

    public void setContent(String s)
    {
        content = s;
    }

    public void setDescription(String s)
    {
        description = s;
    }

    public void setFeed(RssFeed rssfeed)
    {
        feed = rssfeed;
    }

    public void setGuid(String s)
    {
        guid = s;
    }

    public void setLink(String s)
    {
        link = s;
    }

    public void setPubDate(String s)
    {
        try
        {
            pubDate = (new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH)).parse(s);
            return;
        }
        catch(ParseException parseexception)
        {
            parseexception.printStackTrace();
        }
    }

    public void setPubDate(Date date)
    {
        pubDate = date;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("link", link);
        bundle.putSerializable("pubDate", pubDate);
        bundle.putString("description", description);
        bundle.putString("content", content);
        bundle.putString("guid", guid);
        bundle.putParcelable("feed", feed);
        parcel.writeBundle(bundle);
    }

}
