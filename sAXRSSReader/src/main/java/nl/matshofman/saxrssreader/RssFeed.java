package nl.matshofman.saxrssreader;

import android.os.*;
import java.util.ArrayList;

// Referenced classes of package nl.matshofman.saxrssreader:
//            RssItem

public class RssFeed
    implements Parcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        public RssFeed createFromParcel(Parcel parcel)
        {
            return new RssFeed(parcel);
        }

        public RssFeed[] newArray(int i)
        {
            return new RssFeed[i];
        }

    };
    private String description;
    private String language;
    private String link;
    private ArrayList rssItems;
    private String title;

    public RssFeed()
    {
        rssItems = new ArrayList();
    }

    public RssFeed(Parcel parcel)
    {
        Bundle bundle = parcel.readBundle();
        title = bundle.getString("title");
        link = bundle.getString("link");
        description = bundle.getString("description");
        language = bundle.getString("language");
        rssItems = bundle.getParcelableArrayList("rssItems");
    }

    void addRssItem(RssItem rssitem)
    {
        rssItems.add(rssitem);
    }

    public int describeContents()
    {
        return 0;
    }

    public String getDescription()
    {
        return description;
    }

    public String getLanguage()
    {
        return language;
    }

    public String getLink()
    {
        return link;
    }

    public ArrayList getRssItems()
    {
        return rssItems;
    }

    public String getTitle()
    {
        return title;
    }

    public void setDescription(String s)
    {
        description = s;
    }

    public void setLanguage(String s)
    {
        language = s;
    }

    public void setLink(String s)
    {
        link = s;
    }

    public void setRssItems(ArrayList arraylist)
    {
        rssItems = arraylist;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("link", link);
        bundle.putString("description", description);
        bundle.putString("language", language);
        bundle.putParcelableArrayList("rssItems", rssItems);
        parcel.writeBundle(bundle);
    }

}
